FROM php:7.2.5-fpm

RUN apt-get update && apt-get install -y libmcrypt-dev zlib1g-dev libpng-dev g++ \
    mysql-client zip unzip curl --no-install-recommends \
    && docker-php-ext-install pdo_mysql \
    && docker-php-ext-install zip \
    && docker-php-ext-install gd

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php composer-setup.php && \
    php -r "unlink('composer-setup.php');" && \
    mv composer.phar /usr/local/bin/composer

WORKDIR /var/www/html
COPY . /var/www/html

RUN composer install
RUN composer update
RUN php artisan ide-helper:generate
#RUN php artisan ide-helper:models
#RUN php artisan ide-helper:meta