<?php

namespace App\Http\Controllers;

use App\Traits\Utilities\TransformsResponses;

use App;
use Carbon\Carbon;
use DateTime;
use DateTimeZone;
use DB;
use Exception;

class BaseController extends Controller
{
    use TransformsResponses;

    const DATE_FORMAT           = 'Y-m-d H:i:s';
    const VIEW_DATE_FORMAT      = 'F j, Y H:i';
    const API_IMAGE_UPLOAD_PATH = '/somewhere';
    const DATETIME_FIELD        = 'datetime';
    const DATECOMPLETED_FIELD   = 'date_completed';
    const DEVICEORDERTIME_FIELD = 'deviceOrderTime';

    public $order_date_field;

    public function __construct()
    {

    }

    function getMysqlTimezone()
    {
        $result  =   DB::select("SELECT @@system_time_zone as mysql_timezone");
//dd($result);
        $timezone_abbreviations = DateTimeZone::listAbbreviations();
        return $timezone_abbreviations[strtolower($result[0]->mysql_timezone)][0]['timezone_id'];
    }

    function getVenueTimezone($venueId)
    {
        $result  =   DB::select("SELECT `timezone` FROM (`venues`) WHERE `id` =  :venueId", ["venueId" => $venueId]);

        return $result[0]->timezone;
    }

    public function get_next_date($date)
    {
        return date('Y-m-d H:i:s', strtotime("+1 day", strtotime($date)));
    }

    public function get_weekday_name($date, $timezone)
    {
        if (is_null($timezone))
        {
            return date('m-d-Y', strtotime($date));
        }
        else
        {
            return $this->convertFromUTC($date, $timezone, 'm-d-Y');
        }
    }

    public function set_order_date_field($report_based_on_datetime)
    {
        if ($report_based_on_datetime == '1')
        {
            $this->order_date_field = self::DEVICEORDERTIME_FIELD;
        }
        elseif ($report_based_on_datetime == '0')
        {
            $this->order_date_field = self::DATECOMPLETED_FIELD;
        }

    }


    public function convertAndFormatTimeFromUTC($utcTimeStamp, $toTimezone="America/New_York", $format='Y-m-d H:i:s')
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $utcTimeStamp, 'UTC')->setTimezone($toTimezone)->format($format);
    }

    public static function convertTimezone($dateStr, $fromTimezone = NULL, $toTimezone = NULL, $dateFormat = 'Y-m-d H:i:s')
    {
        if(empty($fromTimezone) || empty($toTimezone))
        {
            return date($dateFormat, strtotime($dateStr));
        }

        try
        {
            $date = new DateTime($dateStr, new DateTimeZone($fromTimezone));
            $date->setTimezone(new DateTimeZone($toTimezone));
            return $date->format($dateFormat);
        }
        catch(Exception $ex)
        {
            return $dateStr;
        }
    }


    function setAdjustedDateParameters($start_date = FALSE, $end_date = FALSE, $timezone = NULL, $day_start_time = NULL, $day_end_time = NULL)
    {
        $adj_start_date = $start_date;
        $adj_end_date = $end_date;

        if($adj_start_date == FALSE)
        {
            $adj_start_date = $this->convertFromUTC(date('Y-m-d H:i:s'), $timezone, 'Y-m-d');
            $adj_start_time = $this->convertFromUTC(date('Y-m-d H:i:s'), $timezone, 'H:i:s');

            // Adjust start date based on current local time and day start time
            if(isset($day_start_time))
            {
                if(strtotime($adj_start_time) < strtotime($day_start_time))
                {
                    // Currently still the previous day
                    $adj_start_date = date('Y-m-d', strtotime($adj_start_date . ' - 1 day'));
                }
            }
        }

        if($adj_end_date == FALSE)
        {
            $adj_end_date = $this->convertFromUTC(date('Y-m-d H:i:s', strtotime($adj_start_date . ' + 1 day')), $timezone, 'Y-m-d');
        }

        if(!preg_match('/\d+:\d+/', $adj_start_date) && isset($day_start_time))
        {
            $adj_start_date = $adj_start_date . ' ' . $day_start_time;
        }

        if(!preg_match('/\d+:\d+/', $adj_end_date) && isset($day_end_time))
        {
            $start_time = date('H:i:s', strtotime($adj_start_date));

            if(strtotime($start_time) < strtotime($day_end_time))
            {
                $adj_end_date = date('Y-m-d', strtotime($adj_start_date)) . ' ' . $day_end_time;
            }
            else
            {
                $adj_end_date = $adj_end_date . ' ' . $day_end_time;
            }
        }

        return array('start_date' => $adj_start_date, 'end_date' => $adj_end_date);
    }

    function convertFromUTC($dateStr, $timezone = NULL, $dateFormat = 'Y-m-d H:i:s')
    {
        try
        {
            if(empty($timezone))
            {
                return date($dateFormat, strtotime($dateStr));
            }

            // Convert from UTC to output timezone
            //$date = new DateTime($dateStr, new DateTimeZone('UTC'));

            // Temporarily convert from server time
            $date = new DateTime($dateStr, new DateTimeZone('America/Los_Angeles'));

            $date->setTimezone(new DateTimeZone($timezone));
            return $date->format($dateFormat);
        }
        catch(Exception $ex)
        {
            return $dateStr;
        }
    }

    function convertToUTC($dateStr, $timezone = NULL, $dateFormat = 'Y-m-d H:i:s')
    {
        $serverTimeZone = 'America/Los_Angeles';

        try
        {
            if(empty($timezone) || $serverTimeZone == $timezone)
            {
                return date($dateFormat, strtotime($dateStr));
            }

            // Convert from input timezone to UTC
            $date = new DateTime($dateStr, new DateTimeZone($timezone));
            $date->setTimezone(new DateTimeZone('UTC'));
            //return $date->format('Y-m-d H:i:s');

            // Temporarily convert to server time
            $date->setTimezone(new DateTimeZone($serverTimeZone));
            return $date->format($dateFormat);
        }
        catch(Exception $ex)
        {
            return $dateStr;
        }
    }
}