<?php
namespace App\Http\Controllers\Api\V1\Reports;

use App\Http\Controllers\BaseController;
use App\Http\Requests\Api\V1\Reports\TenderTypeReportRequest;
use Carbon\Carbon;
use DB;
use DateTime;
use DateInterval;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;

class TenderTypeReportController extends BaseController
{
    public $cachedTaxColumns;

    public function index(TenderTypeReportRequest $Request)
    {
        //phpinfo();

        $requestStartTime = time();

        $preparedData  =   $this->prepareReportVariables($Request->validated());

        //dd($preparedData);

        // Run report & retrieve data
        $itemSell       = [];
        $dataForReport  = [];

        //// Start Transaction
        if (    $preparedData['report']['reportSettings']['reportType'] == 'excel'
            &&  $preparedData['report']['reportSettings']['weekdays'] == '1'
            &&  $preparedData['report']['report']['data']['daysBetween'] <= 15
        )
        {
            if ($preparedData['report']['data']['daysBetween'] >= 2)
            {
                $curr_start_date = $preparedData['startDate'];
                $curr_end_date   = $this->get_next_date($preparedData['startDate']);

                for ($i = 1; $i <= $preparedData['report']['data']['daysBetween']; $i++)
                {
                    $itemSell[$this->get_weekday_name($curr_start_date, $preparedData['report']['data']['timeZone'])] = array(
                        $preparedData['reportSettings']['venueInfo']['name'],
                        $preparedData['reportSettings']['activeLayoutInfo']['name'],
                        'START DATE ' . date(self::VIEW_DATE_FORMAT, strtotime($preparedData['reportSettings']['start_date'])) . '      END DATE ' . date(self::VIEW_DATE_FORMAT, strtotime($preparedData['reportSettings']['end_date'])),
                        $this->runActualTenderTypeReport
                        (
                            $preparedData['report']['reportSettings']['connect_entity_id'],
                            $preparedData['report']['reportSettings']['connect_entity_type'],
                            $curr_start_date,
                            $curr_end_date,
                            $preparedData['report']['data']['extraUser']
                        )
                    );

                    $curr_start_date = $curr_end_date;
                    $curr_end_date   = $this->get_next_date($curr_end_date);
                }
            }
            else
            {
                $itemSell[$this->get_weekday_name($preparedData['startDate'], $preparedData['report']['data']['timeZone'])] = array(
                    $preparedData['reportSettings']['venueInfo']['name'],
                    $preparedData['reportSettings']['activeLayoutInfo']['name'],
                    'START DATE ' . date(self::VIEW_DATE_FORMAT, strtotime($preparedData['reportSettings']['start_date'])) . '      END DATE ' . date(self::VIEW_DATE_FORMAT, strtotime($preparedData['reportSettings']['end_date'])),
                    $this->runActualTenderTypeReport
                    (
                        $preparedData['report']['reportSettings']['connect_entity_id'],
                        $preparedData['report']['reportSettings']['connect_entity_type'],
                        $preparedData['startDate'],
                        $preparedData['endDate'],
                        $preparedData['report']['data']['extraUser']
                    )
                );
            }
        }
        else
        {
            //dd("in second condition under index");
            //dd($preparedData);
            $itemSell = $this->runActualTenderTypeReport
            (
                $preparedData['report']['reportSettings']['connect_entity_id'],
                $preparedData['report']['reportSettings']['connect_entity_type'],
                $preparedData['startDate'],
                $preparedData['endDate'],
                $preparedData['report']['data']['extraUser']
            );
        }

        $dataRunEndTime = $generateReportStartTime = time();

        //// End Transaction


        // Pass Data to Report Generator with template and file types
        $report_name        = 'Tender Type';


        // Get File (xls csv odf, ?pdf?) data ready
        // ============================================
        $columns   = array('Category', 'Quantity', 'Cash', 'Credit', 'Submit', 'Sales', 'Tax (Inc)', 'Tax (Exc)', 'Net Revenue', 'Discount', 'Commission', 'Net Total');

        $availableExtensions = [
            'excel' => '.xlsx',
            'csv'   => '.csv',
            'ods'   => '.ods',
            'pdf'   => '.pdf',
        ];

        $fileExtension = (array_key_exists($preparedData['report']['reportSettings']['reportType'], $availableExtensions) ? $availableExtensions[$preparedData['report']['reportSettings']['reportType']]  : ".xlsx");
        $fileNameNoExt = $preparedData['report']['reportSettings']['venueId'] . '_' . time();
        $fileName = $fileNameNoExt . $fileExtension;
        //dd($fileName);
        $filePath = storage_path() . "/";
        $fullFilePath = $filePath . $fileName;
        //dd($downloadPath);
        //dd($preparedData);


        $reportType  = 'tender-type';
        $reportTitle = $report_name . ' Report from ' . date(self::VIEW_DATE_FORMAT, strtotime($preparedData['report']['reportSettings']['start_date'])) . ' to ' . date(self::VIEW_DATE_FORMAT, strtotime($preparedData['report']['reportSettings']['end_date']));


        $dataForReport['parkingPassCodes']              =   $preparedData['report']['parking']['parking_pass_codes'];
        $dataForReport['parkingPassCodesByVendor']      =   $preparedData['report']['parking']['parking_pass_codes_by_vendor'];
        $dataForReport['parkingPassCodesByAccount']     =   $preparedData['report']['parking']['parking_pass_codes_by_account'];
        $dataForReport['parkingPassCodesByVenue']       =   $preparedData['report']['parking']['parking_pass_codes_by_venue'];
        $dataForReport['parkingPassCodesByVendorGroup'] =   $preparedData['report']['parking']['parking_pass_codes_by_vendor_group'];


        //dd($preparedData);
        if (!($preparedData['report']['reportSettings']['reportType'] == 'excel' && $preparedData['report']['data']['weekdays'] == '1' && $preparedData['report']['data']['daysBetween'] <= 15))
        {
            $dataForReport['report_data']       = $itemSell;
            $dataForReport['currentVenueName']  = $preparedData['venueInfo'][0]->name;
            $dataForReport['currentLayoutName'] = $preparedData['activeLayoutInfo'][0]->name;
            $dataForReport['second_line']       = 'START DATE ' . date(self::VIEW_DATE_FORMAT, strtotime($preparedData['report']['reportSettings']['start_date'])) . '      END DATE ' . date(self::VIEW_DATE_FORMAT, strtotime($preparedData['report']['reportSettings']['end_date']));
        }
        else
        {
            $dataForReport = $itemSell;
        }

        $fileViewData = [
            'stream' => FALSE,
            'filePath'              => $filePath,
            'fullFilePath'          => $fullFilePath,
            'filename'              => $fileName,
            'fileNameNoExt'         => $fileNameNoExt,
            'fileExtension'         => $fileExtension,
            'weekdays'              => ($preparedData['report']['reportSettings']['reportType'] == 'excel' && $preparedData['report']['data']['weekdays'] == '1' && $preparedData['report']['data']['daysBetween'] <= 15
                                        ? TRUE
                                        : FALSE),
            'template'              => $reportType,
            'title'                 => $reportTitle,
            'currentLayoutName'     => $reportTitle,
            'currentVenueDateTime'  => $preparedData['currentVenueDateTime'],
            'dataForReport'         => $dataForReport,
            'columns'               => $columns,
            'creator'               => 'Appetize Inc',
            'subject'               => '',
            'description'           => 'Tender Type Report',
            'category'              => '',
            'exportType'            => '',
            'preparedData'          => $preparedData,
            'exportFileData'        =>  [

            ],
        ];

        //dd($fileViewData);

        $fileOutput= [];
        if ($preparedData['report']['data']['weekdays'] == TRUE && ($fileViewData['exportType'] == 'Excel2007' || $fileViewData['exportType'] == 'Excel5'))
        {
            $excel_datas = $dataForReport;
            $i           = 0;

            foreach ($excel_datas as $curr_title => $curr_excel_data)
            {
                $objWorkSheet = $this->objPHPExcel->createSheet($i); //Setting index when creating
                $this->objPHPExcel->setActiveSheetIndex($i);
                $this->data['report_data']        = $curr_excel_data[3];
                $this->data['currentVenueName']   = $curr_excel_data[0];
                $this->data['currentLayoutName']  = $curr_excel_data[1];
                $this->data['second_line']        = $curr_excel_data[2];
                $this->data['parking_pass_codes'] = 0;

                $fileOutput = $this->renderTenderType($fileViewData);
                $this->autoWidth = FALSE;

                // Rename sheet
                //$activeSheet->setTitle($curr_title);
                $exportFileData['row'] = 1;
                $i++;
            }

        }
        else
        {
            $fileOutput = $this->renderTenderType($fileViewData);
        }

        // Store files in S3 - pass location under report bucket and file for each


        // Craft and Email with File locations


        // https://connect.awesome.dev/analytics?report=tenderType
        //$this->analyze->reportGenerated(2, 5, $this->corporateId, $this->venue_id, $this->session->userdata);
        //$this->analyze->reportDownloaded(2, 5, $this->corporateId, $this->venue_id, $this->session->userdata);



        $requestEndTime = time();

        $output = [
            'title'     =>  'Get Tender Type Report',
            'status'    =>  true,
            'data'      =>  [
                'report' => [
                    'files' => $fileOutput,
                    'time'  => [
                        'requestStartTime'          => $requestStartTime,
                        'requestEndTime'            => $requestEndTime,
                        'dataRunEndTime'            => $dataRunEndTime,
                        'generateReportStartTime'   => $generateReportStartTime,
                        'timeToRunReportQueries'    => ($dataRunEndTime - $requestStartTime) . "seconds",
                        'timeToGenerateReportFiles' => ($requestEndTime - $generateReportStartTime) . "seconds",
                        'timeForRequest'            => ($requestEndTime - $requestStartTime) . "seconds",
                    ],
                    'parameters' => $Request->validated(),
                ]
            ],
            'message'   =>  'Tender Type generated successfully.',
            "timestamp" =>  Carbon::now('UTC'),
        ];

        return response()->json($output);
    }


    /**
     * Render the tender type files
     *
     * @param array $fileViewData
     *
     * @return array
     *
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    private function renderTenderType(array $fileViewData)
    {
        //dd($fileViewData);

        $preparedData = $fileViewData['preparedData'];

        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0);

        $activeSheet = $spreadsheet->getActiveSheet();

        // Line 1
        $activeSheet->mergeCells('A1:Y1');
        $objRichText = strtoupper($preparedData['venueInfo'][0]->name) . (isset($preparedData['activeLayoutInfo'][0]->name) ? '      LAYOUT: ' . strtoupper($preparedData['activeLayoutInfo'][0]->name) : '') . '      PRINTED DATE ' . $fileViewData['currentVenueDateTime'];
        $this->setSheetStyle($activeSheet, 'A1', '595959', 'FFFFFF', NULL, 28);
        $activeSheet->setCellValue('A1', is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);


        /* Line 2 */
        $activeSheet->mergeCells('A2:Y2');
        $objRichText = $fileViewData['dataForReport']['second_line'];
        $this->setSheetStyle($activeSheet, 'A2', 'BFBFBF', '17214B', NULL, 18);
        $activeSheet->setCellValue('A2', is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

        /* Line 3 */
        $activeSheet->mergeCells('A3:Y3');
        $objRichText = 'Tender Breakdown';
        $this->setSheetStyle($activeSheet, 'A3', 'FFFFFF', '17214B', NULL, 36, TRUE);
        $activeSheet->setCellValue('A3', is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);


        /* Credit Card Break Down */
        $activeSheet->mergeCells('A5:P5');
        $objRichText = 'Credit Card Breakdown (including fees and tips)';
        $this->setSheetStyle($activeSheet, 'A5', '595959', 'FFFFFF', FALSE, 20);
        $activeSheet->setCellValue('A5', is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

        $activeSheet->mergeCells('A6:D6');
        $objRichText = 'Credit Types';
        $this->setSheetStyle($activeSheet,'A6', 'BFBFBF', '17214B', FALSE, 12, TRUE);
        $activeSheet->setCellValue('A6', is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

        $activeSheet->mergeCells('E6:G6');
        $objRichText = 'Card Present';
        $this->setSheetStyle($activeSheet,'E6', 'BFBFBF', '17214B', FALSE, 12, TRUE);
        $activeSheet->setCellValue('E6', is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

        $activeSheet->mergeCells('H6:J6');
        $objRichText = 'Card Not Present';
        $this->setSheetStyle($activeSheet,'H6', 'BFBFBF', '17214B', FALSE, 12, TRUE);
        $activeSheet->setCellValue('H6', is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

        $activeSheet->mergeCells('K6:M6');
        $objRichText = 'Credit Total';
        $this->setSheetStyle($activeSheet,'K6', 'BFBFBF', '17214B', FALSE, 12, TRUE);
        $activeSheet->setCellValue('K6', is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

        $activeSheet->mergeCells('N6:P6');
        $objRichText = 'Transactions';
        $this->setSheetStyle($activeSheet,'N6', 'BFBFBF', '17214B', FALSE, 12, TRUE);
        $activeSheet->setCellValue('N6', is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

        $exportFileData = [];

        $exportFileData['row']  = 7;
        $cash_total             = 0;
        $credit_tips            = 0;
        $sub_total              = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);


        //dd($fileViewData);

        $card_tax_col       = $fileViewData['dataForReport']['report_data']['card'][1];
        $exportFileData['card'] = $fileViewData['dataForReport']['report_data']['card'][0];
//        dd([
//            $exportFileData['card'],
//            json_decode(json_encode($fileViewData['dataForReport']['report_data']['card'][0]), true)
//        ]);
        $exportFileData['card'] = json_decode(json_encode($fileViewData['dataForReport']['report_data']['card'][0]), true);

        if (count($exportFileData['card']) > 0) 
        {
            $cash_total        = $exportFileData['card'][0]['cash_total'];
            $cash_transactions = $exportFileData['card'][0]['cash_tran_count'];
            $credit_tips       = $exportFileData['card'][0]['total_credit_tips'];
            $sub_total[0]      = $exportFileData['card'][0]['cp_amount_with_fee_tip'] + $exportFileData['card'][0]['apple_pay_amount_with_fee_tip'];
            $sub_total[1]      = $exportFileData['card'][0]['cnp_amount_with_fee_tip'] + $exportFileData['card'][0]['beacon_amount_with_fee_tip'] + $exportFileData['card'][0]['wristband_amount_with_fee_tip'] + $exportFileData['card'][0]['paypal_amount_with_fee_tip'] + $exportFileData['card'][0]['directpayment_amount_with_fee_tip'];
            $sub_total[2]      = $exportFileData['card'][0]['credit_total_with_fee_tip'];
            $sub_total[3]      = $exportFileData['card'][0]['cp_amount_with_fee_tip'] + $exportFileData['card'][0]['apple_pay_amount_with_fee_tip'];
            $sub_total[4]      = $exportFileData['card'][0]['cnp_amount_with_fee_tip'] + $exportFileData['card'][0]['beacon_amount_with_fee_tip'] + $exportFileData['card'][0]['wristband_amount_with_fee_tip'] + $exportFileData['card'][0]['paypal_amount_with_fee_tip'] + $exportFileData['card'][0]['directpayment_amount_with_fee_tip'];
            $sub_total[5]      = $exportFileData['card'][0]['credit_total_with_fee_tip'];
            $sub_total[6]      = $exportFileData['card'][0]['beacon_amount'];
            $sub_total[7]      = $exportFileData['card'][0]['apple_pay_amount'];
            $sub_total[8]      = $exportFileData['card'][0]['wristband_amount'];
            $sub_total[9]      = $exportFileData['card'][0]['paypal_amount'];
            $sub_total[10]     = $exportFileData['card'][0]['directpayment_amount'];
            $sub_total[11]      = 0;

            $activeSheet->mergeCells('A' . $exportFileData['row'] . ':D' . $exportFileData['row']);
            $objRichText = "American Express";
            $this->setSheetStyle($activeSheet,'A' . $exportFileData['row'], NULL, NULL, FALSE, 12);
            $activeSheet->setCellValue('A' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('E' . $exportFileData['row'] . ':G' . $exportFileData['row']);
            $objRichText = $exportFileData['card'][0]['amex_cp_amount_with_fee_tip'];
            $this->setSheetStyle($activeSheet,'E' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
            $activeSheet->setCellValue('E' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('H' . $exportFileData['row'] . ':J' . $exportFileData['row']);
            $objRichText = $exportFileData['card'][0]['amex_cnp_amount_with_fee_tip'];
            $this->setSheetStyle($activeSheet,'H' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
            $activeSheet->setCellValue('H' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('K' . $exportFileData['row'] . ':M' . $exportFileData['row']);
            $objRichText = $exportFileData['card'][0]['total_amex_sales_with_fee_tip'];
            $this->setSheetStyle($activeSheet,'K' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
            $activeSheet->setCellValue('K' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('N' . $exportFileData['row'] . ':P' . $exportFileData['row']);
            $objRichText = $exportFileData['card'][0]['amex_tran_count'];
            $this->setSheetStyle($activeSheet,'N' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
            $activeSheet->setCellValue('N' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
            $sub_total[11] += $exportFileData['card'][0]['amex_tran_count'];

            $exportFileData['row']++;

            $activeSheet->mergeCells('A' . $exportFileData['row'] . ':D' . $exportFileData['row']);
            $objRichText = "Discover";
            $this->setSheetStyle($activeSheet,'A' . $exportFileData['row'], NULL, NULL, FALSE, 12);
            $activeSheet->setCellValue('A' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('E' . $exportFileData['row'] . ':G' . $exportFileData['row']);
            $objRichText = $exportFileData['card'][0]['discover_cp_amount_with_fee_tip'];
            $this->setSheetStyle($activeSheet,'E' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
            $activeSheet->setCellValue('E' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('H' . $exportFileData['row'] . ':J' . $exportFileData['row']);
            $objRichText = $exportFileData['card'][0]['discover_cnp_amount_with_fee_tip'];
            $this->setSheetStyle($activeSheet,'H' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
            $activeSheet->setCellValue('H' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('K' . $exportFileData['row'] . ':M' . $exportFileData['row']);
            $objRichText = $exportFileData['card'][0]['total_discover_sales_with_fee_tip'];
            $this->setSheetStyle($activeSheet,'K' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
            $activeSheet->setCellValue('K' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('N' . $exportFileData['row'] . ':P' . $exportFileData['row']);
            $objRichText = $exportFileData['card'][0]['discover_tran_count'];
            $this->setSheetStyle($activeSheet,'N' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
            $activeSheet->setCellValue('N' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
            $sub_total[11] += $exportFileData['card'][0]['discover_tran_count'];

            $exportFileData['row']++;

            $activeSheet->mergeCells('A' . $exportFileData['row'] . ':D' . $exportFileData['row']);
            $objRichText = "MasterCard";
            $this->setSheetStyle($activeSheet,'A' . $exportFileData['row'], NULL, NULL, FALSE, 12);
            $activeSheet->setCellValue('A' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('E' . $exportFileData['row'] . ':G' . $exportFileData['row']);
            $objRichText = $exportFileData['card'][0]['mastercard_cp_amount_with_fee_tip'];
            $this->setSheetStyle($activeSheet,'E' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
            $activeSheet->setCellValue('E' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('H' . $exportFileData['row'] . ':J' . $exportFileData['row']);
            $objRichText = $exportFileData['card'][0]['mastercard_cnp_amount_with_fee_tip'];
            $this->setSheetStyle($activeSheet,'H' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
            $activeSheet->setCellValue('H' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('K' . $exportFileData['row'] . ':M' . $exportFileData['row']);
            $objRichText = $exportFileData['card'][0]['total_mastercard_sales_with_fee_tip'];
            $this->setSheetStyle($activeSheet,'K' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
            $activeSheet->setCellValue('K' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('N' . $exportFileData['row'] . ':P' . $exportFileData['row']);
            $objRichText = $exportFileData['card'][0]['mastercard_tran_count'];
            $this->setSheetStyle($activeSheet,'N' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
            $activeSheet->setCellValue('N' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
            $sub_total[11] += $exportFileData['card'][0]['mastercard_tran_count'];

            $exportFileData['row']++;

            $activeSheet->mergeCells('A' . $exportFileData['row'] . ':D' . $exportFileData['row']);
            $objRichText = "Visa";
            $this->setSheetStyle($activeSheet,'A' . $exportFileData['row'], NULL, NULL, FALSE, 12);
            $activeSheet->setCellValue('A' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('E' . $exportFileData['row'] . ':G' . $exportFileData['row']);
            $objRichText = $exportFileData['card'][0]['visa_cp_amount_with_fee_tip'];
            $this->setSheetStyle($activeSheet,'E' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
            $activeSheet->setCellValue('E' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('H' . $exportFileData['row'] . ':J' . $exportFileData['row']);
            $objRichText = $exportFileData['card'][0]['visa_cnp_amount_with_fee_tip'];
            $this->setSheetStyle($activeSheet,'H' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
            $activeSheet->setCellValue('H' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('K' . $exportFileData['row'] . ':M' . $exportFileData['row']);
            $objRichText = $exportFileData['card'][0]['total_visa_sales_with_fee_tip'];
            $this->setSheetStyle($activeSheet,'K' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
            $activeSheet->setCellValue('K' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('N' . $exportFileData['row'] . ':P' . $exportFileData['row']);
            $objRichText = $exportFileData['card'][0]['visa_tran_count'];
            $this->setSheetStyle($activeSheet,'N' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
            $activeSheet->setCellValue('N' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
            $sub_total[11] += $exportFileData['card'][0]['visa_tran_count'];

            $exportFileData['row']++;

            $activeSheet->mergeCells('A' . $exportFileData['row'] . ':D' . $exportFileData['row']);
            $objRichText = "Other";
            $this->setSheetStyle($activeSheet,'A' . $exportFileData['row'], NULL, NULL, FALSE, 12);
            $activeSheet->setCellValue('A' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('E' . $exportFileData['row'] . ':G' . $exportFileData['row']);
            $objRichText = $exportFileData['card'][0]['other_cp_amount_with_fee_tip'];
            $this->setSheetStyle($activeSheet,'E' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
            $activeSheet->setCellValue('E' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('H' . $exportFileData['row'] . ':J' . $exportFileData['row']);
            $objRichText = $exportFileData['card'][0]['other_cnp_amount_with_fee_tip'];
            $this->setSheetStyle($activeSheet,'H' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
            $activeSheet->setCellValue('H' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('K' . $exportFileData['row'] . ':M' . $exportFileData['row']);
            $objRichText = $exportFileData['card'][0]['total_other_sales_with_fee_tip'];
            $this->setSheetStyle($activeSheet,'K' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
            $activeSheet->setCellValue('K' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('N' . $exportFileData['row'] . ':P' . $exportFileData['row']);
            $objRichText = $exportFileData['card'][0]['other_tran_count'];
            $this->setSheetStyle($activeSheet,'N' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
            $activeSheet->setCellValue('N' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
            $sub_total[11] += $exportFileData['card'][0]['other_tran_count'];

            $exportFileData['row']++;

            if ($exportFileData['card'][0]['beacon_amount_with_fee_tip'] != 0)
            {
                $activeSheet->mergeCells('A' . $exportFileData['row'] . ':D' . $exportFileData['row']);
                $objRichText = "Beacon";
                $this->setSheetStyle($activeSheet,'A' . $exportFileData['row'], NULL, NULL, FALSE, 12);
                $activeSheet->setCellValue('A' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

                $activeSheet->mergeCells('E' . $exportFileData['row'] . ':G' . $exportFileData['row']);
                $objRichText = 0;
                $this->setSheetStyle($activeSheet,'E' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                $activeSheet->setCellValue('E' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

                $activeSheet->mergeCells('H' . $exportFileData['row'] . ':J' . $exportFileData['row']);
                $objRichText = $exportFileData['card'][0]['beacon_amount_with_fee_tip'];
                $this->setSheetStyle($activeSheet,'H' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                $activeSheet->setCellValue('H' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

                $activeSheet->mergeCells('K' . $exportFileData['row'] . ':M' . $exportFileData['row']);
                $objRichText = $exportFileData['card'][0]['beacon_amount_with_fee_tip'];
                $this->setSheetStyle($activeSheet,'K' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                $activeSheet->setCellValue('K' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

                $activeSheet->mergeCells('N' . $exportFileData['row'] . ':P' . $exportFileData['row']);
                $objRichText = $exportFileData['card'][0]['beacon_count'];
                $this->setSheetStyle($activeSheet,'N' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                $activeSheet->setCellValue('N' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
                $sub_total[11] += $exportFileData['card'][0]['beacon_count'];

                $exportFileData['row']++;
            }

            /* "Hide the Apple Pay line", more details there: https://trello.com/c/CFGzNztu/5008-end-of-show-tender-type-cc-breakdown
            if ($exportFileData['card'][0]['apple_pay_amount_with_fee_tip'] != 0)
            {
                            $activeSheet->mergeCells('A' . $exportFileData['row'] . ':D' . $exportFileData['row']);
                            $objRichText = "Apple Pay";
                            $this->setSheetStyle($activeSheet,'A' . $exportFileData['row'], NULL, NULL, FALSE, 12);
                            $activeSheet->setCellValue('A' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
            
                            $activeSheet->mergeCells('E' . $exportFileData['row'] . ':G' . $exportFileData['row']);
                            $objRichText = 0;
                            $this->setSheetStyle($activeSheet,'E' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                            $activeSheet->setCellValue('E' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
            
                            $activeSheet->mergeCells('H' . $exportFileData['row'] . ':J' . $exportFileData['row']);
                            $objRichText = $exportFileData['card'][0]['apple_pay_amount_with_fee_tip'];
                            $this->setSheetStyle($activeSheet,'H' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                            $activeSheet->setCellValue('H' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
            
                            $activeSheet->mergeCells('K' . $exportFileData['row'] . ':M' . $exportFileData['row']);
                            $objRichText = $exportFileData['card'][0]['apple_pay_amount_with_fee_tip'];
                            $this->setSheetStyle($activeSheet,'K' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                            $activeSheet->setCellValue('K' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
            
                            $activeSheet->mergeCells('N' . $exportFileData['row'] . ':P' . $exportFileData['row']);
                            $objRichText = $exportFileData['card'][0]['apple_pay_count'];
                            $this->setSheetStyle($activeSheet,'N' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                            $activeSheet->setCellValue('N' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
                            $sub_total[11] += $exportFileData['card'][0]['apple_pay_count'];
            
                            $exportFileData['row']++;
                        }
            */
            if ($exportFileData['card'][0]['wristband_amount_with_fee_tip'] != 0)
            {
                $activeSheet->mergeCells('A' . $exportFileData['row'] . ':D' . $exportFileData['row']);
                $objRichText = "Wristbands";
                $this->setSheetStyle($activeSheet,'A' . $exportFileData['row'], NULL, NULL, FALSE, 12);
                $activeSheet->setCellValue('A' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

                $activeSheet->mergeCells('E' . $exportFileData['row'] . ':G' . $exportFileData['row']);
                $objRichText = 0;
                $this->setSheetStyle($activeSheet,'E' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                $activeSheet->setCellValue('E' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

                $activeSheet->mergeCells('H' . $exportFileData['row'] . ':J' . $exportFileData['row']);
                $objRichText = $exportFileData['card'][0]['wristband_amount_with_fee_tip'];
                $this->setSheetStyle($activeSheet,'H' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                $activeSheet->setCellValue('H' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

                $activeSheet->mergeCells('K' . $exportFileData['row'] . ':M' . $exportFileData['row']);
                $objRichText = $exportFileData['card'][0]['wristband_amount_with_fee_tip'];
                $this->setSheetStyle($activeSheet,'K' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                $activeSheet->setCellValue('K' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

                $activeSheet->mergeCells('N' . $exportFileData['row'] . ':P' . $exportFileData['row']);
                $objRichText = $exportFileData['card'][0]['wristband_count'];
                $this->setSheetStyle($activeSheet,'N' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                $activeSheet->setCellValue('N' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
                $sub_total[11] += $exportFileData['card'][0]['wristband_count'];

                $exportFileData['row']++;
            }

            if ($exportFileData['card'][0]['paypal_amount_with_fee_tip'] != 0)
            {
                $activeSheet->mergeCells('A' . $exportFileData['row'] . ':D' . $exportFileData['row']);
                $objRichText = 'PayPal';
                $this->setSheetStyle($activeSheet,'A' . $exportFileData['row'], NULL, NULL, FALSE, 12);
                $activeSheet->setCellValue('A' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

                $activeSheet->mergeCells('E' . $exportFileData['row'] . ':G' . $exportFileData['row']);
                $objRichText = 0;
                $this->setSheetStyle($activeSheet,'E' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                $activeSheet->setCellValue('E' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

                $activeSheet->mergeCells('H' . $exportFileData['row'] . ':J' . $exportFileData['row']);
                $objRichText = $exportFileData['card'][0]['paypal_amount_with_fee_tip'];
                $this->setSheetStyle($activeSheet,'H' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                $activeSheet->setCellValue('H' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

                $activeSheet->mergeCells('K' . $exportFileData['row'] . ':M' . $exportFileData['row']);
                $objRichText = $exportFileData['card'][0]['paypal_amount_with_fee_tip'];
                $this->setSheetStyle($activeSheet,'K' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                $activeSheet->setCellValue('K' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

                $activeSheet->mergeCells('N' . $exportFileData['row'] . ':P' . $exportFileData['row']);
                $objRichText = $exportFileData['card'][0]['paypal_count'];
                $this->setSheetStyle($activeSheet,'N' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                $activeSheet->setCellValue('N' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
                $sub_total[11] += $exportFileData['card'][0]['paypal_count'];

                $exportFileData['row']++;
            }

            if ($exportFileData['card'][0]['directpayment_amount_with_fee_tip'] != 0)
            {
                $activeSheet->mergeCells('A' . $exportFileData['row'] . ':D' . $exportFileData['row']);
                $objRichText = 'Skidata Direct Payment';
                $this->setSheetStyle($activeSheet,'A' . $exportFileData['row'], NULL, NULL, FALSE, 12);
                $activeSheet->setCellValue('A' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

                $activeSheet->mergeCells('E' . $exportFileData['row'] . ':G' . $exportFileData['row']);
                $objRichText = 0;
                $this->setSheetStyle($activeSheet,'E' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                $activeSheet->setCellValue('E' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

                $activeSheet->mergeCells('H' . $exportFileData['row'] . ':J' . $exportFileData['row']);
                $objRichText = $exportFileData['card'][0]['directpayment_amount_with_fee_tip'];
                $this->setSheetStyle($activeSheet,'H' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                $activeSheet->setCellValue('H' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

                $activeSheet->mergeCells('K' . $exportFileData['row'] . ':M' . $exportFileData['row']);
                $objRichText = $exportFileData['card'][0]['directpayment_amount_with_fee_tip'];
                $this->setSheetStyle($activeSheet,'K' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                $activeSheet->setCellValue('K' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

                $activeSheet->mergeCells('N' . $exportFileData['row'] . ':P' . $exportFileData['row']);
                $objRichText = $exportFileData['card'][0]['directpayment_count'];
                $this->setSheetStyle($activeSheet,'N' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                $activeSheet->setCellValue('N' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
                $sub_total[11] += $exportFileData['card'][0]['directpayment_count'];

                $exportFileData['row']++;
            }

            /* Subtotal row */
            $activeSheet->mergeCells('A' . $exportFileData['row'] . ':D' . $exportFileData['row']);
            $objRichText = 'Total';
            $this->setSheetStyle($activeSheet,'A' . $exportFileData['row'], 'BFBFBF', '17214B', FALSE, 12, TRUE);
            $activeSheet->setCellValue('A' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('E' . $exportFileData['row'] . ':G' . $exportFileData['row']);
            $objRichText = $sub_total[0];
            $this->setSheetStyle($activeSheet,'E' . $exportFileData['row'], 'BFBFBF', '17214B', FALSE, 12, TRUE);
            $activeSheet->setCellValue('E' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('H' . $exportFileData['row'] . ':J' . $exportFileData['row']);
            $objRichText = $sub_total[1];
            $this->setSheetStyle($activeSheet,'H' . $exportFileData['row'], 'BFBFBF', '17214B', FALSE, 12, TRUE);
            $activeSheet->setCellValue('H' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('K' . $exportFileData['row'] . ':M' . $exportFileData['row']);
            $objRichText = $sub_total[2];
            $this->setSheetStyle($activeSheet,'K' . $exportFileData['row'], 'BFBFBF', '17214B', FALSE, 12, TRUE);
            $activeSheet->setCellValue('K' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('N' . $exportFileData['row'] . ':P' . $exportFileData['row']);
            $objRichText = $sub_total[11];
            $this->setSheetStyle($activeSheet,'N' . $exportFileData['row'], 'BFBFBF', '17214B', FALSE, 12, TRUE);
            $activeSheet->setCellValue('N' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $exportFileData['row'] += 2;

            /* Cash Breakdown Block */
            $activeSheet->mergeCells('R5:Y5');
            $objRichText = 'Cash Breakdown';
            $this->setSheetStyle($activeSheet,'R5', '595959', 'FFFFFF', FALSE, 20);
            $activeSheet->setCellValue('R5', is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('R6:Y6');
            $this->setSheetStyle($activeSheet,'R6', '595959', 'FFFFFF', FALSE, 12, TRUE);

            /* Parking Passes Block */
            $activeSheet->mergeCells('R9:Y9');
            $objRichText = 'Parking Passes';
            $this->setSheetStyle($activeSheet,'R9', '595959', 'FFFFFF', FALSE, 20);
            $activeSheet->setCellValue('R9', is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('R10:Y10');
            $this->setSheetStyle($activeSheet,'R10', '595959', 'FFFFFF', FALSE, 12, TRUE);

            /*$activeSheet->mergeCells('U6:X6');
            $this->setSheetStyle($activeSheet,'U6', '595959', 'FFFFFF', FALSE, 12, TRUE);*/

            /* Cash value row */
            $activeSheet->mergeCells('R7:S7');
            $objRichText = 'Cash';
            $this->setSheetStyle($activeSheet,'R7', NULL, NULL, FALSE, 12);
            $activeSheet->setCellValue('R7', is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('R11:S11');
            $objRichText = 'Passes';
            $this->setSheetStyle($activeSheet,'R11', NULL, NULL, FALSE, 12);
            $activeSheet->setCellValue('R11', is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('T11:V11');
            $objRichText = $fileViewData['dataForReport']['parkingPassCodes'];
            $activeSheet->setCellValue('T11', is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('T12:V12');
            $objRichText = $fileViewData['dataForReport']['parkingPassCodes'];
            $activeSheet->setCellValue('T12', is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('T7:V7');
            $objRichText = 'Total';
            $this->setSheetStyle($activeSheet,'T7', NULL, NULL, FALSE, 12, TRUE);
            $activeSheet->setCellValue('T7', is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('W7:Y7');
            $objRichText = 'Transactions';
            $this->setSheetStyle($activeSheet,'W7', NULL, NULL, FALSE, 12, TRUE);
            $activeSheet->setCellValue('W7', is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            /* Cash Subtotal */
            $activeSheet->mergeCells('R8:S8');
            $objRichText = 'Subtotal';
            $this->setSheetStyle($activeSheet,'R8', 'BFBFBF', '17214B', FALSE, 12, TRUE);
            $activeSheet->setCellValue('R8', is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('R12:S12');
            $objRichText = 'Subtotal';
            $this->setSheetStyle($activeSheet,'R12', NULL, NULL, FALSE, 12);
            $activeSheet->setCellValue('R12', is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('T8:V8');
            $objRichText = $cash_total;
            $this->setSheetStyle($activeSheet,'T8', 'BFBFBF', '17214B', FALSE, 12, TRUE);
            $activeSheet->setCellValue('T8', is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('W8:Y8');
            $objRichText = $cash_transactions;
            $this->setSheetStyle($activeSheet,'W8', 'BFBFBF', '17214B', FALSE, 12, TRUE);
            $activeSheet->setCellValue('W8', is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
        }

        $sub_total2 = array(0, 0);
        if (count($fileViewData['dataForReport']['report_data']['code']) > 0) 
        {
            /* Code Breakdown Block */
            $activeSheet->mergeCells('A' . $exportFileData['row'] . ':J' . $exportFileData['row']);
            $objRichText = 'Code Breakdown';
            $this->setSheetStyle($activeSheet, 'A' . $exportFileData['row'], '595959', 'FFFFFF', FALSE, 20);
            $activeSheet->setCellValue('A' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
            $exportFileData['row']++;

            $activeSheet->mergeCells('A' . $exportFileData['row'] . ':D' . $exportFileData['row']);
            $objRichText = 'Type';
            $this->setSheetStyle($activeSheet, 'A' . $exportFileData['row'], 'BFBFBF', '17214B', FALSE, 12, TRUE);
            $activeSheet->setCellValue('A' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('E' . $exportFileData['row'] . ':G' . $exportFileData['row']);
            $objRichText = 'Quantity';
            $this->setSheetStyle($activeSheet, 'E' . $exportFileData['row'], 'BFBFBF', '17214B', FALSE, 12, TRUE);
            $activeSheet->setCellValue('E' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('H' . $exportFileData['row'] . ':J' . $exportFileData['row']);
            $objRichText = 'Total';
            $this->setSheetStyle($activeSheet, 'H' . $exportFileData['row'], 'BFBFBF', '17214B', FALSE, 12, TRUE);
            $activeSheet->setCellValue('H' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $exportFileData['row']++;

            /* Code rows */
            foreach ($fileViewData['dataForReport']['report_data']['code'] as $curr_code)
            {
                $curr_code     = get_object_vars($curr_code);

                $activeSheet->mergeCells('A' . $exportFileData['row'] . ':D' . $exportFileData['row']);
                $objRichText = $curr_code['type'];
                $this->setSheetStyle($activeSheet, 'A' . $exportFileData['row'], NULL, NULL, FALSE, 12);
                $activeSheet->setCellValue('A' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

                $activeSheet->mergeCells('E' . $exportFileData['row'] . ':G' . $exportFileData['row']);
                $objRichText = $curr_code['quantity'];
                $sub_total2[0] += $curr_code['quantity'];
                $this->setSheetStyle($activeSheet, 'E' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                $activeSheet->setCellValue('E' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

                $activeSheet->mergeCells('H' . $exportFileData['row'] . ':J' . $exportFileData['row']);
                $objRichText = $curr_code['total'];
                $sub_total2[1] += $curr_code['total'];
                $this->setSheetStyle($activeSheet, 'H' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                $activeSheet->setCellValue('H' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

                $exportFileData['row']++;
            }

            /* Subtotal row */
            $activeSheet->mergeCells('A' . $exportFileData['row'] . ':D' . $exportFileData['row']);
            $objRichText = 'Subtotal';
            $this->setSheetStyle($activeSheet, 'A' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
            $activeSheet->setCellValue('A' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('E' . $exportFileData['row'] . ':G' . $exportFileData['row']);
            $objRichText = $sub_total2[0];
            $this->setSheetStyle($activeSheet, 'E' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
            $activeSheet->setCellValue('E' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $activeSheet->mergeCells('H' . $exportFileData['row'] . ':J' . $exportFileData['row']);
            $objRichText = $sub_total2[1];
            $this->setSheetStyle($activeSheet, 'H' . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
            $activeSheet->setCellValue('H' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

            $exportFileData['row'] += 2;
        }

        $customTenderColumnNames = [];

        // Custom Tenders sub table
        if (count($fileViewData['dataForReport']['report_data']['custom_tenders']) > 0)
        {
            $customTendersQuantitySubTotal = 0;
            $customTendersTotalSubTotal = 0;
            $this->cellWithMerge($activeSheet, $exportFileData['row'], 'Custom Tenders', 'A', 'J', false, '595959', 'FFFFFF', 20);

            $exportFileData['row']++;

            $this->cellWithMerge($activeSheet, $exportFileData['row'], 'Tender Name', 'A', 'D', true, 'BFBFBF', '17214B');
            $this->cellWithMerge($activeSheet, $exportFileData['row'], 'Quantity', 'E', 'G', true, 'BFBFBF', '17214B');
            $this->cellWithMerge($activeSheet, $exportFileData['row'], 'Total', 'H', 'J', true, 'BFBFBF', '17214B');

            $exportFileData['row']++;

            foreach ($fileViewData['dataForReport']['report_data']['custom_tenders'] as $tender)
            {
                $tender     = get_object_vars($tender);

                $customTendersQuantitySubTotal += $tender['quantity'];
                $customTendersTotalSubTotal    += $tender['total'];
                $customTenderColumnNames[]     = $tender['name'];

                $this->cellWithMerge($activeSheet, $exportFileData['row'], $tender['type'], 'A', 'D', false);
                $this->cellWithMerge($activeSheet, $exportFileData['row'], $tender['quantity'], 'E', 'G');
                $this->cellWithMerge($activeSheet, $exportFileData['row'], $tender['total'], 'H', 'J');

                $exportFileData['row']++;
            }

            $this->cellWithMerge($activeSheet, $exportFileData['row'], 'Subtotal', 'A', 'D');
            $this->cellWithMerge($activeSheet, $exportFileData['row'], $customTendersQuantitySubTotal, 'E', 'G');
            $this->cellWithMerge($activeSheet, $exportFileData['row'], $customTendersTotalSubTotal, 'H', 'J');

            $exportFileData['row'] += 2;
        }

        /* Total Tender Breakdown */
        $activeSheet->mergeCells('A' . $exportFileData['row'] . ':U' . $exportFileData['row']);
        $objRichText = 'Total Tender Breakdown';
        $this->setSheetStyle($activeSheet, 'A' . $exportFileData['row'], NULL, NULL, FALSE, 36, TRUE);
        $activeSheet->setCellValue('A' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
        $exportFileData['row']++;

        $columnNames = array_merge(
            [
                'Card Present',
                'Card Not Present',
                'Credit Total',
                'Cash Total',
                'Code Total',
                'Cash/Credit Sales',
                'Credit Tips',
                'Cash Less Credit Tips',
            ], $customTenderColumnNames
        );

        $columnNames = array_merge($columnNames, [
            'Beacon',
            'Apple Pay',
            'Wristband',
            'Pre-Ordered Parking Passes',
            'Stored Value',
            'PayPal',
            'Skidata Direct Payment',
            'Skidata Loaded Value'
        ]);

        $columnNumber = 1;

        $activeSheet->mergeCells(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'] . ':' . Coordinate::stringFromColumnIndex($columnNumber + 2) . $exportFileData['row']);
        $this->setSheetStyle($activeSheet, Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], 'BFBFBF', '17214B', FALSE, 12, TRUE);
        $activeSheet->setCellValue(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], 'Total Tender');
        $columnNumber += 3;

        foreach ($columnNames as $columnName)
        {
            $activeSheet->mergeCells(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'] . ':' . Coordinate::stringFromColumnIndex($columnNumber + 1) . $exportFileData['row']);
            $this->setSheetStyle($activeSheet, Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], 'BFBFBF', '17214B', FALSE, 12, TRUE);
            $activeSheet->setCellValue(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], $columnName);
            $columnNumber += 2;
        }

        if ($card_tax_col != NULL)
        {
            $curr_column = Coordinate::stringFromColumnIndex($columnNumber);
            $subtotal_counter = 9;
            foreach ($card_tax_col as $card_tax => $real_tax_name)
            {
                $card_tax    = str_replace(array('tot.', '`'), '', $card_tax);
                $next_column = $this->getNextColumn($curr_column);
                $curr_column = $this->getNextColumn($next_column);
                $activeSheet->mergeCells($next_column . $exportFileData['row'] . ':' . $curr_column . $exportFileData['row']);
                $objRichText = $real_tax_name;

                if (isset($sub_total[$subtotal_counter]))
                {
                    $sub_total[$subtotal_counter] += $exportFileData['card'][0][$card_tax];
                }
                else
                {
                    $sub_total[$subtotal_counter] = $exportFileData['card'][0][$card_tax];
                }

                $this->setSheetStyle($activeSheet, $next_column . $exportFileData['row'], 'BFBFBF', '17214B', FALSE, 12, TRUE);
                $activeSheet->setCellValue($next_column . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
                $subtotal_counter++;
            }
        }

        $exportFileData['row']++;
        /* total tender breakdown value */
        $columnNumber = 1;

        $activeSheet->mergeCells(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'] . ':' . (Coordinate::stringFromColumnIndex($columnNumber + 2)) . $exportFileData['row']);
        $this->setSheetStyle($activeSheet, Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
        $activeSheet->setCellValue(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], 'Subtotal');
        $columnNumber += 3;


        $rowValues   = [];
        $rowValues[] = $sub_total[3];
        $rowValues[] = $sub_total[4];
        $rowValues[] = $sub_total[4]+$sub_total[3];
        $rowValues[] = $cash_total;
        $rowValues[] = $sub_total2[1];
        $rowValues[] = $sub_total[5] + $cash_total;
        $rowValues[] = $credit_tips;
        $rowValues[] = $cash_total - $credit_tips;
        $rowValues   = array_merge($rowValues, $this->getTotalCustomTenderCell($fileViewData['dataForReport']['report_data']['custom_tenders'], $exportFileData['card'][0]));
        $rowValues[] = $sub_total[6];
        $rowValues[] = $sub_total[7];
        $rowValues[] = $sub_total[8];
        $rowValues[] = $fileViewData['dataForReport']['parkingPassCodes'];
        $rowValues[] = $exportFileData['card'][0]['givex_amount'];
        $rowValues[] = $exportFileData['card'][0]['paypal_amount'];
        $rowValues[] = $exportFileData['card'][0]['directpayment_amount'];
        $rowValues[] = $exportFileData['card'][0]['loadedvalue_amount'];

        foreach ($rowValues as $value) 
        {
            $activeSheet->mergeCells(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'] . ':' . (Coordinate::stringFromColumnIndex($columnNumber + 1)) . $exportFileData['row']);
            $this->setSheetStyle($activeSheet, Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
            $activeSheet->setCellValue(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], is_numeric($value) ? round($value, 2) : $value);
            $columnNumber += 2;
        }

        if ($card_tax_col != NULL)
        {
            $curr_column = Coordinate::stringFromColumnIndex($columnNumber);
            $subtotal_counter = 9;
            foreach ($card_tax_col as $card_tax => $real_tax_name)
            {
                $card_tax    = str_replace(array('tot.', '`'), '', $card_tax);
                $next_column = $this->getNextColumn($curr_column);
                $curr_column = $this->getNextColumn($next_column);
                $activeSheet->mergeCells($next_column . $exportFileData['row'] . ':' . $curr_column . $exportFileData['row']);
                $objRichText = $sub_total[$subtotal_counter];
                $this->setSheetStyle($activeSheet, $next_column . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                $activeSheet->setCellValue($next_column . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
                $subtotal_counter++;
            }
        }

        $exportFileData['row'] += 2;

        /* Venue / Vendor Breakdown */
        $v_types = ['venue', 'vendor_group', 'vendor'];
        /* Don't render vendor breakdown for corporate report */
        $corporate = false;
        if (isset($fileViewData['dataForReport']['report_data']['venue'])) 
        {
            $v_types = array('venue');
            $corporate = true;
        }

        //dd($fileViewData);

        foreach ($v_types as $v_type) 
        {
            $card_tax_col                                           =   isset($fileViewData['dataForReport']['report_data'][$v_type][1]) ? $fileViewData['dataForReport']['report_data'][$v_type][1] : null;
            $fileViewData['dataForReport']['report_data'][$v_type]  =   isset($fileViewData['dataForReport']['report_data'][$v_type][0]) ? $fileViewData['dataForReport']['report_data'][$v_type][0] : null;
            
            if (isset($fileViewData['dataForReport']['report_data'][$v_type]) && count($fileViewData['dataForReport']['report_data'][$v_type]) > 0) 
            {
                $activeSheet->mergeCells('A' . $exportFileData['row'] . ':X' . $exportFileData['row']);
                $objRichText = ($v_type == 'vendor_group' ? 'Vendor Group' : ucfirst($v_type)) . ' Tender Type';
                $this->setSheetStyle($activeSheet, 'A' . $exportFileData['row'], NULL, NULL, FALSE, 36, TRUE);
                $activeSheet->setCellValue('A' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
                $exportFileData['row']++;

                // START: vendor/vendor_group header
                $columnNumber = 1;

                $activeSheet->mergeCells(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'] . ':' . Coordinate::stringFromColumnIndex($columnNumber + 3) . $exportFileData['row']);
                $objRichText = ($v_type == 'vendor_group' ? 'Vendor Group' : ucfirst($v_type)) . ' Name';
                $this->setSheetStyle($activeSheet, Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], 'BFBFBF', '17214B', FALSE, 12, TRUE);
                $activeSheet->setCellValue(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], $objRichText);
                $columnNumber += 4;

                $columnNames = $this->buildTenderTypeColumnNames($customTenderColumnNames);

                $columnNames           = array_merge($columnNames, ['Beacon', 'Apple Pay', 'Wristband',
                    'Cash & Credit Taxes', 'Pre-Ordered Parking Passes', 'Stored Value', 'Stored Value Taxes', 'Stored Value Tips',
                    'Cash/Credit Sales Taxes', 'Cash/Credit Sales Tips', 'PayPal', 'Skidata Direct Payment', 'Skidata Loaded Value']);

                foreach ($columnNames as $columnName) 
                {
                    $activeSheet->mergeCells(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'] . ':' . Coordinate::stringFromColumnIndex($columnNumber + 1) . $exportFileData['row']);
                    $this->setSheetStyle($activeSheet, Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], 'BFBFBF', '17214B', FALSE, 12, TRUE);
                    $activeSheet->setCellValue(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], $columnName);
                    $columnNumber += 2;
                }

                if ($card_tax_col != NULL) 
                {
                    $curr_column = Coordinate::stringFromColumnIndex($columnNumber);
                    foreach ($card_tax_col as $card_tax => $real_tax_name)
                    {
                        $card_tax    = str_replace(array('tot.', '`'), '', $card_tax);
                        $next_column = $this->getNextColumn($curr_column);
                        $curr_column = $this->getNextColumn($next_column);
                        $activeSheet->mergeCells($next_column . $exportFileData['row'] . ':' . $curr_column . $exportFileData['row']);
                        $objRichText = $real_tax_name;
                        $this->setSheetStyle($activeSheet, $next_column . $exportFileData['row'], 'BFBFBF', '17214B', FALSE, 12, TRUE);
                        $activeSheet->setCellValue($next_column . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
                    }
                    // END: vendor/vendor_group header
                }

                $exportFileData['row']++;
                $sub_total = [];
                foreach ($fileViewData['dataForReport']['report_data'][$v_type] as $curr_vendor) 
                {
                    $curr_vendor     = get_object_vars($curr_vendor);

                    $subTotalIndex = 0;
                    $columnNumber = 1;
                    $activeSheet->mergeCells(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'] . ':' . Coordinate::stringFromColumnIndex($columnNumber + 3) . $exportFileData['row']);
                    $objRichText = $v_type == 'vendor_group' ? $curr_vendor['vg_name'] : $curr_vendor[$v_type . '_name'];
                    $this->setSheetStyle($activeSheet, 'A' . $exportFileData['row'], NULL, NULL, TRUE, 20);
                    $activeSheet->setCellValue('A' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
                    $columnNumber += 4;

                    $rowValues = [];
                    $rowValues[] = $curr_vendor['cp_amount'];
                    $rowValues[] = $curr_vendor['cnp_amount'];
                    $rowValues[] = $curr_vendor['credit_total_with_fee_tip'] - $curr_vendor['total_credit_tips'];
                    $rowValues[] = $curr_vendor['cash_total_with_fee_tip'] - $curr_vendor['total_cash_tips'];
                    $rowValues[] = $curr_vendor['discount'];
                    $rowValues[] = $curr_vendor['house_account_amount'];
                    $rowValues[] = $curr_vendor['submit_with_no_payment_amount'];
                    $rowValues[] = $curr_vendor['total_credit_fees'] + $curr_vendor['total_cash_fees'];
                    $rowValues[] = $curr_vendor['custom_fees'];
                    $rowValues[] = $curr_vendor['cash_total_with_fee_tip'] + $curr_vendor['credit_total_with_fee_tip'] - $curr_vendor['total_cash_tips'] - $curr_vendor['total_credit_tips'];
                    $rowValues[] = $curr_vendor['total_credit_tips'];
                    $rowValues[] = $curr_vendor['total_wristband_tips'];
                    $rowValues[] = $curr_vendor['tip'];
                    $rowValues[] = $curr_vendor['cash_total_with_fee_tip'] - $curr_vendor['total_cash_tips'] - $curr_vendor['total_credit_tips'];
                    $rowValues   = array_merge($rowValues, $this->getTotalCustomTenderCell($fileViewData['dataForReport']['report_data']['custom_tenders'], $curr_vendor));
                    $rowValues[] = $curr_vendor['beacon_amount'];
                    $rowValues[] = $curr_vendor['apple_pay_amount'];
                    $rowValues[] = $curr_vendor['wristband_amount'];
                    $rowValues[] = $curr_vendor['total_credit_taxes'] + $curr_vendor['total_cash_taxes'];

                    if ($corporate) 
                    {
                        $value = key_exists($curr_vendor['vu_id'], $fileViewData['dataForReport']['parkingPassCodesByVenue']) ? $fileViewData['dataForReport']['parkingPassCodesByVenue'][$curr_vendor['vu_id']] : 0;
                    } 
                    else 
                    {
                        $curr_vendor['v_id'] = isset($curr_vendor['v_id']) ? $curr_vendor['v_id'] : null;
                        $value = key_exists($curr_vendor['v_id'], $fileViewData['dataForReport']['parkingPassCodesByVendor']) ? $fileViewData['dataForReport']['parkingPassCodesByVendor'][$curr_vendor['v_id']] : 0;
                        if ($v_type == "vendor_group") 
                        {
                            $value = key_exists($curr_vendor['vg_id'], $fileViewData['dataForReport']['parkingPassCodesByVendorGroup']) ? $fileViewData['dataForReport']['parkingPassCodesByVendorGroup'][$curr_vendor['vg_id']] : 0;
                        }
                    }
                    $rowValues[] = $value;

                    foreach ($rowValues as $value) 
                    {
                        $activeSheet->mergeCells(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'] . ':' . (Coordinate::stringFromColumnIndex($columnNumber + 1)) . $exportFileData['row']);
                        $this->setSheetStyle($activeSheet, Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                        $activeSheet->setCellValue(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], is_numeric($value) ? round($value, 2) : $value);
                        $columnNumber += 2;

                        if (!isset($sub_total[$subTotalIndex])) 
                        {
                            $sub_total[$subTotalIndex] = 0;
                        }

                        $sub_total[$subTotalIndex] += $value;
                        $subTotalIndex++;
                    }

                    if ($card_tax_col != NULL) 
                    {
                        $curr_column      = Coordinate::stringFromColumnIndex($columnNumber);
                        foreach ($card_tax_col as $card_tax => $real_tax_name) 
                        {
                            $card_tax    = str_replace(array('tot.', '`'), '', $card_tax);
                            $next_column = $this->getNextColumn($curr_column);
                            $curr_column = $this->getNextColumn($next_column);
                            $activeSheet->mergeCells($next_column . $exportFileData['row'] . ':' . $curr_column . $exportFileData['row']);
                            $objRichText = $curr_vendor[$card_tax];

                            if (!isset($sub_total[$subTotalIndex]))
                            {
                                $sub_total[$subTotalIndex] = 0;
                            }

                            if (isset($sub_total[$subTotalIndex]))
                            {
                                $sub_total[$subTotalIndex] += $curr_vendor[$card_tax];
                            }
                            else
                            {
                                $sub_total[$subTotalIndex] = $curr_vendor[$card_tax];
                            }

                            $this->setSheetStyle($activeSheet, $next_column . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                            $activeSheet->setCellValue($next_column . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

                            $subTotalIndex++;
                        }

                        $curr_column += 2;
                    }

                    $rowValues = [];
                    $rowValues[] = $curr_vendor['givex_amount'];
                    $rowValues[] = $curr_vendor['total_stored_value_taxes'];
                    $rowValues[] = $curr_vendor['total_stored_value_tips'];
                    $rowValues[] = $curr_vendor['total_house_taxes'];
                    $rowValues[] = $curr_vendor['total_house_tips'];
                    $rowValues[] = $curr_vendor['paypal_amount'];
                    $rowValues[] = $curr_vendor['directpayment_amount'];
                    $rowValues[] = $curr_vendor['loadedvalue_amount'];

                    foreach ($rowValues as $value) 
                    {
                        $activeSheet->mergeCells(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'] . ':' . (Coordinate::stringFromColumnIndex($columnNumber + 1)) . $exportFileData['row']);
                        $this->setSheetStyle($activeSheet, Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                        $activeSheet->setCellValue(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], is_numeric($value) ? round($value, 2) : $value);
                        $columnNumber += 2;

                        if (!isset($sub_total[$subTotalIndex]))
                        {
                            $sub_total[$subTotalIndex] = 0;
                        }

                        $sub_total[$subTotalIndex] += $value;
                        $subTotalIndex++;
                    }

                    $exportFileData['row']++;
                }
                
                
                /* SubTotal vendor */
                if ($v_type != 'vendor_group') 
                {
                    $columnNumber = 1;
                    $activeSheet->mergeCells(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'] . ':' . Coordinate::stringFromColumnIndex($columnNumber + 3) . $exportFileData['row']);
                    $objRichText = 'Totals';
                    $this->setSheetStyle($activeSheet, Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], NULL, NULL, TRUE, 20);
                    $activeSheet->setCellValue(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
                    $columnNumber += 4;

                    for ($i = 0; $i < $subTotalIndex; $i++) 
                    {
                        $activeSheet->mergeCells(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'] . ':' . Coordinate::stringFromColumnIndex($columnNumber + 1) . $exportFileData['row']);
                        $objRichText = $sub_total[$i];
                        $this->setSheetStyle($activeSheet, Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                        $activeSheet->setCellValue(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
                        $columnNumber += 2;
                    }

                    if ($card_tax_col != NULL) 
                    {
                        $curr_column      = Coordinate::stringFromColumnIndex($columnNumber);
                        foreach ($card_tax_col as $card_tax => $real_tax_name) 
                        {
                            $card_tax    = str_replace(array('tot.', '`'), '', $card_tax);
                            $next_column = $this->getNextColumn($curr_column);
                            $curr_column = $this->getNextColumn($next_column);
                            $activeSheet->mergeCells($next_column . $exportFileData['row'] . ':' . $curr_column . $exportFileData['row']);
                            $objRichText = $sub_total[$subTotalIndex];

                            $this->setSheetStyle($activeSheet, $next_column . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                            $activeSheet->setCellValue($next_column . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

                            $subTotalIndex++;
                        }
                    }
                }

                $exportFileData['row'] += 2;
            } 
            /* end of if vendor/venue breakdown */
        }
        /* end of foreach vendor/venue breakdown */

        //dd($fileViewData);
        
        /* Account group breakdown */
        if (isset($fileViewData['dataForReport']['report_data']['account_group'])) 
        {
            $card_tax_col                                                   =   $fileViewData['dataForReport']['report_data']['account_group'][1];
            $fileViewData['dataForReport']['report_data']['account_group']  =   $fileViewData['dataForReport']['report_data']['account_group'][0];
            
            if (isset($fileViewData['dataForReport']['report_data']['account_group']) && count($fileViewData['dataForReport']['report_data']['account_group']) > 0) 
            {
                $activeSheet->mergeCells('A' . $exportFileData['row'] . ':X' . $exportFileData['row']);
                $objRichText = 'Account Group Tender Type';
                $this->setSheetStyle($activeSheet, 'A' . $exportFileData['row'], NULL, NULL, FALSE, 36, TRUE);
                $activeSheet->setCellValue('A' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
                $exportFileData['row']++;

                // START: account group header
                $columnNumber = 1;

                $activeSheet->mergeCells(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'] . ':' . Coordinate::stringFromColumnIndex($columnNumber + 3) . $exportFileData['row']);
                $objRichText = 'ACCOUNT GROUP NAME';
                $this->setSheetStyle($activeSheet, Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], 'BFBFBF', '17214B', FALSE, 12, TRUE);
                $activeSheet->setCellValue(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], $objRichText);
                $columnNumber += 4;

                $columnNames = $this->buildTenderTypeColumnNames($customTenderColumnNames);

                $columnNames     = array_merge($columnNames, ['Beacon', 'Apple Pay', 'Wristband',
                    'Cash & Credit Taxes', 'Stored Value', 'Stored Value Taxes', 'Stored Value Tips',
                    'Cash/Credit Sales Taxes', 'Cash/Credit Sales Tips', 'PayPal', 'Skidata Direct Payment', 'Skidata Loaded Value']);

                foreach ($columnNames as $columnName) 
                {
                    $activeSheet->mergeCells(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'] . ':' . Coordinate::stringFromColumnIndex($columnNumber + 1) . $exportFileData['row']);
                    $this->setSheetStyle($activeSheet, Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], 'BFBFBF', '17214B', FALSE, 12, TRUE);
                    $activeSheet->setCellValue(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], $columnName);
                    $columnNumber += 2;
                }
                
                if ($card_tax_col != NULL) 
                {
                    $curr_column = Coordinate::stringFromColumnIndex($columnNumber);
                    foreach ($card_tax_col as $card_tax => $real_tax_name) 
                    {
                        $card_tax    = str_replace(['tot.', '`'], '', $card_tax);
                        $next_column = $this->getNextColumn($curr_column);
                        $curr_column = $this->getNextColumn($next_column);
                        $activeSheet->mergeCells($next_column . $exportFileData['row'] . ':' . $curr_column . $exportFileData['row']);
                        $objRichText = $real_tax_name;
                        $this->setSheetStyle($activeSheet, $next_column . $exportFileData['row'], 'BFBFBF', '17214B', FALSE, 12, TRUE);
                        $activeSheet->setCellValue($next_column . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
                    }
                }
                // END: account group header

                $exportFileData['row']++;
                $sub_total = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                foreach ($fileViewData['dataForReport']['report_data']['account_group'] as $curr_vendor)
                {
                    $curr_vendor     = get_object_vars($curr_vendor);

                    $subTotalIndex = 0;
                    $columnNumber = 1;
                    $activeSheet->mergeCells(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'] . ':' . Coordinate::stringFromColumnIndex($columnNumber + 3) . $exportFileData['row']);
                    $objRichText = $curr_vendor['ag_name'];
                    $this->setSheetStyle($activeSheet, 'A' . $exportFileData['row'], NULL, NULL, TRUE, 20);
                    $activeSheet->setCellValue('A' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
                    $columnNumber += 4;

                    $rowValues = [];

                    $rowValues[] = $curr_vendor['cp_amount'];
                    $rowValues[] = $curr_vendor['cnp_amount'];
                    $rowValues[] = $curr_vendor['credit_total_with_fee_tip'] - $curr_vendor['total_credit_tips'];
                    $rowValues[] = $curr_vendor['cash_total_with_fee_tip'] - $curr_vendor['total_cash_tips'];
                    $rowValues[] = $curr_vendor['discount'];
                    $rowValues[] = $curr_vendor['house_account_amount'];
                    $rowValues[] = $curr_vendor['submit_with_no_payment_amount'];
                    $rowValues[] = $curr_vendor['total_credit_fees'] + $curr_vendor['total_cash_fees'];
                    $rowValues[] = $curr_vendor['custom_fees'];
                    $rowValues[] = $curr_vendor['cash_total_with_fee_tip'] + $curr_vendor['credit_total_with_fee_tip'] - $curr_vendor['total_cash_tips'] - $curr_vendor['total_credit_tips'];
                    $rowValues[] = $curr_vendor['total_credit_tips'];
                    $rowValues[] = $curr_vendor['total_wristband_tips'];
                    $rowValues[] = $curr_vendor['tip'];
                    $rowValues[] = $curr_vendor['cash_total_with_fee_tip'] - $curr_vendor['total_cash_tips'] - $curr_vendor['total_credit_tips'];
                    $rowValues   = array_merge($rowValues, $this->getTotalCustomTenderCell($fileViewData['dataForReport']['report_data']['custom_tenders'], $curr_vendor));
                    $rowValues[] = $curr_vendor['beacon_amount'];
                    $rowValues[] = $curr_vendor['apple_pay_amount'];
                    $rowValues[] = $curr_vendor['wristband_amount'];
                    $rowValues[] = $curr_vendor['total_credit_taxes'] + $curr_vendor['total_cash_taxes'];
                    $rowValues[] = $curr_vendor['givex_amount'];
                    $rowValues[] = $curr_vendor['total_stored_value_taxes'];
                    $rowValues[] = $curr_vendor['total_stored_value_tips'];
                    $rowValues[] = $curr_vendor['total_house_taxes'];
                    $rowValues[] = $curr_vendor['total_house_tips'];
                    $rowValues[] = $curr_vendor['paypal_amount'];
                    $rowValues[] = $curr_vendor['directpayment_amount'];
                    $rowValues[] = $curr_vendor['loadedvalue_amount'];

                    foreach ($rowValues as $value)
                    {
                        $activeSheet->mergeCells(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'] . ':' . (Coordinate::stringFromColumnIndex($columnNumber + 1)) . $exportFileData['row']);
                        $this->setSheetStyle($activeSheet, Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                        $activeSheet->setCellValue(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], is_numeric($value) ? round($value, 2) : $value);
                        $columnNumber += 2;

                        $sub_total[$subTotalIndex] += $value;
                        $subTotalIndex++;
                    }

                    if ($card_tax_col != NULL)
                    {
                        //$subtotal_counter = 19;
                        $curr_column = Coordinate::stringFromColumnIndex($columnNumber);
                        foreach ($card_tax_col as $card_tax => $real_tax_name)
                        {
                            $card_tax    = str_replace(array('tot.', '`'), '', $card_tax);
                            $next_column = $this->getNextColumn($curr_column);
                            $curr_column = $this->getNextColumn($next_column);
                            $activeSheet->mergeCells($next_column . $exportFileData['row'] . ':' . $curr_column . $exportFileData['row']);
                            $objRichText = $curr_vendor[$card_tax];

                            if (isset($sub_total[$subTotalIndex]))
                            {
                                $sub_total[$subTotalIndex] += $curr_vendor[$card_tax];
                            }
                            else
                            {
                                $sub_total[$subTotalIndex] = $curr_vendor[$card_tax];
                            }

                            $this->setSheetStyle($activeSheet, $next_column . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                            $activeSheet->setCellValue($next_column . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

                            $subTotalIndex++;
                        }
                    }

                    $exportFileData['row']++;
                }

                $exportFileData['row'] += 2;
            }
        }

        /* Account breakdown */
        if (isset($fileViewData['dataForReport']['report_data']['account']))
        {
            $card_tax_col                                               =   $fileViewData['dataForReport']['report_data']['account'][1];
            $fileViewData['dataForReport']['report_data']['account']    =   $fileViewData['dataForReport']['report_data']['account'][0];

            if (isset($fileViewData['dataForReport']['report_data']['account']) && count($fileViewData['dataForReport']['report_data']['account']) > 0)
            {
                $activeSheet->mergeCells('A' . $exportFileData['row'] . ':X' . $exportFileData['row']);
                $objRichText = 'Account Tender Type';
                $this->setSheetStyle($activeSheet, 'A' . $exportFileData['row'], NULL, NULL, FALSE, 36, TRUE);
                $activeSheet->setCellValue('A' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
                $exportFileData['row']++;

                // START: account header
                $columnNumber = 1;

                $activeSheet->mergeCells(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'] . ':' . Coordinate::stringFromColumnIndex($columnNumber + 3) . $exportFileData['row']);
                $objRichText = 'ACCOUNT NAME';
                $this->setSheetStyle($activeSheet, Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], 'BFBFBF', '17214B', FALSE, 12, TRUE);
                $activeSheet->setCellValue(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], $objRichText);
                $columnNumber += 4;

                $columnNames = $this->buildTenderTypeColumnNames($customTenderColumnNames);

                $columnNames           = array_merge($columnNames, ['Beacon', 'Apple Pay', 'Wristband',
                    'Cash & Credit Taxes', 'Stored Value', 'Stored Value Taxes', 'Stored Value Tips',
                    'Cash/Credit Sales Taxes', 'Cash/Credit Sales Tips', 'PayPal', 'Skidata Direct Payment', 'Skidata Loaded Value']);

                foreach ($columnNames as $columnName)
                {
                    $activeSheet->mergeCells(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'] . ':' . Coordinate::stringFromColumnIndex($columnNumber + 1) . $exportFileData['row']);
                    $this->setSheetStyle($activeSheet, Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], 'BFBFBF', '17214B', FALSE, 12, TRUE);
                    $activeSheet->setCellValue(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], $columnName);
                    $columnNumber += 2;
                }

                if ($card_tax_col != NULL)
                {
                    $curr_column = Coordinate::stringFromColumnIndex($columnNumber);
                    foreach ($card_tax_col as $card_tax => $real_tax_name)
                    {
                        $card_tax    = str_replace(array('tot.', '`'), '', $card_tax);
                        $next_column = $this->getNextColumn($curr_column);
                        $curr_column = $this->getNextColumn($next_column);
                        $activeSheet->mergeCells($next_column . $exportFileData['row'] . ':' . $curr_column . $exportFileData['row']);
                        $objRichText = $real_tax_name;
                        $this->setSheetStyle($activeSheet, $next_column . $exportFileData['row'], 'BFBFBF', '17214B', FALSE, 12, TRUE);
                        $activeSheet->setCellValue($next_column . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
                    }
                }
                // END: account header

                $exportFileData['row']++;
                $sub_total = [];

                foreach ($fileViewData['dataForReport']['report_data']['account'] as $curr_vendor)
                {
                    $curr_vendor     = get_object_vars($curr_vendor);

                    $subTotalIndex = 0;
                    $columnNumber = 1;
                    $activeSheet->mergeCells(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'] . ':' . Coordinate::stringFromColumnIndex($columnNumber + 3) . $exportFileData['row']);
                    $objRichText = $curr_vendor['name'];
                    $this->setSheetStyle($activeSheet, 'A' . $exportFileData['row'], NULL, NULL, TRUE, 20);
                    $activeSheet->setCellValue('A' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
                    $columnNumber += 4;

                    $rowValues = [];
                    $rowValues[] = $curr_vendor['cp_amount'];
                    $rowValues[] = $curr_vendor['cnp_amount'];
                    $rowValues[] = $curr_vendor['credit_total_with_fee_tip'] - $curr_vendor['total_credit_tips'];
                    $rowValues[] = $curr_vendor['cash_total_with_fee_tip'] - $curr_vendor['total_cash_tips'];
                    $rowValues[] = $curr_vendor['discount'];
                    $rowValues[] = $curr_vendor['house_account_amount'];
                    $rowValues[] = $curr_vendor['submit_with_no_payment_amount'];
                    $rowValues[] = $curr_vendor['total_credit_fees'] + $curr_vendor['total_cash_fees'];
                    $rowValues[] = $curr_vendor['custom_fees'];
                    $rowValues[] = $curr_vendor['cash_total_with_fee_tip'] + $curr_vendor['credit_total_with_fee_tip'] - $curr_vendor['total_cash_tips'] - $curr_vendor['total_credit_tips'];
                    $rowValues[] = $curr_vendor['total_credit_tips'];
                    $rowValues[] = $curr_vendor['total_wristband_tips'];
                    $rowValues[] = $curr_vendor['tip'];
                    $rowValues[] = $curr_vendor['cash_total_with_fee_tip'] - $curr_vendor['total_cash_tips'] - $curr_vendor['total_credit_tips'];
                    $rowValues   = array_merge($rowValues, $this->getTotalCustomTenderCell($fileViewData['dataForReport']['report_data']['custom_tenders'], $curr_vendor));
                    $rowValues[] = $curr_vendor['beacon_amount'];
                    $rowValues[] = $curr_vendor['apple_pay_amount'];
                    $rowValues[] = $curr_vendor['wristband_amount'];
                    $rowValues[] = $curr_vendor['total_credit_taxes'] + $curr_vendor['total_cash_taxes'];
                    $rowValues[] = $curr_vendor['givex_amount'];
                    $rowValues[] = $curr_vendor['total_stored_value_taxes'];
                    $rowValues[] = $curr_vendor['total_stored_value_tips'];
                    $rowValues[] = $curr_vendor['total_house_taxes'];
                    $rowValues[] = $curr_vendor['total_house_tips'];
                    $rowValues[] = $curr_vendor['paypal_amount'];
                    $rowValues[] = $curr_vendor['directpayment_amount'];
                    $rowValues[] = $curr_vendor['loadedvalue_amount'];

                    foreach ($rowValues as $value)
                    {
                        $activeSheet->mergeCells(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'] . ':' . (Coordinate::stringFromColumnIndex($columnNumber + 1)) . $exportFileData['row']);
                        $this->setSheetStyle($activeSheet, Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                        $activeSheet->setCellValue(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], is_numeric($value) ? round($value, 2) : $value);
                        $columnNumber += 2;

                        if (!isset($sub_total[$subTotalIndex])) {
                            $sub_total[$subTotalIndex] = 0;
                        }

                        $sub_total[$subTotalIndex] += $value;
                        $subTotalIndex++;
                    }


                    if ($card_tax_col != NULL)
                    {
                        //$subtotal_counter = 20;
                        $curr_column      = Coordinate::stringFromColumnIndex($columnNumber);
                        foreach ($card_tax_col as $card_tax => $real_tax_name)
                        {
                            $card_tax    = str_replace(array('tot.', '`'), '', $card_tax);
                            $next_column = $this->getNextColumn($curr_column);
                            $curr_column = $this->getNextColumn($next_column);
                            $activeSheet->mergeCells($next_column . $exportFileData['row'] . ':' . $curr_column . $exportFileData['row']);
                            $objRichText = $curr_vendor[$card_tax];

                            if (!isset($sub_total[$subTotalIndex]))
                            {
                                $sub_total[$subTotalIndex] = 0;
                            }

                            if (isset($sub_total[$subTotalIndex]))
                            {
                                $sub_total[$subTotalIndex] += $curr_vendor[$card_tax];
                            }
                            else
                            {
                                $sub_total[$subTotalIndex] = $curr_vendor[$card_tax];
                            }

                            $this->setSheetStyle($activeSheet, $next_column . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                            $activeSheet->setCellValue($next_column . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

                            $subTotalIndex++;
                        }
                    }

                    $exportFileData['row']++;
                }

                /* SubTotal account */
                $columnNumber = 1;
                $activeSheet->mergeCells(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'] . ':' . Coordinate::stringFromColumnIndex($columnNumber + 3) . $exportFileData['row']);
                $objRichText = 'Totals';
                $this->setSheetStyle($activeSheet, Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], NULL, NULL, TRUE, 20);
                $activeSheet->setCellValue(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
                $columnNumber += 4;

                for ($i = 0; $i < $subTotalIndex; $i++)
                {
                    $activeSheet->mergeCells(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'] . ':' . Coordinate::stringFromColumnIndex($columnNumber + 1) . $exportFileData['row']);
                    $objRichText = $sub_total[$i];
                    $this->setSheetStyle($activeSheet, Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                    $activeSheet->setCellValue(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
                    $columnNumber += 2;
                }

                if ($card_tax_col != NULL)
                {
                    $subtotal_counter = Coordinate::stringFromColumnIndex($columnNumber);
                    $curr_column      = Coordinate::stringFromColumnIndex($columnNumber);
                    foreach ($card_tax_col as $card_tax => $real_tax_name)
                    {
                        $card_tax    = str_replace(array('tot.', '`'), '', $card_tax);
                        $next_column = $this->getNextColumn($curr_column);
                        $curr_column = $this->getNextColumn($next_column);
                        $activeSheet->mergeCells($next_column . $exportFileData['row'] . ':' . $curr_column . $exportFileData['row']);
                        $objRichText = $sub_total[$subtotal_counter];

                        $this->setSheetStyle($activeSheet, $next_column . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                        $activeSheet->setCellValue($next_column . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

                        $subtotal_counter++;
                    }
                }

                $exportFileData['row'] += 2;
            }
        }

        /* Accounts by Vendor breakdown */
        if (isset($fileViewData['dataForReport']['report_data']['account_by_vendor']))
        {
            $card_tax_col                                                       =   $fileViewData['dataForReport']['report_data']['account_by_vendor'][1];
            $fileViewData['dataForReport']['report_data']['account_by_vendor']  =   $fileViewData['dataForReport']['report_data']['account_by_vendor'][0];

            if (isset($fileViewData['dataForReport']['report_data']['account_by_vendor']) && count($fileViewData['dataForReport']['report_data']['account_by_vendor']) > 0)
            {
                $activeSheet->mergeCells('A' . $exportFileData['row'] . ':X' . $exportFileData['row']);
                $objRichText = 'Accounts By Vendor Tender Type';
                $this->setSheetStyle($activeSheet, 'A' . $exportFileData['row'], NULL, NULL, FALSE, 36, TRUE);
                $activeSheet->setCellValue('A' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
                $exportFileData['row']++;

                $current_vendor_name = '';
                $sub_total = [];
                foreach ($fileViewData['dataForReport']['report_data']['account_by_vendor'] as $curr_vendor)
                {
                    $curr_vendor     = get_object_vars($curr_vendor);

                    if ($curr_vendor['vendor_name'] != $current_vendor_name)
                    {
                        if ($current_vendor_name != '')
                        {
                            $columnNumber = 1;
                            $activeSheet->mergeCells(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'] . ':' . Coordinate::stringFromColumnIndex($columnNumber + 3) . $exportFileData['row']);
                            $objRichText = 'Vendor Tender Totals';
                            $this->setSheetStyle($activeSheet, Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], NULL, NULL, TRUE, 20);
                            $activeSheet->setCellValue(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
                            $columnNumber += 4;

                            for ($i = 0; $i < $subTotalIndex; $i++)
                            {
                                $activeSheet->mergeCells(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'] . ':' . Coordinate::stringFromColumnIndex($columnNumber + 1) . $exportFileData['row']);
                                $objRichText = $sub_total[$i];
                                $this->setSheetStyle($activeSheet, Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                                $activeSheet->setCellValue(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
                                $columnNumber += 2;
                            }

                            if ($card_tax_col != NULL)
                            {
                                //$subtotal_counter = 20;
                                $curr_column      = Coordinate::stringFromColumnIndex($columnNumber);
                                foreach ($card_tax_col as $card_tax => $real_tax_name)
                                {
                                    $card_tax    = str_replace(array('tot.', '`'), '', $card_tax);
                                    $next_column = $this->getNextColumn($curr_column);
                                    $curr_column = $this->getNextColumn($next_column);
                                    $activeSheet->mergeCells($next_column . $exportFileData['row'] . ':' . $curr_column . $exportFileData['row']);
                                    $objRichText = $sub_total[$subtotal_counter];

                                    $this->setSheetStyle($activeSheet, $next_column . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                                    $activeSheet->setCellValue($next_column . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

                                    $subtotal_counter++;
                                }
                            }

                            $exportFileData['row']++;

                            $sub_total = [];
                            $exportFileData['row']++;
                        }
                        $current_vendor_name = $curr_vendor['vendor_name'];

                        $activeSheet->mergeCells('A' . $exportFileData['row'] . ':D' . $exportFileData['row']);
                        $objRichText = 'Vendor Name: ' . $curr_vendor['vendor_name'];
                        $this->setSheetStyle($activeSheet, 'A' . $exportFileData['row'], NULL, NULL, TRUE, 20);
                        $activeSheet->setCellValue('A' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

                        $exportFileData['row']++;

                        // START: account group header
                        $columnNumber = 1;

                        $activeSheet->mergeCells(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'] . ':' . Coordinate::stringFromColumnIndex($columnNumber + 3) . $exportFileData['row']);
                        $objRichText = 'ACCOUNT NAME';
                        $this->setSheetStyle($activeSheet, Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], 'BFBFBF', '17214B', FALSE, 12, TRUE);
                        $activeSheet->setCellValue(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], $objRichText);
                        $columnNumber += 4;

                        $columnNames = $this->buildTenderTypeColumnNames($customTenderColumnNames);

                        $columnNames = array_merge($columnNames, ['Beacon', 'Apple Pay', 'Wristband',
                            'Cash & Credit Taxes', 'Stored Value', 'Stored Value Taxes', 'Stored Value Tips',
                            'Cash/Credit Sales Taxes', 'Cash/Credit Sales Tips', 'PayPal', 'Skidata Direct Payment', 'Skidata Loaded Value']);

                        foreach ($columnNames as $columnName) {
                            $activeSheet->mergeCells(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'] . ':' . Coordinate::stringFromColumnIndex($columnNumber + 1) . $exportFileData['row']);
                            $this->setSheetStyle($activeSheet, Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], 'BFBFBF', '17214B', FALSE, 12, TRUE);
                            $activeSheet->setCellValue(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], $columnName);
                            $columnNumber += 2;
                        }

                        if ($card_tax_col != NULL) {
                            $curr_column = Coordinate::stringFromColumnIndex($columnNumber);
                            foreach ($card_tax_col as $card_tax => $real_tax_name) {
                                $card_tax    = str_replace(array('tot.', '`'), '', $card_tax);
                                $next_column = $this->getNextColumn($curr_column);
                                $curr_column = $this->getNextColumn($next_column);
                                $activeSheet->mergeCells($next_column . $exportFileData['row'] . ':' . $curr_column . $exportFileData['row']);
                                $objRichText = $real_tax_name;
                                $this->setSheetStyle($activeSheet, $next_column . $exportFileData['row'], 'BFBFBF', '17214B', FALSE, 12, TRUE);
                                $activeSheet->setCellValue($next_column . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
                            }
                        }

                        $exportFileData['row']++;
                    }


                    $subTotalIndex = 0;
                    $columnNumber = 1;
                    $activeSheet->mergeCells(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'] . ':' . Coordinate::stringFromColumnIndex($columnNumber + 3) . $exportFileData['row']);
                    $objRichText = $curr_vendor['name'];
                    $this->setSheetStyle($activeSheet, 'A' . $exportFileData['row'], NULL, NULL, TRUE, 20);
                    $activeSheet->setCellValue('A' . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
                    $columnNumber += 4;

                    $rowValues = [];
                    $rowValues[] = $curr_vendor['cp_amount'];
                    $rowValues[] = $curr_vendor['cnp_amount'];
                    $rowValues[] = $curr_vendor['credit_total_with_fee_tip'] - $curr_vendor['total_credit_tips'];
                    $rowValues[] = $curr_vendor['cash_total_with_fee_tip'] - $curr_vendor['total_cash_tips'];
                    $rowValues[] = $curr_vendor['discount'];
                    $rowValues[] = $curr_vendor['house_account_amount'];
                    $rowValues[] = $curr_vendor['submit_with_no_payment_amount'];
                    $rowValues[] = $curr_vendor['total_credit_fees'] + $curr_vendor['total_cash_fees'];
                    $rowValues[] = $curr_vendor['custom_fees'];
                    $rowValues[] = $curr_vendor['cash_total_with_fee_tip'] + $curr_vendor['credit_total_with_fee_tip'] - $curr_vendor['total_cash_tips'] - $curr_vendor['total_credit_tips'];
                    $rowValues[] = $curr_vendor['total_credit_tips'];
                    $rowValues[] = $curr_vendor['total_wristband_tips'];
                    $rowValues[] = $curr_vendor['tip'];
                    $rowValues[] = $curr_vendor['cash_total_with_fee_tip'] - $curr_vendor['total_cash_tips'] - $curr_vendor['total_credit_tips'];
                    $rowValues   = array_merge($rowValues, $this->getTotalCustomTenderCell($fileViewData['dataForReport']['report_data']['custom_tenders'], $curr_vendor));
                    $rowValues[] = $curr_vendor['beacon_amount'];
                    $rowValues[] = $curr_vendor['apple_pay_amount'];
                    $rowValues[] = $curr_vendor['wristband_amount'];
                    $rowValues[] = $curr_vendor['total_credit_taxes'] + $curr_vendor['total_cash_taxes'];
                    $rowValues[] = $curr_vendor['givex_amount'];
                    $rowValues[] = $curr_vendor['total_stored_value_taxes'];
                    $rowValues[] = $curr_vendor['total_stored_value_tips'];
                    $rowValues[] = $curr_vendor['total_house_taxes'];
                    $rowValues[] = $curr_vendor['total_house_tips'];
                    $rowValues[] = $curr_vendor['paypal_amount'];
                    $rowValues[] = $curr_vendor['directpayment_amount'];
                    $rowValues[] = $curr_vendor['loadedvalue_amount'];

                    foreach ($rowValues as $value) {
                        $activeSheet->mergeCells(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'] . ':' . (Coordinate::stringFromColumnIndex($columnNumber + 1)) . $exportFileData['row']);
                        $this->setSheetStyle($activeSheet, Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                        $activeSheet->setCellValue(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], is_numeric($value) ? round($value, 2) : $value);
                        $columnNumber += 2;

                        if (!isset($sub_total[$subTotalIndex])) {
                            $sub_total[$subTotalIndex] = 0;
                        }

                        $sub_total[$subTotalIndex] += $value;
                        $subTotalIndex++;
                    }

                    if ($card_tax_col != NULL) {
                        //$subtotal_counter = 20;
                        $curr_column      = Coordinate::stringFromColumnIndex($columnNumber);
                        foreach ($card_tax_col as $card_tax => $real_tax_name) {
                            $card_tax    = str_replace(array('tot.', '`'), '', $card_tax);
                            $next_column = $this->getNextColumn($curr_column);
                            $curr_column = $this->getNextColumn($next_column);
                            $activeSheet->mergeCells($next_column . $exportFileData['row'] . ':' . $curr_column . $exportFileData['row']);
                            $objRichText = $curr_vendor[$card_tax];

                            if (!isset($sub_total[$subTotalIndex])) {
                                $sub_total[$subTotalIndex] = 0;
                            }

                            if (isset($sub_total[$subTotalIndex])) {
                                $sub_total[$subTotalIndex] += $curr_vendor[$card_tax];
                            } else {
                                $sub_total[$subTotalIndex] = $curr_vendor[$card_tax];
                            }

                            $this->setSheetStyle($activeSheet, $next_column . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                            $activeSheet->setCellValue($next_column . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

                            $subTotalIndex++;
                        }
                    }

                    $exportFileData['row']++;
                }
                /* SubTotal vendor */
                $columnNumber = 1;
                $activeSheet->mergeCells(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'] . ':' . Coordinate::stringFromColumnIndex($columnNumber + 3) . $exportFileData['row']);
                $objRichText = 'Vendor Tender Totals';
                $this->setSheetStyle($activeSheet, Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], NULL, NULL, TRUE, 20);
                $activeSheet->setCellValue(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
                $columnNumber += 4;

                for ($i = 0; $i < $subTotalIndex; $i++) {
                    $activeSheet->mergeCells(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'] . ':' . Coordinate::stringFromColumnIndex($columnNumber + 1) . $exportFileData['row']);
                    $objRichText = $sub_total[$i];
                    $this->setSheetStyle($activeSheet, Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                    $activeSheet->setCellValue(Coordinate::stringFromColumnIndex($columnNumber) . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);
                    $columnNumber += 2;
                }

                if ($card_tax_col != NULL) {
                    //$subtotal_counter = 20;
                    $curr_column      = Coordinate::stringFromColumnIndex($columnNumber);
                    foreach ($card_tax_col as $card_tax => $real_tax_name) {
                        $card_tax    = str_replace(array('tot.', '`'), '', $card_tax);
                        $next_column = $this->getNextColumn($curr_column);
                        $curr_column = $this->getNextColumn($next_column);
                        $activeSheet->mergeCells($next_column . $exportFileData['row'] . ':' . $curr_column . $exportFileData['row']);
                        $objRichText = $sub_total[$subTotalIndex];

                        $this->setSheetStyle($activeSheet, $next_column . $exportFileData['row'], NULL, NULL, FALSE, 12, TRUE);
                        $activeSheet->setCellValue($next_column . $exportFileData['row'], is_numeric($objRichText) ? round($objRichText, 2) : $objRichText);

                        $subTotalIndex++;
                    }
                }

                $exportFileData['row'] += 2;
            }
        }

        set_time_limit(30000);
        $xlsxWriter = new Xlsx($spreadsheet);
        $xlsxWriter->save($fileViewData['filePath'] . $fileViewData['fileNameNoExt'] . ".xlsx");

        set_time_limit(30000);
        $csvWriter = new Csv($spreadsheet);
        $csvWriter->save($fileViewData['filePath'] . $fileViewData['fileNameNoExt'] . ".csv");
//
//        set_time_limit(30000);
//        ini_set("pcre.backtrack_limit", "50000000");
//        $class = Mpdf::class;
//        IOFactory::registerWriter('Pdf', $class);
//        $pdfWriter = IOFactory::createWriter($spreadsheet, 'Pdf');
//        $pdfWriter->save($fileViewData['filePath'] . $fileViewData['fileNameNoExt'] . ".pdf");

        return [
            'xlsx'  => $fileViewData['filePath'] . $fileViewData['fileNameNoExt'] . ".xlsx",
            'csv'   => $fileViewData['filePath'] . $fileViewData['fileNameNoExt'] . ".csv",
            //'pdf'   => $fileViewData['filePath'] . $fileViewData['fileNameNoExt'] . ".pdf",
        ];
    }


    /**
     * Build custom tender report column names
     *
     * @param array $customTenderColumnNames
     *
     * @return array
     */
    private function buildTenderTypeColumnNames($customTenderColumnNames):array
    {
        return array_merge(
            [
                'Card Present Sales',
                'Card Not Present',
                'Total Credit',
                'Total Cash',
                'Promotions',
                'House',
                'Submit',
                'Cash & Credit Fees',
                'Custom Fees',
                'Cash/Credit Sales',
                'Credit Tips',
                'Wristbands Tips',
                'Total Tips',
                'Cash Less Credit Tips',
            ], $customTenderColumnNames
        );
    }


    /**
     * Total Custom Tender for especific Cell
     *
     * @param array $customTenders
     * @param array $currentVendor
     *
     * @return array
     */
    private function getTotalCustomTenderCell($customTenders, $currentVendor)
    {
        $rowValues = [];

        foreach ($customTenders as $customTender)
        {
            $customTender     = get_object_vars($customTender);

            $rowValues[] = (isset($currentVendor['customTenders'][$customTender['id']]['cashAmount']) ?
                $currentVendor['customTenders'][$customTender['id']]['cashAmount'] : 0);
        }

        return $rowValues;
    }


    /**
     * Ensure the next column generated matches up with the column index maintained by PhpSpreadsheet
     *
     * @param $curr_column
     * @param int $next_by
     * @return string
     *
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function getNextColumn($curr_column, $next_by = 1)
    {
        $columnIndex         = Coordinate::columnIndexFromString($curr_column);
        $adjustedColumnIndex = $columnIndex + $next_by;

        return Coordinate::stringFromColumnIndex($adjustedColumnIndex - 1);
    }


    /**
     * Merge a cell and set its value
     *
     * @param Worksheet $sheet
     * @param $row
     * @param $value
     * @param $startColumn
     * @param $endColumn
     * @param bool $center
     * @param null $bgColor
     * @param null $fontColor
     * @param int $fontSize
     *
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function cellWithMerge(Worksheet $sheet, $row, $value, $startColumn, $endColumn, $center = true, $bgColor = null, $fontColor = null, $fontSize = 12)
    {
        $sheet->mergeCells($startColumn . $row . ':' . $endColumn . $row);
        $this->setSheetStyle($sheet, $startColumn . $row, $bgColor, $fontColor, false, $fontSize, $center);
        $sheet->setCellValue($startColumn . $row, is_numeric($value) ? round($value, 2) : $value);
    }


    /**
     * Conveniently set the style of the PhpSpreadsheet element
     *
     * @param Worksheet $sheet
     * @param $cell_no
     * @param null $fillColor
     * @param null $fontColor
     * @param bool $isBold
     * @param null $fontSize
     * @param bool $isCenter
     * @param bool $isVerticalCentre
     *
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function setSheetStyle(Worksheet $sheet, $cell_no, $fillColor = NULL, $fontColor = NULL, $isBold = FALSE, $fontSize = NULL, $isCenter = FALSE, $isVerticalCentre = FALSE)
    {
        // Background Color Fill
        if (!is_null($fillColor))
        {
            $sheet->getStyle($cell_no)->applyFromArray
            (
                [
                    'fill' => [
                        'fillType'  => Fill::FILL_SOLID,
                        'color'     => ['argb' => $fillColor],
                    ]
                ]
            );
        }

        // Center Cell Value Horizontally
        if ($isCenter)
        {
            $sheet->getStyle($cell_no)->applyFromArray
            (
                [
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER,
                    ],
                ]
            );
        }

        // Center Cell Value Vertically
        if ($isVerticalCentre)
        {
            $sheet->getStyle($cell_no)->applyFromArray
            (
                [
                    'alignment' => [
                        'vertical' => Alignment::VERTICAL_CENTER,
                    ],
                ]
            );
        }

        $fontArr = NULL;

        if (!is_null($fontColor))
        {
            $fontArr['color'] = ['argb' => $fontColor];
        }

        if ($isBold)
        {
            $fontArr['bold'] = true;
        }

        if (!is_null($fontSize))
        {
            $fontArr['size'] = $fontSize;
        }

        if (!is_null($fontArr))
        {
            $sheet->getStyle($cell_no)->applyFromArray
            (
                ['font' => $fontArr]
            );
        }
    }


    /**
     * Run the queries to get the Report data
     *
     * @param $id
     * @param string $id_type
     * @param $start_date
     * @param $end_date
     * @param array $extra_user
     *
     * @return array
     */
    private function runActualTenderTypeReport($id, $id_type = 'venue', $start_date, $end_date, $extra_user = array(FALSE, FALSE))
    {

//        dd([
//            'id' => $id,
//            'id_type' => $id_type,
//            'start_date' => $start_date,
//            'end_date' => $end_date,
//            'extra_user' => $extra_user
//        ]);

        $sales_by_card_type     = $this->getCpTenderBreakdownReportVenueByCardType($id, $start_date, $end_date, $extra_user);

         //dd($sales_by_card_type);

        if(isset($sales_by_card_type[0]->cash_total_with_fee_tip))
        {
            $sales_by_card_type[0]->cash_total_with_fee_tip -= $this->getCustomRefundAmount($id, $start_date, $end_date);
        }

        $code_breakdown =   DB::select
                            (
                                "SELECT 
                                    p.code type, 
                                    p.id p_id, 
                                    count(distinct o.id) quantity, 
                                    SUM(
                                      CASE 
                                        WHEN op.promo_id IS NOT NULL 
                                        THEN op.amount 
                                        ELSE o.original_subtotal - o.bill_amount 
                                      END
                                    ) total
                                
                                FROM orders o
                                  JOIN order_payments op  
                                    ON (o.id = op.order_id)
                                  JOIN promos p           
                                    ON 
                                    (
                                          p.id=COALESCE(op.promo_id, o.promo_id) 
                                      AND p.venue_id = " . $id . " 
                                      AND o." . $this->order_date_field . " >= '" . $start_date . "' 
                                      AND o." . $this->order_date_field . " < '" . $end_date . "' 
                                      AND o.refund_status NOT IN ('REFUNDED') 
                                      AND o.order_status IN ('COMPLETED', 'PARTIAL_REFUND')
                                    )
                                
                                GROUP BY p.id"
                            );
        //dd($code_breakdown);

        $vendor_group_breakdown = $this->getCpTenderBreakdownReportVenueByVgId($id, $start_date, $end_date, $extra_user);

        //dd($vendor_group_breakdown);

        $vendor_breakdown       = $this->getCpTenderBreakdownReportVenueByVendorId($id, $start_date, $end_date, $extra_user);

        //dd($vendor_breakdown);

        $output = [
            'card'         => [$sales_by_card_type, NULL],
            'code'         => $code_breakdown,
            'vendor_group' => [$vendor_group_breakdown, NULL],
            'vendor'       => [$vendor_breakdown, NULL],
        ];

        //dd($output);

        if ('corporate' == $id_type)
        {
            dd(23232323232322);
            $venue_breakdown = $this->getCpTenderBreakdownReportVenueByVenueId($id, $start_date, $end_date, $extra_user);
            $output['venue'] = [$venue_breakdown, NULL];
        }
        else
        {
            // Getting output for Account Tender Type Breakdown
            $account_breakdown              =   $this->getCpTenderBreakdownReportVenueByUserId($id, $start_date, $end_date, $extra_user);

            //dd($account_breakdown);

            // Getting output for Account Group Tender Type Breakdown
            $account_group_breakdown        =   $this->getCpTenderBreakdownReportVenueByAgId($id, $start_date, $end_date, $extra_user);

            //dd($account_group_breakdown);

            // Getting output for Account by Vendor Tender Type Breakdown
            $account_breakdown_by_vendor    =   $this->getCpTenderBreakdownReportVenueByAccountsByVendor($id, $start_date, $end_date, $extra_user);

            //dd($account_breakdown_by_vendor);

            $output['account']              =   [$account_breakdown             , NULL];
            $output['account_group']        =   [$account_group_breakdown       , NULL];
            $output['account_by_vendor']    =   [$account_breakdown_by_vendor   , NULL];
        }


        //dd($output);

        $output['custom_tenders'] = $this->getCustomTenderReport($id, $start_date, $end_date);

        //dd($output);

        return $output;
    }


    /**
     * Get Custom Tender data for Tender Type report
     *
     * @param $venueId
     * @param $startDate
     * @param $endDate
     *
     * @return mixed
     */
    private function getCustomTenderReport($venueId, $startDate, $endDate)
    {
        $result = DB::select
        ("SELECT
                  ct.id               AS id,
                  ct.name             AS name,
                  ct.displayName      AS type,
                  count(1)            AS quantity,
                  SUM(oct.cashAmount) AS total
                
                FROM orders o
                  JOIN vendors v ON v.id = o.vendor_id AND v.venue_id = :venueId
                  JOIN users u ON u.id = o.user_id AND u.type IN ('EMPLOYEE')
                  JOIN OrderCustomTenders oct ON oct.orderId = o.id
                  JOIN CustomTender ct ON ct.id = oct.customTenderId
                  
                WHERE
                        (o.payment_type = 12 or o.payment_type = 0)
                    AND o.order_status IN ('COMPLETED', 'PARTIAL_REFUND')
                    AND o.datetime >= :startDate
                    AND o.datetime < :endDate
                
                GROUP BY id, name, type ASC",
            [
                'venueId'   => $venueId,
                'startDate' => $startDate,
                'endDate'   => $endDate,
            ]
        );

        return $result;
    }


    /**
     *
     *
     * @param $escapedId
     * @param $escapedStartDate
     * @param $escapedEndDate
     * @param bool $force
     *
     * @return array
     */
    private function multiple_tax_helper_venue($escapedId, $escapedStartDate, $escapedEndDate, $force = false)
    {
        //dd($this->cachedTaxColumns);
        if (!$force && $this->cachedTaxColumns !== false && !is_null($this->cachedTaxColumns))
        {
            return $this->cachedTaxColumns;
        }

        //dd($this->order_date_field);
        $orderDateFieldSelector = 'o.' . $this->order_date_field;

        //dd($orderDateFieldSelector);
        //DB::enableQueryLog();
        $taxResults = DB::select("SELECT 
                      o.id as order_id, 
                      t.name tax_name, 
                      t.id tax_id, 
                      IF
                      (
                        t.type = 0, 
                        COALESCE(t.value, 0.00), 
                        COALESCE(t.value, 0.00) / (IF(t.is_exclusive = 1 , 100, (100 + COALESCE(t.value, 0.00)))) * COALESCE(oai.original_cost, oi.original_cost, 0)
                      ) * (oi.quantity - IFNULL(rop.quantity, 0)) tax_value

                    FROM orders o

                           JOIN vendors v                             ON v.id = o.vendor_id AND v.venue_id = {$escapedId}  
                           JOIN venues vu                             ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active = 1) OR vu.total_by_active_layout = 0)
                           JOIN order_items oi                        ON oi.order_id = o.id AND {$orderDateFieldSelector} >= '{$escapedStartDate}' AND {$orderDateFieldSelector} < '{$escapedEndDate}'  AND o.refund_status NOT IN ('REFUNDED') AND o.order_status IN ('COMPLETED', 'PARTIAL_REFUND')
                           JOIN product_catalog p                     ON p.id = oi.product_id
                      LEFT JOIN order_additional_items oai            ON oai.order_items_id = oi.id AND p.add_price_override = 1
                      LEFT JOIN product_catalog_additional_items a    ON a.id = oai.additional_id
                      LEFT JOIN refund_order_payments rop             ON rop.order_id = oi.order_id AND rop.item_row_id = oi.row_id AND rop.refund_status = 'COMPLETED'
                           JOIN tax_group tg                          ON tg.id = p.tax_groups_id AND tg.status = 1
                           JOIN tax t                                 ON tg.id = t.group_id
     
                    GROUP BY 
                      o.id, oi.row_id, t.id, oai.original_cost , oi.original_cost    , oi.quantity   , rop.quantity             
                      
                    UNION ALL                    
                    
                    SELECT 
                      o.id as order_id, 
                      t.name tax_name, 
                      t.id tax_id, 
                      IF
                      (
                        t.type = 0, 
                        COALESCE(t.value, 0.00), 
                        COALESCE(t.value, 0.00) / (IF(t.is_exclusive = 1, 100, (100 + COALESCE(t.value, 0.00)))) * COALESCE(oai.original_cost, 0)
                      ) * (oi.quantity - IFNULL(rop.quantity, 0)) tax_value
                        
                    FROM orders o
                           JOIN vendors v                             ON v.id = o.vendor_id AND v.venue_id = {$escapedId}  
                           JOIN venues vu                             ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active = 1) OR vu.total_by_active_layout = 0)
                           JOIN order_items oi                        ON oi.order_id = o.id AND {$orderDateFieldSelector} >= '{$escapedStartDate}' AND {$orderDateFieldSelector} < '{$escapedEndDate}'  AND o.refund_status NOT IN ('REFUNDED') AND o.order_status IN ('COMPLETED', 'PARTIAL_REFUND')
                           JOIN product_catalog p                     ON p.id = oi.product_id
                           JOIN order_additional_items oai            ON oai.order_items_id = oi.id AND p.add_price_override = 0
                           JOIN product_catalog_additional_items a    ON a.id = oai.additional_id AND a.id IS NOT NULL
                      LEFT JOIN refund_order_payments rop             ON rop.order_id = oi.order_id AND rop.item_row_id = oi.row_id AND rop.refund_status = 'COMPLETED'
                           JOIN tax_group tg ON tg.id = p.tax_groups_id AND tg.status = 1
                           JOIN tax t ON tg.id = t.group_id
                           
                    GROUP BY o.id, t.id, oai.original_cost , oi.original_cost    , oi.quantity   , rop.quantity 
                    ");

        //$query = DB::getQueryLog();
        //$lastQuery = end($query);
        //dd($lastQuery);

        //dd($taxResults);


        $orderArr   = NULL;
        $taxNames   = NULL;
        foreach ($taxResults as $taxResult)
        {
            $taxNames[$taxResult['tax_id']] = $taxResult['tax_name'];
            if (isset($orderArr[$taxResult['order_id']][$taxResult['tax_id']]))
            {
                $orderArr[$taxResult['order_id']][$taxResult['tax_id']] += $taxResult['tax_value'];
            }
            else
            {
                $orderArr[$taxResult['order_id']][$taxResult['tax_id']] = $taxResult['tax_value'];
            }
        }

        if (is_array($taxNames))
        {
            ksort($taxNames);
        }

        $rowArr    = NULL;
        $tableCols = [];
        $sqlHeader = ['order_id int(11) NOT NULL AUTO_INCREMENT'];
        if (is_array($orderArr))
        {
            $flag = 0;
            foreach ($orderArr as $orderId => $order)
            {
                $rowArrTaxes = [$orderId];
                $taxCounter  = 1;
                foreach ($taxNames as $taxId => $taxName)
                {
                    if ($flag == 0) {
                        $tableCols['tot.`tax' . $taxCounter . '`'] = $taxName;
                        $sqlHeader[]                               = '`' . 'tax' . $taxCounter . '` decimal(10,4)';
                    }
                    $rowArrTaxes[] = $order[$taxId];

                    $taxCounter++;
                }

                $rowArr[$orderId] = implode(',', $rowArrTaxes);
                $flag             = 1;
            }
        }

        DB::select('DROP TEMPORARY TABLE IF EXISTS tax_order_temp');
        DB::select('CREATE TEMPORARY TABLE IF NOT EXISTS tax_order_temp(' . implode(', ', $sqlHeader) . ', PRIMARY KEY (order_id))');

        if (is_array($tableCols) && count($tableCols) > 0)
        {
            DB::select('INSERT INTO tax_order_temp VALUES (' . implode('), (', array_values($rowArr)) . ')');
        }


        //dd($tableCols);
        $this->cachedTaxColumns = $tableCols;

        return $tableCols;
    }


    private function multiple_tax_helper_corporate($escapedId, $idType, $escapedStartDate, $escapedEndDate, $force = false)
    {
        if (!$force && $this->cachedTaxColumns !== false)
        {
            return $this->cachedTaxColumns;
        }

        $taxSqlJoin = "
        JOIN vendors v ON v.id = o.vendor_id
        JOIN corporate_venues cv ON cv.venue_id = v.venue_id AND cv.corporate_id = {$escapedId} 
        JOIN venues vu ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active = 1) OR vu.total_by_active_layout = 0)";

        $orderDateFieldSelector = 'o.' . $this->order_date_field;

        $sqlStmt = "SELECT 
                      o.id as order_id, 
                      t.name tax_name, 
                      t.id tax_id, 
                      IF
                      (
                        t.type = 0, 
                        COALESCE(t.value, 0.00), 
                        COALESCE(t.value, 0.00) / (IF(t.is_exclusive = 1 , 100, (100 + COALESCE(t.value, 0.00)))) * COALESCE(oai.original_cost, oi.original_cost, 0)
                      ) * (oi.quantity - IFNULL(rop.quantity, 0)) tax_value
                      
                     FROM orders o
                           JOIN vendors v                           ON v.id = o.vendor_id
                           JOIN corporate_venues cv                 ON cv.venue_id = v.venue_id AND cv.corporate_id = {$escapedId} 
                           JOIN venues vu                           ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active = 1) OR vu.total_by_active_layout = 0)
                           JOIN order_items oi                      ON oi.order_id = o.id AND {$orderDateFieldSelector} >= {$escapedStartDate} AND {$orderDateFieldSelector} < {$escapedStartDate} AND o.refund_status NOT IN ('REFUNDED') AND o.order_status IN ('COMPLETED', 'PARTIAL_REFUND')
                           JOIN product_catalog p                   ON p.id = oi.product_id
                      LEFT JOIN order_additional_items oai          ON oai.order_items_id = oi.id AND p.add_price_override = 1
                      LEFT JOIN product_catalog_additional_items a  ON a.id = oai.additional_id
                      LEFT JOIN refund_order_payments rop           ON rop.order_id = oi.order_id AND rop.item_row_id = oi.row_id AND rop.refund_status = 'COMPLETED'
                           JOIN tax_group tg                        ON tg.id = p.tax_groups_id AND tg.status = 1
                           JOIN tax t                               ON tg.id = t.group_id
                     
                     GROUP BY o.id, oi.row_id, t.id
                     
                     UNION ALL
                     
                     SELECT 
                        o.id as order_id, 
                        t.name tax_name, 
                        t.id tax_id, 
                        IF
                        (
                          t.type = 0, 
                          COALESCE(t.value, 0.00), 
                          COALESCE(t.value, 0.00) / (IF(t.is_exclusive = 1, 100, (100 + COALESCE(t.value, 0.00)))) * COALESCE(oai.original_cost, 0)
                        ) * (oi.quantity - IFNULL(rop.quantity, 0)) tax_value
                        
                    FROM orders o
                           JOIN vendors v                           ON v.id = o.vendor_id
                           JOIN corporate_venues cv                 ON cv.venue_id = v.venue_id AND cv.corporate_id = {$escapedId} 
                           JOIN venues vu                           ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active = 1) OR vu.total_by_active_layout = 0)
                           JOIN order_items oi                      ON oi.order_id = o.id AND {$orderDateFieldSelector} >= {$escapedStartDate} AND {$orderDateFieldSelector} < {$escapedStartDate} AND o.refund_status NOT IN ('REFUNDED') AND o.order_status IN ('COMPLETED', 'PARTIAL_REFUND')
                           JOIN product_catalog p                   ON p.id = oi.product_id
                           JOIN order_additional_items oai          ON oai.order_items_id = oi.id AND p.add_price_override = 0
                           JOIN product_catalog_additional_items a  ON a.id = oai.additional_id AND a.id IS NOT NULL
                      LEFT JOIN refund_order_payments rop           ON rop.order_id = oi.order_id AND rop.item_row_id = oi.row_id AND rop.refund_status = 'COMPLETED'
                           JOIN tax_group tg                        ON tg.id = p.tax_groups_id AND tg.status = 1
                           JOIN tax t                               ON tg.id = t.group_id
                           
                    GROUP BY o.id, t.id";

        $taxResults = DB::select($sqlStmt);
        $orderArr   = NULL;
        $taxNames   = NULL;

        foreach ($taxResults as $taxResult)
        {
            $taxNames[$taxResult['tax_id']] = $taxResult['tax_name'];
            if (isset($orderArr[$taxResult['order_id']][$taxResult['tax_id']]))
            {
                $orderArr[$taxResult['order_id']][$taxResult['tax_id']] += $taxResult['tax_value'];
            }
            else
            {
                $orderArr[$taxResult['order_id']][$taxResult['tax_id']] = $taxResult['tax_value'];
            }
        }

        if (is_array($taxNames))
        {
            ksort($taxNames);
        }

        $rowArr    = NULL;
        $tableCols = [];
        $sqlHeader = ['order_id int(11) NOT NULL AUTO_INCREMENT'];
        if (is_array($orderArr))
        {
            $flag = 0;
            foreach ($orderArr as $orderId => $order)
            {
                $rowArrTaxes = [$orderId];
                $taxCounter   = 1;
                foreach ($taxNames as $taxId => $taxName) {
                    if ($flag == 0) {
                        $tableCols['tot.`tax' . $taxCounter . '`'] = $taxName;
                        $sqlHeader[]                                = '`' . 'tax' . $taxCounter . '` decimal(10,4)';
                    }
                    $rowArrTaxes[] = $order[$taxId];

                    $taxCounter++;
                }
                $rowArr[$orderId] = implode(',', $rowArrTaxes);
                $flag               = 1;
            }
        }

        DB::select('DROP TEMPORARY TABLE IF EXISTS tax_order_temp');
        DB::select('CREATE TEMPORARY TABLE IF NOT EXISTS tax_order_temp(' . implode(', ', $sqlHeader) . ', PRIMARY KEY (order_id))');

        if (is_array($tableCols) && count($tableCols) > 0)
        {
            DB::select('INSERT INTO tax_order_temp values (' . implode('), (', array_values($rowArr)) . ')');
        }

        $this->cachedTaxColumns = $tableCols;

        return $tableCols;
    }


    private function getCpTenderBreakdownReportVenueByAgId($id = NULL, $start_date, $end_date, $extra_user)
    {
        if ($id === '')
        {
            return [];
        }

        $order_date_field = $this->order_date_field;

        $table_cols = $this->multiple_tax_helper_venue($id, $start_date, $end_date);
        $table_cols_name = array_keys($table_cols);

        DB::select('DROP TEMPORARY TABLE IF EXISTS payment_log_by_order');
        DB::select("CREATE TEMPORARY TABLE IF NOT EXISTS payment_log_by_order 
                          (
                            PRIMARY KEY payment_log_by_order_PK (order_id)
                          ) 
                          AS
                          (
                            SELECT
                              o.id         AS order_id,
                              pl.card_type AS card_type,
                              pl.transaction_id,
                              pl.approved
                          
                            FROM orders o
                                   JOIN vendors v ON v.id = o.vendor_id AND v.venue_id = {$id}
                                   JOIN venues vu ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active=1) OR vu.total_by_active_layout = 0)
                                   JOIN payment_log pl  ON o.id = pl.order_id AND pl.transaction_type = 1
                              LEFT JOIN payment_log pl2 ON pl.order_id = pl2.order_id AND pl2.transaction_type = 1 AND (pl.approved < pl2.approved OR (pl.approved = pl2.approved AND pl.transaction_id < pl2.transaction_id))
                            
                            WHERE 
                                  o.{$order_date_field} >= '{$start_date}'
                              AND o.{$order_date_field} < '{$end_date}'
                              AND pl2.id IS NULL
                            
                            GROUP BY o.id, card_type, pl.transaction_id, pl.approved
                          )");

        DB::select('DROP TEMPORARY TABLE IF EXISTS tmp_orders_refunds');
        DB::select("CREATE TEMPORARY TABLE IF NOT EXISTS tmp_orders_refunds 
                        (
  	                      PRIMARY KEY tmp_orders_refunds_PK (order_id, payment_index)
                        ) 
                        AS 
                        (
                          SELECT
                           o.id              AS order_id,
                           op.payment_index  AS payment_index,
                           SUM(rop.amount)   AS refunds_amount,
                           SUM(rop.subtotal) AS refunds_subtotal,
                           SUM(rop.tip)      AS refunds_tip,
                           SUM(rop.fee)      AS refunds_fee,
                           SUM(rop.tax)      AS refunds_tax
                          FROM order_payments op
                                  JOIN orders o ON op.order_id = o.id
                             LEFT JOIN refund_order_payments rop ON rop.order_id = o.id AND rop.refund_status = 'COMPLETED' AND rop.payment_index = op.payment_index
                                  JOIN vendors v                  ON v.id = o.vendor_id AND v.venue_id = {$id}
                                  JOIN venues vu                  ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active = 1) OR vu.total_by_active_layout = 0)
                             LEFT JOIN payment_log_by_order pl    ON o.id = pl.order_id AND op.payment_type IN (1,6,7,8,13,14)
        			   
        			      WHERE 
                                o.{$order_date_field} >= '{$start_date}'
                            AND o.{$order_date_field} < '{$end_date}'
                            AND o.order_status IN ('COMPLETED', 'PARTIAL_REFUND') 
                            AND o.refund_status NOT IN ('REFUNDED')
                            AND op.payment_status IN ('COMPLETED', 'PARTIAL_REFUND')
                        
 					      GROUP BY o.id , op.payment_index
                        )");

        DB::select('DROP TEMPORARY TABLE IF EXISTS sales_by_payment_type');
        DB::select("CREATE TEMPORARY TABLE IF NOT EXISTS sales_by_payment_type
                (
                  PRIMARY KEY sales_by_payment_type_PK (order_id)
                )
                AS
                (
                  SELECT
                    o.id AS order_id,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_subtotal,0), 0)) AS total_cash_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS total_cash_sales_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_cash_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_cash_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_cash_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, 1, 0)) AS cash_tran_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (1,6,7,8,13,14), COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_subtotal,0), 0)) AS total_credit_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (1,6,7,8,13,14), COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS total_credit_sales_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (1,6,7,8,13,14), COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_credit_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (1,6,7,8,13,14), COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_credit_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (1,6,7,8,13,14), COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_credit_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (3,11,16), COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS total_givex_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS total_loadedvalue_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (11,16), COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_stored_value_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (11,16), COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_stored_value_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (11,16), COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_stored_value_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_directpayment_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_directpayment_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_loadedvalue_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_loadedvalue_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 5, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS total_house_sales_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 5, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS total_house_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 5, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_house_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 5, COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_house_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 5, COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_house_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 9, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS total_submit_with_no_payment_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 1 AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS cp_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 1 AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0), 0)) AS cnp_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 1 AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0), 0)) AS cnp_amount_minus_refund,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 6, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS beacon_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 6, 1, 0)) AS beacon_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 7, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS apple_pay_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 7, 1, 0)) AS apple_pay_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 8, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS wristband_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 8, 1, 0)) AS wristband_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 13, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS paypal_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 13, 1, 0)) AS paypal_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS directpayment_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, 1, 0)) AS directpayment_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS loadedvalue_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, 1, 0)) AS loadedvalue_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 9, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS no_payment_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 9, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS no_payment_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 1 AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS cp_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 1 AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS cnp_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 6, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS beacon_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 7, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS apple_pay_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 8, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS wristband_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 13, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS paypal_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0))  AS directpayment_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax)- COALESCE(tor.refunds_amount,0), 0))  AS loadedvalue_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 8, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_wristband_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 13, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_paypal_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_directpayment_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_loadedvalue_tips,
                
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express'), COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS total_amex_sales_with_fee_tip,
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express') AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS amex_cp_amount_with_fee_tip,
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express') AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS amex_cnp_amount_with_fee_tip,
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express'), 1, 0)) AS amex_tran_count,
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express') AND op.is_card_present = 1, 1, 0)) AS amex_cp_tran_count,
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express') AND COALESCE(op.is_card_present,0) <> 1, 1, 0)) AS amex_cnp_tran_count,
                    SUM(IF(pl.card_type = 'Discover', COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS total_discover_sales_with_fee_tip,
                    SUM(IF(pl.card_type = 'Discover' AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS discover_cp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Discover' AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS discover_cnp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Discover', 1, 0)) AS discover_tran_count,
                    SUM(IF(pl.card_type = 'Discover' AND op.is_card_present = 1, 1, 0)) AS discover_cp_tran_count,
                    SUM(IF(pl.card_type = 'Discover' AND COALESCE(op.is_card_present,0) <> 1, 1, 0)) AS discover_cnp_tran_count,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master'), COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS total_mastercard_sales_with_fee_tip,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master') AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS mastercard_cp_amount_with_fee_tip,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master') AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS mastercard_cnp_amount_with_fee_tip,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master'), 1, 0)) AS mastercard_tran_count,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master') AND op.is_card_present = 1, 1, 0)) AS mastercard_cp_tran_count,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master') AND COALESCE(op.is_card_present,0) <> 1, 1, 0)) AS mastercard_cnp_tran_count,
                    SUM(IF(pl.card_type = 'Visa', COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS total_visa_sales_with_fee_tip,
                    SUM(IF(pl.card_type = 'Visa' AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS visa_cp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Visa' AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS visa_cnp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Visa', 1, 0)) AS visa_tran_count,
                    SUM(IF(pl.card_type = 'Visa' AND op.is_card_present = 1, 1, 0)) AS visa_cp_tran_count,
                    SUM(IF(pl.card_type = 'Visa' AND COALESCE(op.is_card_present,0) <> 1, 1, 0)) AS visa_cnp_tran_count,
                    SUM(IF(pl.card_type = 'Other', 1, 0)) AS other_tran_count,
                    SUM(IF(pl.card_type = 'Other' AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS other_cp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Other' AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS other_cnp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Other', COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS total_other_sales_with_fee_tip
                
                  FROM order_payments op
                         JOIN orders o                ON op.order_id = o.id
                    LEFT JOIN tmp_orders_refunds tor  ON (tor.order_id = o.id AND tor.payment_index = op.payment_index)
                         JOIN vendors v                  ON v.id = o.vendor_id AND v.venue_id = {$id}
                         JOIN venues vu                  ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active = 1) OR vu.total_by_active_layout = 0)
                    LEFT JOIN payment_log_by_order pl    ON o.id = pl.order_id AND op.payment_type IN (1,6,7,8,13,14)
        			   
        		  WHERE 
                        o.{$order_date_field} >= '{$start_date}'
                    AND o.{$order_date_field} < '{$end_date}'
                    AND o.order_status IN ('COMPLETED', 'PARTIAL_REFUND') 
                    AND o.refund_status NOT IN ('REFUNDED')
                    AND op.payment_status IN ('COMPLETED', 'PARTIAL_REFUND')
                        
 				  GROUP BY o.id 
 				)  
               ");

        $sql_user_type[] = 'EMPLOYEE';

        if ($extra_user[0] === TRUE)
        {
            $sql_user_type[] = 'GUEST';
        }

        if ($extra_user[1] === TRUE)
        {
            $sql_user_type[] = 'USER';
        }

        $result = DB::select("  SELECT                       
                           COALESCE(ag.id, 0) ag_id, 
                           COALESCE(ag.name, 'No Account Group') ag_name, 
                           SUM(COALESCE(o.tip, 0)) tip,
                           SUM(COALESCE(o.fee, 0)) fee,
                           ROUND(SUM(COALESCE(ocf.`customFeeValue`, 0)), 2) AS custom_fees,
                           SUM(COALESCE(o.tax, 0)) tax,
                           SUM(COALESCE(op.total_credit_tips, 0)) total_credit_tips,
                           SUM(COALESCE(op.total_credit_fees, 0)) total_credit_fees,
                           SUM(COALESCE(op.total_credit_taxes, 0)) total_credit_taxes,
                           SUM(COALESCE(op.total_cash_fees, 0)) total_cash_fees,
                           SUM(COALESCE(op.total_cash_taxes, 0)) total_cash_taxes,
                           SUM(COALESCE(op.total_wristband_tips, 0)) total_wristband_tips,
                           SUM(COALESCE(op.total_paypal_tips, 0)) total_paypal_tips,
                           SUM(COALESCE(op.total_directpayment_tips, 0)) total_directpayment_tips,
                           SUM(COALESCE(op.total_loadedvalue_tips, 0)) total_loadedvalue_tips,
                           SUM(COALESCE(op.cp_amount, 0)) cp_amount,
                           SUM(COALESCE(op.cnp_amount, 0)) cnp_amount,
                           SUM(COALESCE(op.beacon_amount, 0)) beacon_amount,
                           SUM(COALESCE(op.apple_pay_amount, 0)) apple_pay_amount,
                           SUM(COALESCE(op.wristband_amount, 0)) wristband_amount,
                           SUM(COALESCE(op.paypal_amount, 0)) paypal_amount,
                           SUM(COALESCE(op.directpayment_amount, 0)) directpayment_amount,
                           SUM(COALESCE(op.loadedvalue_amount, 0)) loadedvalue_amount,
                           SUM(COALESCE(op.no_payment_amount, 0)) no_payment_amount,
                           SUM(COALESCE(op.no_payment_with_fee_tip, 0)) no_payment_with_fee_tip,
                           SUM(COALESCE(op.cp_amount_with_fee_tip, 0)) cp_amount_with_fee_tip,
                           SUM(COALESCE(op.cnp_amount_with_fee_tip, 0)) cnp_amount_with_fee_tip,
                           SUM(COALESCE(op.beacon_amount_with_fee_tip, 0)) beacon_amount_with_fee_tip,
                           SUM(COALESCE(op.apple_pay_amount_with_fee_tip, 0)) apple_pay_amount_with_fee_tip,
                           SUM(COALESCE(op.wristband_amount_with_fee_tip, 0)) wristband_amount_with_fee_tip,
                           SUM(COALESCE(op.paypal_amount_with_fee_tip, 0)) paypal_amount_with_fee_tip,
                           SUM(COALESCE(op.directpayment_amount_with_fee_tip, 0)) directpayment_amount_with_fee_tip,
                           SUM(COALESCE(op.loadedvalue_amount_with_fee_tip, 0)) loadedvalue_amount_with_fee_tip,
                           SUM(COALESCE(op.total_givex_sales, 0)) givex_amount,
                           SUM(COALESCE(op.total_stored_value_tips, 0)) total_stored_value_tips,
                           SUM(COALESCE(op.total_stored_value_fees, 0)) total_stored_value_fees,
                           SUM(COALESCE(op.total_stored_value_taxes, 0)) total_stored_value_taxes,
                           SUM(COALESCE(op.total_credit_sales, 0)) credit_total,
                           SUM(COALESCE(op.total_credit_sales_with_fee_tip, 0)) credit_total_with_fee_tip,
                           SUM(COALESCE(op.total_cash_sales, 0)) cash_total,
                           SUM(COALESCE(op.total_cash_tips, 0)) total_cash_tips,
                           SUM(COALESCE(op.total_cash_sales_with_fee_tip, 0)) cash_total_with_fee_tip,
                           SUM(COALESCE(o.original_subtotal - o.bill_amount, 0)) discount,
                           SUM(COALESCE(op.total_house_sales_with_fee_tip, 0)) AS house_total_with_fee_tip,
                           SUM(COALESCE(op.total_house_sales, 0)) AS house_account_amount,
                           SUM(COALESCE(op.total_house_tips, 0)) AS total_house_tips,
                           SUM(COALESCE(op.total_house_fees, 0)) AS total_house_fees,
                           SUM(COALESCE(op.total_house_taxes, 0)) AS total_house_taxes,
                           SUM(COALESCE(op.total_submit_with_no_payment_sales, 0)) AS submit_with_no_payment_amount,
                           SUM(COALESCE(o.original_subtotal, 0)) amount,
                           SUM(COALESCE(op.total_amex_sales_with_fee_tip, 0)) total_amex_sales_with_fee_tip,
                           SUM(COALESCE(op.amex_cp_amount_with_fee_tip, 0)) amex_cp_amount_with_fee_tip,
                           SUM(COALESCE(op.amex_cnp_amount_with_fee_tip, 0)) amex_cnp_amount_with_fee_tip,
                           SUM(COALESCE(op.total_discover_sales_with_fee_tip, 0)) total_discover_sales_with_fee_tip,
                           SUM(COALESCE(op.discover_cp_amount_with_fee_tip, 0)) discover_cp_amount_with_fee_tip,
                           SUM(COALESCE(op.discover_cnp_amount_with_fee_tip, 0)) discover_cnp_amount_with_fee_tip,
                           SUM(COALESCE(op.total_mastercard_sales_with_fee_tip, 0)) total_mastercard_sales_with_fee_tip,
                           SUM(COALESCE(op.mastercard_cp_amount_with_fee_tip, 0)) mastercard_cp_amount_with_fee_tip,
                           SUM(COALESCE(op.mastercard_cnp_amount_with_fee_tip, 0)) mastercard_cnp_amount_with_fee_tip,
                           SUM(COALESCE(op.total_visa_sales_with_fee_tip, 0)) total_visa_sales_with_fee_tip,
                           SUM(COALESCE(op.visa_cp_amount_with_fee_tip, 0)) visa_cp_amount_with_fee_tip,
                           SUM(COALESCE(op.visa_cnp_amount_with_fee_tip, 0)) visa_cnp_amount_with_fee_tip,
                           SUM(COALESCE(op.visa_tran_count, 0)) visa_tran_count,
                           SUM(COALESCE(op.visa_cp_tran_count, 0)) visa_cp_tran_count,
                           SUM(COALESCE(op.visa_cnp_tran_count, 0)) visa_cnp_tran_count,
                           SUM(COALESCE(op.cash_tran_count, 0)) cash_tran_count,
                           SUM(COALESCE(op.mastercard_tran_count, 0)) mastercard_tran_count,
                           SUM(COALESCE(op.mastercard_cp_tran_count, 0)) mastercard_cp_tran_count,
                           SUM(COALESCE(op.mastercard_cnp_tran_count, 0)) mastercard_cnp_tran_count,
                           SUM(COALESCE(op.amex_tran_count, 0)) amex_tran_count,
                           SUM(COALESCE(op.amex_cp_tran_count, 0)) amex_cp_tran_count,
                           SUM(COALESCE(op.amex_cnp_tran_count, 0)) amex_cnp_tran_count,
                           SUM(COALESCE(op.discover_tran_count, 0)) discover_tran_count,
                           SUM(COALESCE(op.discover_cp_tran_count, 0)) discover_cp_tran_count,
                           SUM(COALESCE(op.discover_cnp_tran_count, 0)) discover_cnp_tran_count,
                           SUM(COALESCE(op.beacon_count, 0)) beacon_count,
                           SUM(COALESCE(op.apple_pay_count, 0)) apple_pay_count,
                           SUM(COALESCE(op.wristband_count, 0)) wristband_count,
                           SUM(COALESCE(op.paypal_count, 0)) paypal_count,
                           SUM(COALESCE(op.loadedvalue_count, 0)) loadedvalue_count,
                           SUM(COALESCE(op.directpayment_count, 0)) directpayment_count
                           " . (count($table_cols_name) > 0 ? ", " . implode(", ", $table_cols_name) : "") . ", 
                           'ag.id' AS groupColumn, ag.id AS groupColumnValue
                        FROM orders o
	                            JOIN vendors v ON v.id = o.vendor_id AND v.venue_id = {$id}
	                            JOIN venues vu ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active=1) OR vu.total_by_active_layout = 0)
	                            JOIN users u ON u.id = o.user_id AND u.type in (\"" . implode('", "', $sql_user_type) . "\")
                                JOIN employee_users eu ON u.id = eu.user_id AND eu.is_active = 1 AND eu.status = 1
	                       LEFT JOIN OrderCustomFees ocf ON o.id = ocf.orderId
                           LEFT JOIN accounts_serving_account_group asag ON eu.id = asag.employee_id
                           LEFT JOIN account_group ag ON ag.id = asag.account_group_id AND ag.is_active = 1 AND ag.status = 1
                           LEFT JOIN sales_by_payment_type op on o.id = op.order_id
                           LEFT JOIN tax_order_temp tot ON o.id = tot.order_id 
                           
                        WHERE 
                                o.order_status IN ('COMPLETED', 'PARTIAL_REFUND')        
                            AND o.refund_status NOT IN ('REFUNDED')
                            AND o.{$order_date_field} >= '{$start_date}'
                            AND o.{$order_date_field} < '{$end_date}' 
            
                        GROUP BY ag.id ");

        $resultCustomTenders = DB::select("SELECT
                               ct.id,
                               ct.name,
                               'ag.id' AS groupColumn,
                               ag.id AS groupColumnValue,
                               SUM(oct.cashAmount) AS cashAmount,
                               SUM(oct.customTenderAmount) AS customTenderAmount
                               
                             FROM orders o
                                    JOIN vendors v                              ON v.id = o.vendor_id AND v.venue_id = {$id}
                                    JOIN venues vu                              ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active=1) OR vu.total_by_active_layout = 0)
                                    JOIN users u                                ON u.id = o.user_id AND u.type in (\"" . implode('", "', $sql_user_type) . "\")
                                    JOIN employee_users eu                      ON u.id = eu.user_id AND eu.is_active = 1 AND eu.status = 1
                               LEFT JOIN accounts_serving_account_group asag    ON eu.id = asag.employee_id
                               LEFT JOIN account_group ag                       ON ag.id = asag.account_group_id AND ag.is_active = 1 AND ag.status = 1
                               LEFT JOIN sales_by_payment_type op               on o.id = op.order_id
                               LEFT JOIN tax_order_temp tot                     ON o.id = tot.order_id
                                    JOIN OrderCustomTenders oct                 ON (oct.orderId = o.id)
                                    JOIN CustomTender ct                        ON (ct.id = oct.customTenderId)
                               
                             WHERE 
                                  o.order_status IN ('COMPLETED', 'PARTIAL_REFUND')        
                              AND o.refund_status NOT IN ('REFUNDED')
                              AND o.{$order_date_field} >= '{$start_date}'
                              AND o.{$order_date_field} < '{$end_date}'
                               
                             GROUP BY ag.id, ct.id
                             ORDER BY ct.name ASC");

        $resultsIndexed = [];


        foreach ($result as &$r)
        {
            $r->customTenders                    = [];
            $resultsIndexed[$r->groupColumnValue] = $r;
        }

        foreach ($resultCustomTenders as &$ct)
        {
            $ct     = get_object_vars($ct);
            $ct['cashAmount']                                                  = (float)$ct['cashAmount'] ;
            $resultsIndexed[$ct['groupColumnValue']]->customTenders[$ct['id']] = $ct;
        }

        return array_values($resultsIndexed);
    }


    private function getCpTenderBreakdownReportVenueByCardType($id = NULL, $start_date, $end_date, $extra_user)
    {
        if ($id === '')
        {
            return [];
        }

        $order_date_field   =   $this->order_date_field;
        $table_cols         =   $this->multiple_tax_helper_venue($id, $start_date, $end_date);
        $table_cols_name    =   array_keys($table_cols);

        DB::select('DROP TEMPORARY TABLE IF EXISTS payment_log_by_order');
        DB::select("CREATE TEMPORARY TABLE IF NOT EXISTS payment_log_by_order 
                          (
                            PRIMARY KEY payment_log_by_order_PK (order_id)
                          ) 
                          AS
                          (
                            SELECT
                              o.id         AS order_id,
                              pl.card_type AS card_type,
                              pl.transaction_id,
                              pl.approved
                          
                            FROM orders o
                                   JOIN vendors v ON v.id = o.vendor_id AND v.venue_id = {$id}
                                   JOIN venues vu ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active=1) OR vu.total_by_active_layout = 0)
                                   JOIN payment_log pl  ON o.id = pl.order_id AND pl.transaction_type = 1
                              LEFT JOIN payment_log pl2 ON pl.order_id = pl2.order_id AND pl2.transaction_type = 1 AND (pl.approved < pl2.approved OR (pl.approved = pl2.approved AND pl.transaction_id < pl2.transaction_id))
                            
                            WHERE 
                                  o.{$order_date_field} >= '{$start_date}'
                              AND o.{$order_date_field} < '{$end_date}'
                              AND pl2.id IS NULL
                            
                            GROUP BY o.id, card_type, pl.transaction_id, pl.approved
                          )");

        DB::select('DROP TEMPORARY TABLE IF EXISTS tmp_orders_refunds');
        DB::select("CREATE TEMPORARY TABLE IF NOT EXISTS tmp_orders_refunds 
                        (
  	                      PRIMARY KEY tmp_orders_refunds_PK (order_id, payment_index)
                        ) 
                        AS 
                        (
                          SELECT
                           o.id              AS order_id,
                           op.payment_index  AS payment_index,
                           SUM(rop.amount)   AS refunds_amount,
                           SUM(rop.subtotal) AS refunds_subtotal,
                           SUM(rop.tip)      AS refunds_tip,
                           SUM(rop.fee)      AS refunds_fee,
                           SUM(rop.tax)      AS refunds_tax
                          FROM order_payments op
                                  JOIN orders o ON op.order_id = o.id
                             LEFT JOIN refund_order_payments rop ON rop.order_id = o.id AND rop.refund_status = 'COMPLETED' AND rop.payment_index = op.payment_index
                                  JOIN vendors v                  ON v.id = o.vendor_id AND v.venue_id = {$id}
                                  JOIN venues vu                  ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active = 1) OR vu.total_by_active_layout = 0)
                             LEFT JOIN payment_log_by_order pl    ON o.id = pl.order_id AND op.payment_type IN (1,6,7,8,13,14)
        			   
        			      WHERE 
                                o.{$order_date_field} >= '{$start_date}'
                            AND o.{$order_date_field} < '{$end_date}'
                            AND o.order_status IN ('COMPLETED', 'PARTIAL_REFUND') 
                            AND o.refund_status NOT IN ('REFUNDED')
                            AND op.payment_status IN ('COMPLETED', 'PARTIAL_REFUND')
                        
 					      GROUP BY o.id , op.payment_index
                        )");

        DB::select('DROP TEMPORARY TABLE IF EXISTS sales_by_payment_type');
        DB::select("CREATE TEMPORARY TABLE IF NOT EXISTS sales_by_payment_type
                (
                  PRIMARY KEY sales_by_payment_type_PK (order_id)
                )
                AS
                (
                  SELECT
                    o.id AS order_id,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_subtotal,0), 0)) AS total_cash_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS total_cash_sales_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_cash_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_cash_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_cash_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, 1, 0)) AS cash_tran_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (1,6,7,8,13,14), COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_subtotal,0), 0)) AS total_credit_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (1,6,7,8,13,14), COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS total_credit_sales_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (1,6,7,8,13,14), COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_credit_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (1,6,7,8,13,14), COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_credit_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (1,6,7,8,13,14), COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_credit_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (3,11,16), COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS total_givex_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS total_loadedvalue_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (11,16), COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_stored_value_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (11,16), COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_stored_value_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (11,16), COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_stored_value_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_directpayment_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_directpayment_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_loadedvalue_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_loadedvalue_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 5, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS total_house_sales_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 5, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS total_house_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 5, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_house_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 5, COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_house_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 5, COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_house_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 9, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS total_submit_with_no_payment_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 1 AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS cp_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 1 AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0), 0)) AS cnp_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 1 AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0), 0)) AS cnp_amount_minus_refund,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 6, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS beacon_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 6, 1, 0)) AS beacon_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 7, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS apple_pay_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 7, 1, 0)) AS apple_pay_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 8, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS wristband_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 8, 1, 0)) AS wristband_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 13, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS paypal_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 13, 1, 0)) AS paypal_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS directpayment_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, 1, 0)) AS directpayment_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS loadedvalue_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, 1, 0)) AS loadedvalue_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 9, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS no_payment_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 9, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS no_payment_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 1 AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS cp_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 1 AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS cnp_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 6, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS beacon_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 7, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS apple_pay_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 8, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS wristband_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 13, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS paypal_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0))  AS directpayment_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax)- COALESCE(tor.refunds_amount,0), 0))  AS loadedvalue_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 8, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_wristband_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 13, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_paypal_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_directpayment_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_loadedvalue_tips,
                
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express'), COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS total_amex_sales_with_fee_tip,
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express') AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS amex_cp_amount_with_fee_tip,
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express') AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS amex_cnp_amount_with_fee_tip,
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express'), 1, 0)) AS amex_tran_count,
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express') AND op.is_card_present = 1, 1, 0)) AS amex_cp_tran_count,
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express') AND COALESCE(op.is_card_present,0) <> 1, 1, 0)) AS amex_cnp_tran_count,
                    SUM(IF(pl.card_type = 'Discover', COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS total_discover_sales_with_fee_tip,
                    SUM(IF(pl.card_type = 'Discover' AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS discover_cp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Discover' AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS discover_cnp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Discover', 1, 0)) AS discover_tran_count,
                    SUM(IF(pl.card_type = 'Discover' AND op.is_card_present = 1, 1, 0)) AS discover_cp_tran_count,
                    SUM(IF(pl.card_type = 'Discover' AND COALESCE(op.is_card_present,0) <> 1, 1, 0)) AS discover_cnp_tran_count,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master'), COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS total_mastercard_sales_with_fee_tip,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master') AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS mastercard_cp_amount_with_fee_tip,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master') AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS mastercard_cnp_amount_with_fee_tip,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master'), 1, 0)) AS mastercard_tran_count,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master') AND op.is_card_present = 1, 1, 0)) AS mastercard_cp_tran_count,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master') AND COALESCE(op.is_card_present,0) <> 1, 1, 0)) AS mastercard_cnp_tran_count,
                    SUM(IF(pl.card_type = 'Visa', COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS total_visa_sales_with_fee_tip,
                    SUM(IF(pl.card_type = 'Visa' AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS visa_cp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Visa' AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS visa_cnp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Visa', 1, 0)) AS visa_tran_count,
                    SUM(IF(pl.card_type = 'Visa' AND op.is_card_present = 1, 1, 0)) AS visa_cp_tran_count,
                    SUM(IF(pl.card_type = 'Visa' AND COALESCE(op.is_card_present,0) <> 1, 1, 0)) AS visa_cnp_tran_count,
                    SUM(IF(pl.card_type = 'Other', 1, 0)) AS other_tran_count,
                    SUM(IF(pl.card_type = 'Other' AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS other_cp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Other' AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS other_cnp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Other', COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS total_other_sales_with_fee_tip
                
                  FROM order_payments op
                         JOIN orders o                ON op.order_id = o.id
                    LEFT JOIN tmp_orders_refunds tor  ON (tor.order_id = o.id AND tor.payment_index = op.payment_index)
                         JOIN vendors v                  ON v.id = o.vendor_id AND v.venue_id = {$id}
                         JOIN venues vu                  ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active = 1) OR vu.total_by_active_layout = 0)
                    LEFT JOIN payment_log_by_order pl    ON o.id = pl.order_id AND op.payment_type IN (1,6,7,8,13,14)
        			   
        		  WHERE 
                        o.{$order_date_field} >= '{$start_date}'
                    AND o.{$order_date_field} < '{$end_date}'
                    AND o.order_status IN ('COMPLETED', 'PARTIAL_REFUND') 
                    AND o.refund_status NOT IN ('REFUNDED')
                    AND op.payment_status IN ('COMPLETED', 'PARTIAL_REFUND')
                        
 				  GROUP BY o.id 
 				)  
               ");

        $sql_user_type[] = 'EMPLOYEE';

        if ($extra_user[0] === TRUE)
        {
            $sql_user_type[] = 'GUEST';
        }

        if ($extra_user[1] === TRUE)
        {
            $sql_user_type[] = 'USER';
        }

        $result = DB::select("SELECT
                   SUM(COALESCE(op.visa_tran_count, 0)) visa_tran_count,
                   SUM(COALESCE(op.visa_cp_tran_count, 0)) visa_cp_tran_count,
                   SUM(COALESCE(op.visa_cnp_tran_count, 0)) visa_cnp_tran_count,
                   SUM(COALESCE(op.cash_tran_count, 0)) cash_tran_count,
                   SUM(COALESCE(op.other_tran_count, 0)) other_tran_count,
                   SUM(COALESCE(op.mastercard_tran_count, 0)) mastercard_tran_count,
                   SUM(COALESCE(op.mastercard_cp_tran_count, 0)) mastercard_cp_tran_count,
                   SUM(COALESCE(op.mastercard_cnp_tran_count, 0)) mastercard_cnp_tran_count,
                   SUM(COALESCE(op.amex_tran_count, 0)) amex_tran_count,
                   SUM(COALESCE(op.amex_cp_tran_count, 0)) amex_cp_tran_count,
                   SUM(COALESCE(op.amex_cnp_tran_count, 0)) amex_cnp_tran_count,
                   SUM(COALESCE(op.discover_tran_count, 0)) discover_tran_count,
                   SUM(COALESCE(op.discover_cp_tran_count, 0)) discover_cp_tran_count,
                   SUM(COALESCE(op.discover_cnp_tran_count, 0)) discover_cnp_tran_count,
                   SUM(COALESCE(op.beacon_count, 0)) beacon_count,
                   SUM(COALESCE(op.apple_pay_count, 0)) apple_pay_count,
                   SUM(COALESCE(op.wristband_count, 0)) wristband_count,
                   SUM(COALESCE(op.paypal_count, 0)) paypal_count,
                   SUM(COALESCE(op.directpayment_count, 0)) directpayment_count,
                   SUM(COALESCE(op.loadedvalue_count, 0)) loadedvalue_count,
                   SUM(COALESCE(o.tip, 0)) tip,
                   SUM(COALESCE(o.fee, 0)) fee,
                   SUM(COALESCE(o.tax, 0)) tax,
                   SUM(COALESCE(op.total_credit_tips, 0)) total_credit_tips,
                   SUM(COALESCE(op.total_credit_fees, 0)) total_credit_fees,
                   SUM(COALESCE(op.total_credit_taxes, 0)) total_credit_taxes,
                   SUM(COALESCE(op.total_cash_fees, 0)) total_cash_fees,
                   SUM(COALESCE(op.total_cash_taxes, 0)) total_cash_taxes,
                   SUM(COALESCE(op.total_wristband_tips, 0)) total_wristband_tips,
                   SUM(COALESCE(op.total_paypal_tips, 0)) total_paypal_tips,
                   SUM(COALESCE(op.total_directpayment_tips, 0)) total_directpayment_tips,
                   SUM(COALESCE(op.total_loadedvalue_tips, 0)) total_loadedvalue_tips,
                   SUM(COALESCE(op.cp_amount, 0)) cp_amount,
                   SUM(COALESCE(op.cnp_amount, 0)) cnp_amount,
                   SUM(COALESCE(op.beacon_amount, 0)) beacon_amount,
                   SUM(COALESCE(op.apple_pay_amount, 0)) apple_pay_amount,
                   SUM(COALESCE(op.wristband_amount, 0)) wristband_amount,
                   SUM(COALESCE(op.paypal_amount, 0)) paypal_amount,
                   SUM(COALESCE(op.directpayment_amount, 0)) directpayment_amount,
                   SUM(COALESCE(op.loadedvalue_amount, 0)) loadedvalue_amount,
                   SUM(COALESCE(op.no_payment_amount, 0)) no_payment_amount,
                   SUM(COALESCE(op.no_payment_with_fee_tip, 0)) no_payment_with_fee_tip,
                   SUM(COALESCE(op.cp_amount_with_fee_tip, 0)) cp_amount_with_fee_tip,
                   SUM(COALESCE(op.cnp_amount_with_fee_tip, 0)) cnp_amount_with_fee_tip,
                   SUM(COALESCE(op.beacon_amount_with_fee_tip, 0)) beacon_amount_with_fee_tip,
                   SUM(COALESCE(op.apple_pay_amount_with_fee_tip, 0)) apple_pay_amount_with_fee_tip,
                   SUM(COALESCE(op.wristband_amount_with_fee_tip, 0)) wristband_amount_with_fee_tip,
                   SUM(COALESCE(op.paypal_amount_with_fee_tip, 0)) paypal_amount_with_fee_tip,
                   SUM(COALESCE(op.directpayment_amount_with_fee_tip, 0)) directpayment_amount_with_fee_tip,
                   SUM(COALESCE(op.loadedvalue_amount_with_fee_tip, 0)) loadedvalue_amount_with_fee_tip,
                   SUM(COALESCE(op.total_givex_sales, 0)) givex_amount,
                   SUM(COALESCE(op.total_stored_value_tips, 0)) total_stored_value_tips,
                   SUM(COALESCE(op.total_stored_value_fees, 0)) total_stored_value_fees,
                   SUM(COALESCE(op.total_stored_value_taxes, 0)) total_stored_value_taxes,
                   SUM(COALESCE(op.total_credit_sales, 0)) credit_total,
                   SUM(COALESCE(op.total_credit_sales_with_fee_tip, 0)) credit_total_with_fee_tip,
                   SUM(COALESCE(op.total_cash_sales, 0)) cash_total,
                   SUM(COALESCE(op.total_cash_tips, 0)) total_cash_tips,
                   SUM(COALESCE(op.total_cash_sales_with_fee_tip, 0)) cash_total_with_fee_tip,
                   SUM(COALESCE(o.original_subtotal - o.bill_amount, 0)) discount,
                   SUM(COALESCE(op.total_house_sales_with_fee_tip, 0)) AS house_total_with_fee_tip,
                   SUM(COALESCE(op.total_house_sales, 0)) AS house_account_amount,
                   SUM(COALESCE(op.total_house_tips, 0)) AS total_house_tips,
                   SUM(COALESCE(op.total_house_fees, 0)) AS total_house_fees,
                   SUM(COALESCE(op.total_house_taxes, 0)) AS total_house_taxes,
                   SUM(COALESCE(op.total_submit_with_no_payment_sales, 0)) AS submit_with_no_payment_amount,
                   SUM(COALESCE(o.original_subtotal, 0)) amount,
                   SUM(COALESCE(op.total_amex_sales_with_fee_tip, 0)) total_amex_sales_with_fee_tip,
                   SUM(COALESCE(op.amex_cp_amount_with_fee_tip, 0)) amex_cp_amount_with_fee_tip,
                   SUM(COALESCE(op.amex_cnp_amount_with_fee_tip, 0)) amex_cnp_amount_with_fee_tip,
                   SUM(COALESCE(op.total_discover_sales_with_fee_tip, 0)) total_discover_sales_with_fee_tip,
                   SUM(COALESCE(op.discover_cp_amount_with_fee_tip, 0)) discover_cp_amount_with_fee_tip,
                   SUM(COALESCE(op.discover_cnp_amount_with_fee_tip, 0)) discover_cnp_amount_with_fee_tip,
                   SUM(COALESCE(op.total_mastercard_sales_with_fee_tip, 0)) total_mastercard_sales_with_fee_tip,
                   SUM(COALESCE(op.mastercard_cp_amount_with_fee_tip, 0)) mastercard_cp_amount_with_fee_tip,
                   SUM(COALESCE(op.mastercard_cnp_amount_with_fee_tip, 0)) mastercard_cnp_amount_with_fee_tip,
                   SUM(COALESCE(op.total_visa_sales_with_fee_tip, 0)) total_visa_sales_with_fee_tip,
                   SUM(COALESCE(op.visa_cp_amount_with_fee_tip, 0)) visa_cp_amount_with_fee_tip,
                   SUM(COALESCE(op.visa_cnp_amount_with_fee_tip, 0)) visa_cnp_amount_with_fee_tip,
                   SUM(COALESCE(op.total_other_sales_with_fee_tip, 0)) total_other_sales_with_fee_tip,
                   SUM(COALESCE(op.other_cp_amount_with_fee_tip, 0)) other_cp_amount_with_fee_tip,
                   SUM(COALESCE(op.other_cnp_amount_with_fee_tip, 0)) other_cnp_amount_with_fee_tip
                   " . (count($table_cols_name) > 0 ? ", " . implode(", ", $table_cols_name) : "") . ", 
                   '' AS groupColumn, 
                   'null' AS groupColumnValue
            
                FROM orders o
                        JOIN vendors v                  ON v.id = o.vendor_id AND v.venue_id = {$id}
                        JOIN venues vu                  ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active=1) OR vu.total_by_active_layout = 0)
                        JOIN users u                    ON u.id = o.user_id AND u.type in (\"" . implode('", "', $sql_user_type) . "\")
                   LEFT JOIN OrderCustomFees ocf        ON o.id = ocf.orderId
                   LEFT JOIN sales_by_payment_type op   ON o.id = op.order_id
                   LEFT JOIN tax_order_temp tot         ON o.id = tot.order_id 
        
                WHERE 
                      o.order_status IN ('COMPLETED', 'PARTIAL_REFUND')
                  AND o.refund_status NOT IN ('REFUNDED')
                  AND o.{$order_date_field} >= '{$start_date}'
                  AND o.{$order_date_field} < '{$end_date}' ");

       //dd($result);

        $resultCustomTenders = DB::select("SELECT
                               ct.id,
                               ct.name,
                               '' AS groupColumn,
                               'null' AS groupColumnValue,
                               SUM(oct.cashAmount) AS cashAmount,
                               SUM(oct.customTenderAmount) AS customTenderAmount
                               
                             FROM orders o
	                                 JOIN vendors v                 ON v.id = o.vendor_id AND v.venue_id = {$id}
	                                 JOIN venues vu                 ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active=1) OR vu.total_by_active_layout = 0)
	                                 JOIN users u                   ON u.id = o.user_id AND u.type in (\"" . implode('", "', $sql_user_type) . "\")
	                            LEFT JOIN OrderCustomFees ocf       ON o.id = ocf.orderId
                                LEFT JOIN sales_by_payment_type op  ON o.id = op.order_id
                                LEFT JOIN tax_order_temp tot        ON o.id = tot.order_id
                                     JOIN OrderCustomTenders oct    ON (oct.orderId = o.id)
                                     JOIN CustomTender ct           ON (ct.id = oct.customTenderId)
                               
                             WHERE 
                                  o.order_status IN ('COMPLETED', 'PARTIAL_REFUND')        
                              AND o.refund_status NOT IN ('REFUNDED')
                              AND o.{$order_date_field} >= '{$start_date}'
                              AND o.{$order_date_field} < '{$end_date}' 
                              
                            GROUP BY ct.id
                            ORDER BY ct.name ASC");

//dd($resultCustomTenders);


        $resultsIndexed = [];


        foreach ($result as &$r)
        {
            $r->customTenders                    = [];
            $resultsIndexed[$r->groupColumnValue] = $r;
        }

        foreach ($resultCustomTenders as &$ct)
        {
            $ct     = get_object_vars($ct);
            $ct['cashAmount']                                                  = (float)$ct['cashAmount'] ;
            $resultsIndexed[$ct['groupColumnValue']]->customTenders[$ct['id']] = $ct;
        }

        //dd($resultsIndexed);

        return array_values($resultsIndexed);
    }


    private function getCpTenderBreakdownReportVenueByVgId($id = NULL, $start_date, $end_date, $extra_user)
    {
        if ($id === '')
        {
            return [];
        }

//        $id               = $this->db_inventory->escape($id);
//        $start_date       = $this->db_inventory->escape($start_date);
//        $end_date         = $this->db_inventory->escape($end_date);
        $order_date_field = $this->order_date_field;

        $table_cols = $this->multiple_tax_helper_venue($id, $start_date, $end_date);
        $table_cols_name = array_keys($table_cols);

        DB::select('DROP TEMPORARY TABLE IF EXISTS payment_log_by_order');
        DB::select("CREATE TEMPORARY TABLE IF NOT EXISTS payment_log_by_order 
                          (
                            PRIMARY KEY payment_log_by_order_PK (order_id)
                          ) 
                          AS
                          (
                            SELECT
                              o.id         AS order_id,
                              pl.card_type AS card_type,
                              pl.transaction_id,
                              pl.approved
                          
                            FROM orders o
                                   JOIN vendors v ON v.id = o.vendor_id AND v.venue_id = {$id}
                                   JOIN venues vu ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active=1) OR vu.total_by_active_layout = 0)
                                   JOIN payment_log pl  ON o.id = pl.order_id AND pl.transaction_type = 1
                              LEFT JOIN payment_log pl2 ON pl.order_id = pl2.order_id AND pl2.transaction_type = 1 AND (pl.approved < pl2.approved OR (pl.approved = pl2.approved AND pl.transaction_id < pl2.transaction_id))
                            
                            WHERE 
                                  o.{$order_date_field} >= '{$start_date}'
                              AND o.{$order_date_field} < '{$end_date}'
                              AND pl2.id IS NULL
                            
                            GROUP BY o.id, card_type, pl.transaction_id, pl.approved
                          )");

        DB::select('DROP TEMPORARY TABLE IF EXISTS tmp_orders_refunds');
        DB::select("CREATE TEMPORARY TABLE IF NOT EXISTS tmp_orders_refunds 
                        (
  	                      PRIMARY KEY tmp_orders_refunds_PK (order_id, payment_index)
                        ) 
                        AS 
                        (
                          SELECT
                           o.id              AS order_id,
                           op.payment_index  AS payment_index,
                           SUM(rop.amount)   AS refunds_amount,
                           SUM(rop.subtotal) AS refunds_subtotal,
                           SUM(rop.tip)      AS refunds_tip,
                           SUM(rop.fee)      AS refunds_fee,
                           SUM(rop.tax)      AS refunds_tax
                          FROM order_payments op
                                  JOIN orders o ON op.order_id = o.id
                             LEFT JOIN refund_order_payments rop ON rop.order_id = o.id AND rop.refund_status = 'COMPLETED' AND rop.payment_index = op.payment_index
                                  JOIN vendors v                  ON v.id = o.vendor_id AND v.venue_id = {$id}
                                  JOIN venues vu                  ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active = 1) OR vu.total_by_active_layout = 0)
                             LEFT JOIN payment_log_by_order pl    ON o.id = pl.order_id AND op.payment_type IN (1,6,7,8,13,14)
        			   
        			      WHERE 
                                o.{$order_date_field} >= '{$start_date}'
                            AND o.{$order_date_field} < '{$end_date}'
                            AND o.order_status IN ('COMPLETED', 'PARTIAL_REFUND') 
                            AND o.refund_status NOT IN ('REFUNDED')
                            AND op.payment_status IN ('COMPLETED', 'PARTIAL_REFUND')
                        
 					      GROUP BY o.id , op.payment_index
                        )");

        DB::select('DROP TEMPORARY TABLE IF EXISTS sales_by_payment_type');
        DB::select("CREATE TEMPORARY TABLE IF NOT EXISTS sales_by_payment_type
                (
                  PRIMARY KEY sales_by_payment_type_PK (order_id)
                )
                AS
                (
                  SELECT
                    o.id AS order_id,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_subtotal,0), 0)) AS total_cash_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS total_cash_sales_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_cash_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_cash_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_cash_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, 1, 0)) AS cash_tran_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (1,6,7,8,13,14), COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_subtotal,0), 0)) AS total_credit_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (1,6,7,8,13,14), COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS total_credit_sales_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (1,6,7,8,13,14), COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_credit_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (1,6,7,8,13,14), COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_credit_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (1,6,7,8,13,14), COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_credit_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (3,11,16), COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS total_givex_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS total_loadedvalue_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (11,16), COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_stored_value_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (11,16), COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_stored_value_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (11,16), COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_stored_value_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_directpayment_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_directpayment_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_loadedvalue_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_loadedvalue_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 5, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS total_house_sales_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 5, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS total_house_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 5, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_house_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 5, COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_house_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 5, COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_house_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 9, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS total_submit_with_no_payment_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 1 AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS cp_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 1 AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0), 0)) AS cnp_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 1 AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0), 0)) AS cnp_amount_minus_refund,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 6, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS beacon_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 6, 1, 0)) AS beacon_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 7, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS apple_pay_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 7, 1, 0)) AS apple_pay_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 8, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS wristband_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 8, 1, 0)) AS wristband_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 13, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS paypal_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 13, 1, 0)) AS paypal_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS directpayment_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, 1, 0)) AS directpayment_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS loadedvalue_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, 1, 0)) AS loadedvalue_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 9, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS no_payment_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 9, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS no_payment_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 1 AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS cp_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 1 AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS cnp_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 6, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS beacon_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 7, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS apple_pay_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 8, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS wristband_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 13, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS paypal_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0))  AS directpayment_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax)- COALESCE(tor.refunds_amount,0), 0))  AS loadedvalue_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 8, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_wristband_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 13, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_paypal_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_directpayment_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_loadedvalue_tips,
                
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express'), COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS total_amex_sales_with_fee_tip,
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express') AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS amex_cp_amount_with_fee_tip,
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express') AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS amex_cnp_amount_with_fee_tip,
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express'), 1, 0)) AS amex_tran_count,
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express') AND op.is_card_present = 1, 1, 0)) AS amex_cp_tran_count,
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express') AND COALESCE(op.is_card_present,0) <> 1, 1, 0)) AS amex_cnp_tran_count,
                    SUM(IF(pl.card_type = 'Discover', COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS total_discover_sales_with_fee_tip,
                    SUM(IF(pl.card_type = 'Discover' AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS discover_cp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Discover' AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS discover_cnp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Discover', 1, 0)) AS discover_tran_count,
                    SUM(IF(pl.card_type = 'Discover' AND op.is_card_present = 1, 1, 0)) AS discover_cp_tran_count,
                    SUM(IF(pl.card_type = 'Discover' AND COALESCE(op.is_card_present,0) <> 1, 1, 0)) AS discover_cnp_tran_count,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master'), COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS total_mastercard_sales_with_fee_tip,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master') AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS mastercard_cp_amount_with_fee_tip,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master') AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS mastercard_cnp_amount_with_fee_tip,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master'), 1, 0)) AS mastercard_tran_count,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master') AND op.is_card_present = 1, 1, 0)) AS mastercard_cp_tran_count,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master') AND COALESCE(op.is_card_present,0) <> 1, 1, 0)) AS mastercard_cnp_tran_count,
                    SUM(IF(pl.card_type = 'Visa', COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS total_visa_sales_with_fee_tip,
                    SUM(IF(pl.card_type = 'Visa' AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS visa_cp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Visa' AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS visa_cnp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Visa', 1, 0)) AS visa_tran_count,
                    SUM(IF(pl.card_type = 'Visa' AND op.is_card_present = 1, 1, 0)) AS visa_cp_tran_count,
                    SUM(IF(pl.card_type = 'Visa' AND COALESCE(op.is_card_present,0) <> 1, 1, 0)) AS visa_cnp_tran_count,
                    SUM(IF(pl.card_type = 'Other', 1, 0)) AS other_tran_count,
                    SUM(IF(pl.card_type = 'Other' AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS other_cp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Other' AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS other_cnp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Other', COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS total_other_sales_with_fee_tip
                
                  FROM order_payments op
                         JOIN orders o                ON op.order_id = o.id
                    LEFT JOIN tmp_orders_refunds tor  ON (tor.order_id = o.id AND tor.payment_index = op.payment_index)
                         JOIN vendors v                  ON v.id = o.vendor_id AND v.venue_id = {$id}
                         JOIN venues vu                  ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active = 1) OR vu.total_by_active_layout = 0)
                    LEFT JOIN payment_log_by_order pl    ON o.id = pl.order_id AND op.payment_type IN (1,6,7,8,13,14)
        			   
        		  WHERE 
                        o.{$order_date_field} >= '{$start_date}'
                    AND o.{$order_date_field} < '{$end_date}'
                    AND o.order_status IN ('COMPLETED', 'PARTIAL_REFUND') 
                    AND o.refund_status NOT IN ('REFUNDED')
                    AND op.payment_status IN ('COMPLETED', 'PARTIAL_REFUND')
                        
 				  GROUP BY o.id 
 				)  
               ");

        $sql_user_type[] = 'EMPLOYEE';

        if ($extra_user[0] === TRUE)
        {
            $sql_user_type[] = 'GUEST';
        }

        if ($extra_user[1] === TRUE)
        {
            $sql_user_type[] = 'USER';
        }

        $result = DB::select("SELECT 
                   COALESCE(vg.name, '-') vg_name, 
                   COALESCE(vg.id, 0) vg_id,
                   SUM(COALESCE(o.tip, 0)) tip,
                   SUM(COALESCE(o.fee, 0)) fee,
                   ROUND(SUM(COALESCE(ocf.`customFeeValue`, 0)), 2) AS custom_fees,
                   SUM(COALESCE(o.tax, 0)) tax,
                   SUM(COALESCE(op.total_credit_tips, 0)) total_credit_tips,
                   SUM(COALESCE(op.total_credit_fees, 0)) total_credit_fees,
                   SUM(COALESCE(op.total_credit_taxes, 0)) total_credit_taxes,
                   SUM(COALESCE(op.total_cash_fees, 0)) total_cash_fees,
                   SUM(COALESCE(op.total_cash_taxes, 0)) total_cash_taxes,
                   SUM(COALESCE(op.total_wristband_tips, 0)) total_wristband_tips,
                   SUM(COALESCE(op.total_paypal_tips, 0)) total_paypal_tips,
                   SUM(COALESCE(op.total_directpayment_tips, 0)) total_directpayment_tips,
                   SUM(COALESCE(op.total_loadedvalue_tips, 0)) total_loadedvalue_tips,
                   SUM(COALESCE(op.cp_amount, 0)) cp_amount,
                   SUM(COALESCE(op.cnp_amount, 0)) cnp_amount,
                   SUM(COALESCE(op.beacon_amount, 0)) beacon_amount,
                   SUM(COALESCE(op.apple_pay_amount, 0)) apple_pay_amount,
                   SUM(COALESCE(op.wristband_amount, 0)) wristband_amount,
                   SUM(COALESCE(op.paypal_amount, 0)) paypal_amount,
                   SUM(COALESCE(op.directpayment_amount, 0)) directpayment_amount,
                   SUM(COALESCE(op.loadedvalue_amount, 0)) loadedvalue_amount,
                   SUM(COALESCE(op.no_payment_amount, 0)) no_payment_amount,
                   SUM(COALESCE(op.no_payment_with_fee_tip, 0)) no_payment_with_fee_tip,
                   SUM(COALESCE(op.cp_amount_with_fee_tip, 0)) cp_amount_with_fee_tip,
                   SUM(COALESCE(op.cnp_amount_with_fee_tip, 0)) cnp_amount_with_fee_tip,
                   SUM(COALESCE(op.beacon_amount_with_fee_tip, 0)) beacon_amount_with_fee_tip,
                   SUM(COALESCE(op.apple_pay_amount_with_fee_tip, 0)) apple_pay_amount_with_fee_tip,
                   SUM(COALESCE(op.wristband_amount_with_fee_tip, 0)) wristband_amount_with_fee_tip,
                   SUM(COALESCE(op.paypal_amount_with_fee_tip, 0)) paypal_amount_with_fee_tip,
                   SUM(COALESCE(op.directpayment_amount_with_fee_tip, 0)) directpayment_amount_with_fee_tip,
                   SUM(COALESCE(op.loadedvalue_amount_with_fee_tip, 0)) loadedvalue_amount_with_fee_tip,
                   SUM(COALESCE(op.total_givex_sales, 0)) givex_amount,
                   SUM(COALESCE(op.total_stored_value_tips, 0)) total_stored_value_tips,
                   SUM(COALESCE(op.total_stored_value_fees, 0)) total_stored_value_fees,
                   SUM(COALESCE(op.total_stored_value_taxes, 0)) total_stored_value_taxes,
                   SUM(COALESCE(op.total_credit_sales, 0)) credit_total,
                   SUM(COALESCE(op.total_credit_sales_with_fee_tip, 0)) credit_total_with_fee_tip,
                   SUM(COALESCE(op.total_cash_sales, 0)) cash_total,
                   SUM(COALESCE(op.total_cash_tips, 0)) total_cash_tips,
                   SUM(COALESCE(op.total_cash_sales_with_fee_tip, 0)) cash_total_with_fee_tip,
                   SUM(COALESCE(o.original_subtotal - o.bill_amount, 0)) discount,
                   SUM(COALESCE(op.total_house_sales_with_fee_tip, 0)) AS house_total_with_fee_tip,
                   SUM(COALESCE(op.total_house_sales, 0)) AS house_account_amount,
                   SUM(COALESCE(op.total_house_tips, 0)) AS total_house_tips,
                   SUM(COALESCE(op.total_house_fees, 0)) AS total_house_fees,
                   SUM(COALESCE(op.total_house_taxes, 0)) AS total_house_taxes,
                   SUM(COALESCE(op.total_submit_with_no_payment_sales, 0)) AS submit_with_no_payment_amount,
                   SUM(COALESCE(o.original_subtotal, 0)) amount,
                   SUM(COALESCE(op.total_amex_sales_with_fee_tip, 0)) total_amex_sales_with_fee_tip,
                   SUM(COALESCE(op.amex_cp_amount_with_fee_tip, 0)) amex_cp_amount_with_fee_tip,
                   SUM(COALESCE(op.amex_cnp_amount_with_fee_tip, 0)) amex_cnp_amount_with_fee_tip,
                   SUM(COALESCE(op.total_discover_sales_with_fee_tip, 0)) total_discover_sales_with_fee_tip,
                   SUM(COALESCE(op.discover_cp_amount_with_fee_tip, 0)) discover_cp_amount_with_fee_tip,
                   SUM(COALESCE(op.discover_cnp_amount_with_fee_tip, 0)) discover_cnp_amount_with_fee_tip,
                   SUM(COALESCE(op.total_mastercard_sales_with_fee_tip, 0)) total_mastercard_sales_with_fee_tip,
                   SUM(COALESCE(op.mastercard_cp_amount_with_fee_tip, 0)) mastercard_cp_amount_with_fee_tip,
                   SUM(COALESCE(op.mastercard_cnp_amount_with_fee_tip, 0)) mastercard_cnp_amount_with_fee_tip,
                   SUM(COALESCE(op.total_visa_sales_with_fee_tip, 0)) total_visa_sales_with_fee_tip,
                   SUM(COALESCE(op.visa_cp_amount_with_fee_tip, 0)) visa_cp_amount_with_fee_tip,
                   SUM(COALESCE(op.visa_cnp_amount_with_fee_tip, 0)) visa_cnp_amount_with_fee_tip,
                   SUM(COALESCE(op.visa_tran_count, 0)) visa_tran_count,
                   SUM(COALESCE(op.visa_cp_tran_count, 0)) visa_cp_tran_count,
                   SUM(COALESCE(op.visa_cnp_tran_count, 0)) visa_cnp_tran_count,
                   SUM(COALESCE(op.cash_tran_count, 0)) cash_tran_count,
                   SUM(COALESCE(op.mastercard_tran_count, 0)) mastercard_tran_count,
                   SUM(COALESCE(op.mastercard_cp_tran_count, 0)) mastercard_cp_tran_count,
                   SUM(COALESCE(op.mastercard_cnp_tran_count, 0)) mastercard_cnp_tran_count,
                   SUM(COALESCE(op.amex_tran_count, 0)) amex_tran_count,
                   SUM(COALESCE(op.amex_cp_tran_count, 0)) amex_cp_tran_count,
                   SUM(COALESCE(op.amex_cnp_tran_count, 0)) amex_cnp_tran_count,
                   SUM(COALESCE(op.discover_tran_count, 0)) discover_tran_count,
                   SUM(COALESCE(op.discover_cp_tran_count, 0)) discover_cp_tran_count,
                   SUM(COALESCE(op.discover_cnp_tran_count, 0)) discover_cnp_tran_count,
                   SUM(COALESCE(op.beacon_count, 0)) beacon_count,
                   SUM(COALESCE(op.apple_pay_count, 0)) apple_pay_count,
                   SUM(COALESCE(op.wristband_count, 0)) wristband_count,
                   SUM(COALESCE(op.paypal_count, 0)) paypal_count,
                   SUM(COALESCE(op.loadedvalue_count, 0)) loadedvalue_count,
                   SUM(COALESCE(op.directpayment_count, 0)) directpayment_count
                   " . (count($table_cols_name) > 0 ? ", " . implode(", ", $table_cols_name) : "") . "
                   , 'vg.id' AS groupColumn, vg.id AS groupColumnValue
                   
                FROM  orders o
	                     JOIN vendors v                         ON v.id = o.vendor_id AND v.venue_id = {$id}
	                     JOIN venues vu                         ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active=1) OR vu.total_by_active_layout = 0)
                         JOIN vendors_serving_vendor_group vsg  ON v.id = vsg.vendor_id
                         JOIN vendor_group vg                   ON vg.id = vsg.vendor_group_id AND vg.status = 1
                         join users u                           on u.id = o.user_id AND u.type in (\"" . implode('", "', $sql_user_type) . "\")
	                LEFT JOIN OrderCustomFees ocf ON o.id = ocf.orderId
                    LEFT JOIN sales_by_payment_type op          on o.id = op.order_id
                    LEFT JOIN tax_order_temp tot                ON o.id = tot.order_id
                
                WHERE 
                        o.order_status IN ('COMPLETED', 'PARTIAL_REFUND')
                    AND o.refund_status NOT IN ('REFUNDED')
                    AND o.{$order_date_field} >= '{$start_date}'
                    AND o.{$order_date_field} < '{$end_date}' GROUP BY vg.id ");

        $resultCustomTenders = DB::select("  SELECT
                                  ct.id,
                                  ct.name,
                                  'vg.id' AS groupColumn,
                                  vg.id AS groupColumnValue,
                                  SUM(oct.cashAmount) AS cashAmount,
                                  SUM(oct.customTenderAmount) AS customTenderAmount
                                  
                                FROM  orders o
	                                   JOIN vendors v ON v.id = o.vendor_id AND v.venue_id = {$id}
	                                   JOIN venues vu ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active=1) OR vu.total_by_active_layout = 0)
                                       JOIN vendors_serving_vendor_group vsg ON v.id = vsg.vendor_id
                                       JOIN vendor_group vg ON vg.id = vsg.vendor_group_id AND vg.status = 1
                                       join users u on u.id = o.user_id AND u.type in (\"" . implode('", "', $sql_user_type) . "\")
                                  LEFT JOIN sales_by_payment_type op on o.id = op.order_id
                                  LEFT JOIN tax_order_temp tot ON o.id = tot.order_id
                                       JOIN OrderCustomTenders oct ON (oct.orderId = o.id)
                                       JOIN CustomTender ct ON (ct.id = oct.customTenderId)
                              
                                WHERE 
                                      o.order_status IN ('COMPLETED', 'PARTIAL_REFUND')
                                  AND o.refund_status NOT IN ('REFUNDED')
                                  AND o.{$order_date_field} >= '{$start_date}'
                                  AND o.{$order_date_field} < '{$end_date}' 
                                
                                GROUP BY vg.id, ct.id
                                ORDER BY ct.name ASC");

        $resultsIndexed = [];


        foreach ($result as &$r)
        {
            $r->customTenders                     = [];
            $resultsIndexed[$r->groupColumnValue] = $r;
        }

        foreach ($resultCustomTenders as &$ct)
        {
            $ct     = get_object_vars($ct);
            $ct['cashAmount']                                                  = (float)$ct['cashAmount'] ;
            $resultsIndexed[$ct['groupColumnValue']]->customTenders[$ct['id']] = $ct;
        }

        return array_values($resultsIndexed);
    }


    private function getCpTenderBreakdownReportVenueByVendorId($id = NULL, $start_date, $end_date, $extra_user)
    {
        if ($id === '')
        {
            return [];
        }

//        $id               = $this->db_inventory->escape($id);
//        $start_date       = $this->db_inventory->escape($start_date);
//        $end_date         = $this->db_inventory->escape($end_date);
        $order_date_field = $this->order_date_field;

        $table_cols = $this->multiple_tax_helper_venue($id, $start_date, $end_date);
        $table_cols_name = array_keys($table_cols);

        DB::select('DROP TEMPORARY TABLE IF EXISTS payment_log_by_order');
        DB::select("CREATE TEMPORARY TABLE IF NOT EXISTS payment_log_by_order 
                          (
                            PRIMARY KEY payment_log_by_order_PK (order_id)
                          ) 
                          AS
                          (
                            SELECT
                              o.id         AS order_id,
                              pl.card_type AS card_type,
                              pl.transaction_id,
                              pl.approved
                          
                            FROM orders o
                                   JOIN vendors v ON v.id = o.vendor_id AND v.venue_id = {$id}
                                   JOIN venues vu ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active=1) OR vu.total_by_active_layout = 0)
                                   JOIN payment_log pl  ON o.id = pl.order_id AND pl.transaction_type = 1
                              LEFT JOIN payment_log pl2 ON pl.order_id = pl2.order_id AND pl2.transaction_type = 1 AND (pl.approved < pl2.approved OR (pl.approved = pl2.approved AND pl.transaction_id < pl2.transaction_id))
                            
                            WHERE 
                                  o.{$order_date_field} >= '{$start_date}'
                              AND o.{$order_date_field} < '{$end_date}'
                              AND pl2.id IS NULL
                            
                            GROUP BY o.id, card_type, pl.transaction_id, pl.approved
                          )");

        DB::select('DROP TEMPORARY TABLE IF EXISTS tmp_orders_refunds');
        DB::select("CREATE TEMPORARY TABLE IF NOT EXISTS tmp_orders_refunds 
                        (
  	                      PRIMARY KEY tmp_orders_refunds_PK (order_id, payment_index)
                        ) 
                        AS 
                        (
                          SELECT
                           o.id              AS order_id,
                           op.payment_index  AS payment_index,
                           SUM(rop.amount)   AS refunds_amount,
                           SUM(rop.subtotal) AS refunds_subtotal,
                           SUM(rop.tip)      AS refunds_tip,
                           SUM(rop.fee)      AS refunds_fee,
                           SUM(rop.tax)      AS refunds_tax
                          FROM order_payments op
                                  JOIN orders o ON op.order_id = o.id
                             LEFT JOIN refund_order_payments rop ON rop.order_id = o.id AND rop.refund_status = 'COMPLETED' AND rop.payment_index = op.payment_index
                                  JOIN vendors v                  ON v.id = o.vendor_id AND v.venue_id = {$id}
                                  JOIN venues vu                  ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active = 1) OR vu.total_by_active_layout = 0)
                             LEFT JOIN payment_log_by_order pl    ON o.id = pl.order_id AND op.payment_type IN (1,6,7,8,13,14)
        			   
        			      WHERE 
                                o.{$order_date_field} >= '{$start_date}'
                            AND o.{$order_date_field} < '{$end_date}'
                            AND o.order_status IN ('COMPLETED', 'PARTIAL_REFUND') 
                            AND o.refund_status NOT IN ('REFUNDED')
                            AND op.payment_status IN ('COMPLETED', 'PARTIAL_REFUND')
                        
 					      GROUP BY o.id , op.payment_index
                        )");

        DB::select('DROP TEMPORARY TABLE IF EXISTS sales_by_payment_type');
        DB::select("CREATE TEMPORARY TABLE IF NOT EXISTS sales_by_payment_type
                (
                  PRIMARY KEY sales_by_payment_type_PK (order_id)
                )
                AS
                (
                  SELECT
                    o.id AS order_id,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_subtotal,0), 0)) AS total_cash_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS total_cash_sales_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_cash_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_cash_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_cash_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, 1, 0)) AS cash_tran_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (1,6,7,8,13,14), COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_subtotal,0), 0)) AS total_credit_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (1,6,7,8,13,14), COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS total_credit_sales_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (1,6,7,8,13,14), COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_credit_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (1,6,7,8,13,14), COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_credit_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (1,6,7,8,13,14), COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_credit_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (3,11,16), COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS total_givex_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS total_loadedvalue_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (11,16), COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_stored_value_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (11,16), COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_stored_value_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (11,16), COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_stored_value_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_directpayment_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_directpayment_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_loadedvalue_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_loadedvalue_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 5, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS total_house_sales_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 5, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS total_house_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 5, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_house_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 5, COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_house_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 5, COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_house_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 9, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS total_submit_with_no_payment_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 1 AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS cp_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 1 AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0), 0)) AS cnp_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 1 AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0), 0)) AS cnp_amount_minus_refund,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 6, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS beacon_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 6, 1, 0)) AS beacon_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 7, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS apple_pay_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 7, 1, 0)) AS apple_pay_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 8, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS wristband_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 8, 1, 0)) AS wristband_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 13, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS paypal_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 13, 1, 0)) AS paypal_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS directpayment_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, 1, 0)) AS directpayment_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS loadedvalue_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, 1, 0)) AS loadedvalue_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 9, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS no_payment_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 9, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS no_payment_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 1 AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS cp_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 1 AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS cnp_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 6, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS beacon_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 7, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS apple_pay_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 8, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS wristband_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 13, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS paypal_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0))  AS directpayment_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax)- COALESCE(tor.refunds_amount,0), 0))  AS loadedvalue_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 8, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_wristband_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 13, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_paypal_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_directpayment_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_loadedvalue_tips,
                
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express'), COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS total_amex_sales_with_fee_tip,
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express') AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS amex_cp_amount_with_fee_tip,
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express') AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS amex_cnp_amount_with_fee_tip,
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express'), 1, 0)) AS amex_tran_count,
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express') AND op.is_card_present = 1, 1, 0)) AS amex_cp_tran_count,
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express') AND COALESCE(op.is_card_present,0) <> 1, 1, 0)) AS amex_cnp_tran_count,
                    SUM(IF(pl.card_type = 'Discover', COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS total_discover_sales_with_fee_tip,
                    SUM(IF(pl.card_type = 'Discover' AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS discover_cp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Discover' AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS discover_cnp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Discover', 1, 0)) AS discover_tran_count,
                    SUM(IF(pl.card_type = 'Discover' AND op.is_card_present = 1, 1, 0)) AS discover_cp_tran_count,
                    SUM(IF(pl.card_type = 'Discover' AND COALESCE(op.is_card_present,0) <> 1, 1, 0)) AS discover_cnp_tran_count,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master'), COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS total_mastercard_sales_with_fee_tip,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master') AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS mastercard_cp_amount_with_fee_tip,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master') AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS mastercard_cnp_amount_with_fee_tip,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master'), 1, 0)) AS mastercard_tran_count,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master') AND op.is_card_present = 1, 1, 0)) AS mastercard_cp_tran_count,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master') AND COALESCE(op.is_card_present,0) <> 1, 1, 0)) AS mastercard_cnp_tran_count,
                    SUM(IF(pl.card_type = 'Visa', COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS total_visa_sales_with_fee_tip,
                    SUM(IF(pl.card_type = 'Visa' AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS visa_cp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Visa' AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS visa_cnp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Visa', 1, 0)) AS visa_tran_count,
                    SUM(IF(pl.card_type = 'Visa' AND op.is_card_present = 1, 1, 0)) AS visa_cp_tran_count,
                    SUM(IF(pl.card_type = 'Visa' AND COALESCE(op.is_card_present,0) <> 1, 1, 0)) AS visa_cnp_tran_count,
                    SUM(IF(pl.card_type = 'Other', 1, 0)) AS other_tran_count,
                    SUM(IF(pl.card_type = 'Other' AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS other_cp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Other' AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS other_cnp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Other', COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS total_other_sales_with_fee_tip
                
                  FROM order_payments op
                         JOIN orders o                ON op.order_id = o.id
                    LEFT JOIN tmp_orders_refunds tor  ON (tor.order_id = o.id AND tor.payment_index = op.payment_index)
                         JOIN vendors v                  ON v.id = o.vendor_id AND v.venue_id = {$id}
                         JOIN venues vu                  ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active = 1) OR vu.total_by_active_layout = 0)
                    LEFT JOIN payment_log_by_order pl    ON o.id = pl.order_id AND op.payment_type IN (1,6,7,8,13,14)
        			   
        		  WHERE 
                        o.{$order_date_field} >= '{$start_date}'
                    AND o.{$order_date_field} < '{$end_date}'
                    AND o.order_status IN ('COMPLETED', 'PARTIAL_REFUND') 
                    AND o.refund_status NOT IN ('REFUNDED')
                    AND op.payment_status IN ('COMPLETED', 'PARTIAL_REFUND')
                        
 				  GROUP BY o.id 
 				)  
               ");

        $sql_user_type[] = 'EMPLOYEE';

        if ($extra_user[0] === TRUE)
        {
            $sql_user_type[] = 'GUEST';
        }

        if ($extra_user[1] === TRUE)
        {
            $sql_user_type[] = 'USER';
        }

        $result = DB::select(" SELECT 
                           v.name vendor_name, 
                           v.id v_id, 
                           SUM(COALESCE(o.tip, 0)) tip,
                           SUM(COALESCE(o.fee, 0)) fee,
                           ROUND(SUM(COALESCE(ocf.`customFeeValue`, 0)), 2) AS custom_fees,
                           SUM(COALESCE(o.tax, 0)) tax,
                           SUM(COALESCE(op.total_credit_tips, 0)) total_credit_tips,
                           SUM(COALESCE(op.total_credit_fees, 0)) total_credit_fees,
                           SUM(COALESCE(op.total_credit_taxes, 0)) total_credit_taxes,
                           SUM(COALESCE(op.total_cash_fees, 0)) total_cash_fees,
                           SUM(COALESCE(op.total_cash_taxes, 0)) total_cash_taxes,
                           SUM(COALESCE(op.total_wristband_tips, 0)) total_wristband_tips,
                           SUM(COALESCE(op.total_paypal_tips, 0)) total_paypal_tips,
                           SUM(COALESCE(op.total_directpayment_tips, 0)) total_directpayment_tips,
                           SUM(COALESCE(op.total_loadedvalue_tips, 0)) total_loadedvalue_tips,
                           SUM(COALESCE(op.cp_amount, 0)) cp_amount,
                           SUM(COALESCE(op.cnp_amount, 0)) cnp_amount,
                           SUM(COALESCE(op.beacon_amount, 0)) beacon_amount,
                           SUM(COALESCE(op.apple_pay_amount, 0)) apple_pay_amount,
                           SUM(COALESCE(op.wristband_amount, 0)) wristband_amount,
                           SUM(COALESCE(op.paypal_amount, 0)) paypal_amount,
                           SUM(COALESCE(op.directpayment_amount, 0)) directpayment_amount,
                           SUM(COALESCE(op.loadedvalue_amount, 0)) loadedvalue_amount,
                           SUM(COALESCE(op.no_payment_amount, 0)) no_payment_amount,
                           SUM(COALESCE(op.no_payment_with_fee_tip, 0)) no_payment_with_fee_tip,
                           SUM(COALESCE(op.cp_amount_with_fee_tip, 0)) cp_amount_with_fee_tip,
                           SUM(COALESCE(op.cnp_amount_with_fee_tip, 0)) cnp_amount_with_fee_tip,
                           SUM(COALESCE(op.beacon_amount_with_fee_tip, 0)) beacon_amount_with_fee_tip,
                           SUM(COALESCE(op.apple_pay_amount_with_fee_tip, 0)) apple_pay_amount_with_fee_tip,
                           SUM(COALESCE(op.wristband_amount_with_fee_tip, 0)) wristband_amount_with_fee_tip,
                           SUM(COALESCE(op.paypal_amount_with_fee_tip, 0)) paypal_amount_with_fee_tip,
                           SUM(COALESCE(op.directpayment_amount_with_fee_tip, 0)) directpayment_amount_with_fee_tip,
                           SUM(COALESCE(op.loadedvalue_amount_with_fee_tip, 0)) loadedvalue_amount_with_fee_tip,
                           SUM(COALESCE(op.total_givex_sales, 0)) givex_amount,
                           SUM(COALESCE(op.total_stored_value_tips, 0)) total_stored_value_tips,
                           SUM(COALESCE(op.total_stored_value_fees, 0)) total_stored_value_fees,
                           SUM(COALESCE(op.total_stored_value_taxes, 0)) total_stored_value_taxes,
                           SUM(COALESCE(op.total_credit_sales, 0)) credit_total,
                           SUM(COALESCE(op.total_credit_sales_with_fee_tip, 0)) credit_total_with_fee_tip,
                           SUM(COALESCE(op.total_cash_sales, 0)) cash_total,
                           SUM(COALESCE(op.total_cash_tips, 0)) total_cash_tips,
                           SUM(COALESCE(op.total_cash_sales_with_fee_tip, 0)) cash_total_with_fee_tip,
                           SUM(COALESCE(o.original_subtotal - o.bill_amount, 0)) discount,
                           SUM(COALESCE(op.total_house_sales_with_fee_tip, 0)) AS house_total_with_fee_tip,
                           SUM(COALESCE(op.total_house_sales, 0)) AS house_account_amount,
                           SUM(COALESCE(op.total_house_tips, 0)) AS total_house_tips,
                           SUM(COALESCE(op.total_house_fees, 0)) AS total_house_fees,
                           SUM(COALESCE(op.total_house_taxes, 0)) AS total_house_taxes,
                           SUM(COALESCE(op.total_submit_with_no_payment_sales, 0)) AS submit_with_no_payment_amount,
                           SUM(COALESCE(o.original_subtotal, 0)) amount,
                           SUM(COALESCE(op.total_amex_sales_with_fee_tip, 0)) total_amex_sales_with_fee_tip,
                           SUM(COALESCE(op.amex_cp_amount_with_fee_tip, 0)) amex_cp_amount_with_fee_tip,
                           SUM(COALESCE(op.amex_cnp_amount_with_fee_tip, 0)) amex_cnp_amount_with_fee_tip,
                           SUM(COALESCE(op.total_discover_sales_with_fee_tip, 0)) total_discover_sales_with_fee_tip,
                           SUM(COALESCE(op.discover_cp_amount_with_fee_tip, 0)) discover_cp_amount_with_fee_tip,
                           SUM(COALESCE(op.discover_cnp_amount_with_fee_tip, 0)) discover_cnp_amount_with_fee_tip,
                           SUM(COALESCE(op.total_mastercard_sales_with_fee_tip, 0)) total_mastercard_sales_with_fee_tip,
                           SUM(COALESCE(op.mastercard_cp_amount_with_fee_tip, 0)) mastercard_cp_amount_with_fee_tip,
                           SUM(COALESCE(op.mastercard_cnp_amount_with_fee_tip, 0)) mastercard_cnp_amount_with_fee_tip,
                           SUM(COALESCE(op.total_visa_sales_with_fee_tip, 0)) total_visa_sales_with_fee_tip,
                           SUM(COALESCE(op.visa_cp_amount_with_fee_tip, 0)) visa_cp_amount_with_fee_tip,
                           SUM(COALESCE(op.visa_cnp_amount_with_fee_tip, 0)) visa_cnp_amount_with_fee_tip,
                           SUM(COALESCE(op.visa_tran_count, 0)) visa_tran_count,
                           SUM(COALESCE(op.visa_cp_tran_count, 0)) visa_cp_tran_count,
                           SUM(COALESCE(op.visa_cnp_tran_count, 0)) visa_cnp_tran_count,
                           SUM(COALESCE(op.cash_tran_count, 0)) cash_tran_count,
                           SUM(COALESCE(op.mastercard_tran_count, 0)) mastercard_tran_count,
                           SUM(COALESCE(op.mastercard_cp_tran_count, 0)) mastercard_cp_tran_count,
                           SUM(COALESCE(op.mastercard_cnp_tran_count, 0)) mastercard_cnp_tran_count,
                           SUM(COALESCE(op.amex_tran_count, 0)) amex_tran_count,
                           SUM(COALESCE(op.amex_cp_tran_count, 0)) amex_cp_tran_count,
                           SUM(COALESCE(op.amex_cnp_tran_count, 0)) amex_cnp_tran_count,
                           SUM(COALESCE(op.discover_tran_count, 0)) discover_tran_count,
                           SUM(COALESCE(op.discover_cp_tran_count, 0)) discover_cp_tran_count,
                           SUM(COALESCE(op.discover_cnp_tran_count, 0)) discover_cnp_tran_count,
                           SUM(COALESCE(op.beacon_count, 0)) beacon_count,
                           SUM(COALESCE(op.apple_pay_count, 0)) apple_pay_count,
                           SUM(COALESCE(op.wristband_count, 0)) wristband_count,
                           SUM(COALESCE(op.paypal_count, 0)) paypal_count,
                           SUM(COALESCE(op.loadedvalue_count, 0)) loadedvalue_count,
                           SUM(COALESCE(op.directpayment_count, 0)) directpayment_count
                           " . (count($table_cols_name) > 0 ? ", " . implode(", ", $table_cols_name) : "") . ", 
                           'v.id' AS groupColumn, v.id AS groupColumnValue
                           
                           FROM orders o
	                                 JOIN vendors v ON v.id = o.vendor_id AND v.venue_id = {$id}
	                                 JOIN venues vu ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active=1) OR vu.total_by_active_layout = 0)
	                                 join users u on u.id = o.user_id AND u.type in (\"" . implode('", "', $sql_user_type) . "\")
	                            LEFT JOIN OrderCustomFees ocf ON o.id = ocf.orderId
                                LEFT JOIN sales_by_payment_type op on o.id = op.order_id
                                LEFT JOIN tax_order_temp tot ON o.id = tot.order_id 
                                
                           WHERE 
                                o.order_status IN ('COMPLETED', 'PARTIAL_REFUND')
                            AND o.refund_status NOT IN ('REFUNDED')
                            AND o.{$order_date_field} >= '{$start_date}'
                            AND o.{$order_date_field} < '{$end_date}' " . " GROUP BY v.id 
                            ");

        //dd($result);

        $resultCustomTenders = DB::select("SELECT
                               ct.id,
                               ct.name,
                               'v.id' AS groupColumn,
                               v.id AS groupColumnValue,
                               SUM(oct.cashAmount) AS cashAmount,
                               SUM(oct.customTenderAmount) AS customTenderAmount
                             FROM orders o
	                    JOIN vendors v ON v.id = o.vendor_id AND v.venue_id = {$id}
	                    JOIN venues vu ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active=1) OR vu.total_by_active_layout = 0)
	                    join users u on u.id = o.user_id AND u.type in (\"" . implode('", "', $sql_user_type) . "\")
                    LEFT JOIN sales_by_payment_type op on o.id = op.order_id
                    LEFT JOIN tax_order_temp tot ON o.id = tot.order_id
                               JOIN OrderCustomTenders oct ON (oct.orderId = o.id)
                               JOIN CustomTender ct ON (ct.id = oct.customTenderId)
                               WHERE o.order_status IN ('COMPLETED', 'PARTIAL_REFUND')
            AND o.refund_status NOT IN ('REFUNDED')
            AND o.{$order_date_field} >= '{$start_date}'
            AND o.{$order_date_field} < '{$end_date}' 
                               GROUP BY v.id , ct.id
                             ORDER BY ct.name ASC");

        //dd($resultCustomTenders);

        $resultsIndexed = [];


        foreach ($result as &$r)
        {
            $r->customTenders                    = [];
            $resultsIndexed[$r->groupColumnValue] = $r;
        }

        foreach ($resultCustomTenders as &$ct)
        {
            $ct     = get_object_vars($ct);
            $ct['cashAmount']                                                  = (float)$ct['cashAmount'] ;
            $resultsIndexed[$ct['groupColumnValue']]->customTenders[$ct['id']] = $ct;
        }

        //dd($resultsIndexed);

        return array_values($resultsIndexed);
    }


    private function getCpTenderBreakdownReportVenueByVenueId($id = NULL, $start_date, $end_date, $extra_user)
    {
        if ($id === '')
        {
            return [];
        }

//        $id               = $this->db_inventory->escape($id);
//        $start_date       = $this->db_inventory->escape($start_date);
//        $end_date         = $this->db_inventory->escape($end_date);
        $order_date_field = $this->order_date_field;

        $table_cols = $this->multiple_tax_helper_venue($id, $start_date, $end_date);
        $table_cols_name = array_keys($table_cols);

        DB::select('DROP TEMPORARY TABLE IF EXISTS payment_log_by_order');
        DB::select("CREATE TEMPORARY TABLE IF NOT EXISTS payment_log_by_order 
                          (
                            PRIMARY KEY payment_log_by_order_PK (order_id)
                          ) 
                          AS
                          (
                            SELECT
                              o.id         AS order_id,
                              pl.card_type AS card_type,
                              pl.transaction_id,
                              pl.approved
                          
                            FROM orders o
                                   JOIN vendors v ON v.id = o.vendor_id AND v.venue_id = {$id}
                                   JOIN venues vu ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active=1) OR vu.total_by_active_layout = 0)
                                   JOIN payment_log pl  ON o.id = pl.order_id AND pl.transaction_type = 1
                              LEFT JOIN payment_log pl2 ON pl.order_id = pl2.order_id AND pl2.transaction_type = 1 AND (pl.approved < pl2.approved OR (pl.approved = pl2.approved AND pl.transaction_id < pl2.transaction_id))
                            
                            WHERE 
                                  o.{$order_date_field} >= '{$start_date}'
                              AND o.{$order_date_field} < '{$end_date}'
                              AND pl2.id IS NULL
                            
                            GROUP BY o.id
                          )");

        DB::select('DROP TEMPORARY TABLE IF EXISTS tmp_orders_refunds');
        DB::select("CREATE TEMPORARY TABLE IF NOT EXISTS tmp_orders_refunds 
                        (
  	                      PRIMARY KEY tmp_orders_refunds_PK (order_id, payment_index)
                        ) 
                        AS 
                        (
                          SELECT
                           o.id              AS order_id,
                           op.payment_index  AS payment_index,
                           SUM(rop.amount)   AS refunds_amount,
                           SUM(rop.subtotal) AS refunds_subtotal,
                           SUM(rop.tip)      AS refunds_tip,
                           SUM(rop.fee)      AS refunds_fee,
                           SUM(rop.tax)      AS refunds_tax
                          FROM order_payments op
                                  JOIN orders o ON op.order_id = o.id
                             LEFT JOIN refund_order_payments rop ON rop.order_id = o.id AND rop.refund_status = 'COMPLETED' AND rop.payment_index = op.payment_index
                                  JOIN vendors v                  ON v.id = o.vendor_id AND v.venue_id = {$id}
                                  JOIN venues vu                  ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active = 1) OR vu.total_by_active_layout = 0)
                             LEFT JOIN payment_log_by_order pl    ON o.id = pl.order_id AND op.payment_type IN (1,6,7,8,13,14)
        			   
        			      WHERE 
                                o.{$order_date_field} >= '{$start_date}'
                            AND o.{$order_date_field} < '{$end_date}'
                            AND o.order_status IN ('COMPLETED', 'PARTIAL_REFUND') 
                            AND o.refund_status NOT IN ('REFUNDED')
                            AND op.payment_status IN ('COMPLETED', 'PARTIAL_REFUND')
                        
 					      GROUP BY o.id , op.payment_index
                        )");

        DB::select('DROP TEMPORARY TABLE IF EXISTS sales_by_payment_type');
        DB::select("CREATE TEMPORARY TABLE IF NOT EXISTS sales_by_payment_type
                (
                  PRIMARY KEY sales_by_payment_type_PK (order_id)
                )
                AS
                (
                  SELECT
                    o.id AS order_id,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_subtotal,0), 0)) AS total_cash_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS total_cash_sales_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_cash_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_cash_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_cash_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, 1, 0)) AS cash_tran_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (1,6,7,8,13,14), COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_subtotal,0), 0)) AS total_credit_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (1,6,7,8,13,14), COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS total_credit_sales_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (1,6,7,8,13,14), COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_credit_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (1,6,7,8,13,14), COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_credit_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (1,6,7,8,13,14), COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_credit_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (3,11,16), COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS total_givex_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS total_loadedvalue_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (11,16), COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_stored_value_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (11,16), COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_stored_value_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (11,16), COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_stored_value_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_directpayment_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_directpayment_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_loadedvalue_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_loadedvalue_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 5, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS total_house_sales_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 5, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS total_house_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 5, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_house_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 5, COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_house_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 5, COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_house_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 9, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS total_submit_with_no_payment_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 1 AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS cp_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 1 AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0), 0)) AS cnp_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 1 AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0), 0)) AS cnp_amount_minus_refund,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 6, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS beacon_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 6, 1, 0)) AS beacon_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 7, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS apple_pay_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 7, 1, 0)) AS apple_pay_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 8, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS wristband_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 8, 1, 0)) AS wristband_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 13, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS paypal_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 13, 1, 0)) AS paypal_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS directpayment_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, 1, 0)) AS directpayment_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS loadedvalue_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, 1, 0)) AS loadedvalue_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 9, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS no_payment_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 9, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS no_payment_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 1 AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS cp_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 1 AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS cnp_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 6, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS beacon_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 7, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS apple_pay_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 8, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS wristband_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 13, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS paypal_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0))  AS directpayment_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax)- COALESCE(tor.refunds_amount,0), 0))  AS loadedvalue_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 8, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_wristband_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 13, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_paypal_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_directpayment_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_loadedvalue_tips,
                
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express'), COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS total_amex_sales_with_fee_tip,
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express') AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS amex_cp_amount_with_fee_tip,
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express') AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS amex_cnp_amount_with_fee_tip,
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express'), 1, 0)) AS amex_tran_count,
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express') AND op.is_card_present = 1, 1, 0)) AS amex_cp_tran_count,
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express') AND COALESCE(op.is_card_present,0) <> 1, 1, 0)) AS amex_cnp_tran_count,
                    SUM(IF(pl.card_type = 'Discover', COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS total_discover_sales_with_fee_tip,
                    SUM(IF(pl.card_type = 'Discover' AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS discover_cp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Discover' AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS discover_cnp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Discover', 1, 0)) AS discover_tran_count,
                    SUM(IF(pl.card_type = 'Discover' AND op.is_card_present = 1, 1, 0)) AS discover_cp_tran_count,
                    SUM(IF(pl.card_type = 'Discover' AND COALESCE(op.is_card_present,0) <> 1, 1, 0)) AS discover_cnp_tran_count,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master'), COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS total_mastercard_sales_with_fee_tip,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master') AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS mastercard_cp_amount_with_fee_tip,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master') AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS mastercard_cnp_amount_with_fee_tip,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master'), 1, 0)) AS mastercard_tran_count,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master') AND op.is_card_present = 1, 1, 0)) AS mastercard_cp_tran_count,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master') AND COALESCE(op.is_card_present,0) <> 1, 1, 0)) AS mastercard_cnp_tran_count,
                    SUM(IF(pl.card_type = 'Visa', COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS total_visa_sales_with_fee_tip,
                    SUM(IF(pl.card_type = 'Visa' AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS visa_cp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Visa' AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS visa_cnp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Visa', 1, 0)) AS visa_tran_count,
                    SUM(IF(pl.card_type = 'Visa' AND op.is_card_present = 1, 1, 0)) AS visa_cp_tran_count,
                    SUM(IF(pl.card_type = 'Visa' AND COALESCE(op.is_card_present,0) <> 1, 1, 0)) AS visa_cnp_tran_count,
                    SUM(IF(pl.card_type = 'Other', 1, 0)) AS other_tran_count,
                    SUM(IF(pl.card_type = 'Other' AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS other_cp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Other' AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS other_cnp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Other', COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS total_other_sales_with_fee_tip
                
                  FROM order_payments op
                         JOIN orders o                ON op.order_id = o.id
                    LEFT JOIN tmp_orders_refunds tor  ON (tor.order_id = o.id AND tor.payment_index = op.payment_index)
                         JOIN vendors v                  ON v.id = o.vendor_id AND v.venue_id = {$id}
                         JOIN venues vu                  ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active = 1) OR vu.total_by_active_layout = 0)
                    LEFT JOIN payment_log_by_order pl    ON o.id = pl.order_id AND op.payment_type IN (1,6,7,8,13,14)
        			   
        		  WHERE 
                        o.{$order_date_field} >= '{$start_date}'
                    AND o.{$order_date_field} < '{$end_date}'
                    AND o.order_status IN ('COMPLETED', 'PARTIAL_REFUND') 
                    AND o.refund_status NOT IN ('REFUNDED')
                    AND op.payment_status IN ('COMPLETED', 'PARTIAL_REFUND')
                        
 				  GROUP BY o.id 
 				)  
               ");

        $sql_user_type[] = 'EMPLOYEE';

        if ($extra_user[0] === TRUE)
        {
            $sql_user_type[] = 'GUEST';
        }

        if ($extra_user[1] === TRUE)
        {
            $sql_user_type[] = 'USER';
        }

        $result = DB::select(" SELECT vu.name venue_name, vu.id vu_id, SUM(COALESCE(o.tip, 0)) tip,
                           SUM(COALESCE(o.fee, 0)) fee,
                           ROUND(SUM(COALESCE(ocf.`customFeeValue`, 0)), 2) AS custom_fees,
                           SUM(COALESCE(o.tax, 0)) tax,
                           SUM(COALESCE(op.total_credit_tips, 0)) total_credit_tips,
                           SUM(COALESCE(op.total_credit_fees, 0)) total_credit_fees,
                           SUM(COALESCE(op.total_credit_taxes, 0)) total_credit_taxes,
                           SUM(COALESCE(op.total_cash_fees, 0)) total_cash_fees,
                           SUM(COALESCE(op.total_cash_taxes, 0)) total_cash_taxes,
                           SUM(COALESCE(op.total_wristband_tips, 0)) total_wristband_tips,
                           SUM(COALESCE(op.total_paypal_tips, 0)) total_paypal_tips,
                           SUM(COALESCE(op.total_directpayment_tips, 0)) total_directpayment_tips,
                           SUM(COALESCE(op.total_loadedvalue_tips, 0)) total_loadedvalue_tips,
                           SUM(COALESCE(op.cp_amount, 0)) cp_amount,
                           SUM(COALESCE(op.cnp_amount, 0)) cnp_amount,
                           SUM(COALESCE(op.beacon_amount, 0)) beacon_amount,
                           SUM(COALESCE(op.apple_pay_amount, 0)) apple_pay_amount,
                           SUM(COALESCE(op.wristband_amount, 0)) wristband_amount,
                           SUM(COALESCE(op.paypal_amount, 0)) paypal_amount,
                           SUM(COALESCE(op.directpayment_amount, 0)) directpayment_amount,
                           SUM(COALESCE(op.loadedvalue_amount, 0)) loadedvalue_amount,
                           SUM(COALESCE(op.no_payment_amount, 0)) no_payment_amount,
                           SUM(COALESCE(op.no_payment_with_fee_tip, 0)) no_payment_with_fee_tip,
                           SUM(COALESCE(op.cp_amount_with_fee_tip, 0)) cp_amount_with_fee_tip,
                           SUM(COALESCE(op.cnp_amount_with_fee_tip, 0)) cnp_amount_with_fee_tip,
                           SUM(COALESCE(op.beacon_amount_with_fee_tip, 0)) beacon_amount_with_fee_tip,
                           SUM(COALESCE(op.apple_pay_amount_with_fee_tip, 0)) apple_pay_amount_with_fee_tip,
                           SUM(COALESCE(op.wristband_amount_with_fee_tip, 0)) wristband_amount_with_fee_tip,
                           SUM(COALESCE(op.paypal_amount_with_fee_tip, 0)) paypal_amount_with_fee_tip,
                           SUM(COALESCE(op.directpayment_amount_with_fee_tip, 0)) directpayment_amount_with_fee_tip,
                           SUM(COALESCE(op.loadedvalue_amount_with_fee_tip, 0)) loadedvalue_amount_with_fee_tip,
                           SUM(COALESCE(op.total_givex_sales, 0)) givex_amount,
                           SUM(COALESCE(op.total_stored_value_tips, 0)) total_stored_value_tips,
                           SUM(COALESCE(op.total_stored_value_fees, 0)) total_stored_value_fees,
                           SUM(COALESCE(op.total_stored_value_taxes, 0)) total_stored_value_taxes,
                           SUM(COALESCE(op.total_credit_sales, 0)) credit_total,
                           SUM(COALESCE(op.total_credit_sales_with_fee_tip, 0)) credit_total_with_fee_tip,
                           SUM(COALESCE(op.total_cash_sales, 0)) cash_total,
                           SUM(COALESCE(op.total_cash_tips, 0)) total_cash_tips,
                           SUM(COALESCE(op.total_cash_sales_with_fee_tip, 0)) cash_total_with_fee_tip,
                           SUM(COALESCE(o.original_subtotal - o.bill_amount, 0)) discount,
                           SUM(COALESCE(op.total_house_sales_with_fee_tip, 0)) AS house_total_with_fee_tip,
                           SUM(COALESCE(op.total_house_sales, 0)) AS house_account_amount,
                           SUM(COALESCE(op.total_house_tips, 0)) AS total_house_tips,
                           SUM(COALESCE(op.total_house_fees, 0)) AS total_house_fees,
                           SUM(COALESCE(op.total_house_taxes, 0)) AS total_house_taxes,
                           SUM(COALESCE(op.total_submit_with_no_payment_sales, 0)) AS submit_with_no_payment_amount,
                           SUM(COALESCE(o.original_subtotal, 0)) amount,
                           SUM(COALESCE(op.total_amex_sales_with_fee_tip, 0)) total_amex_sales_with_fee_tip,
                           SUM(COALESCE(op.amex_cp_amount_with_fee_tip, 0)) amex_cp_amount_with_fee_tip,
                           SUM(COALESCE(op.amex_cnp_amount_with_fee_tip, 0)) amex_cnp_amount_with_fee_tip,
                           SUM(COALESCE(op.total_discover_sales_with_fee_tip, 0)) total_discover_sales_with_fee_tip,
                           SUM(COALESCE(op.discover_cp_amount_with_fee_tip, 0)) discover_cp_amount_with_fee_tip,
                           SUM(COALESCE(op.discover_cnp_amount_with_fee_tip, 0)) discover_cnp_amount_with_fee_tip,
                           SUM(COALESCE(op.total_mastercard_sales_with_fee_tip, 0)) total_mastercard_sales_with_fee_tip,
                           SUM(COALESCE(op.mastercard_cp_amount_with_fee_tip, 0)) mastercard_cp_amount_with_fee_tip,
                           SUM(COALESCE(op.mastercard_cnp_amount_with_fee_tip, 0)) mastercard_cnp_amount_with_fee_tip,
                           SUM(COALESCE(op.total_visa_sales_with_fee_tip, 0)) total_visa_sales_with_fee_tip,
                           SUM(COALESCE(op.visa_cp_amount_with_fee_tip, 0)) visa_cp_amount_with_fee_tip,
                           SUM(COALESCE(op.visa_cnp_amount_with_fee_tip, 0)) visa_cnp_amount_with_fee_tip,
                           SUM(COALESCE(op.visa_tran_count, 0)) visa_tran_count,
                           SUM(COALESCE(op.visa_cp_tran_count, 0)) visa_cp_tran_count,
                           SUM(COALESCE(op.visa_cnp_tran_count, 0)) visa_cnp_tran_count,
                           SUM(COALESCE(op.cash_tran_count, 0)) cash_tran_count,
                           SUM(COALESCE(op.mastercard_tran_count, 0)) mastercard_tran_count,
                           SUM(COALESCE(op.mastercard_cp_tran_count, 0)) mastercard_cp_tran_count,
                           SUM(COALESCE(op.mastercard_cnp_tran_count, 0)) mastercard_cnp_tran_count,
                           SUM(COALESCE(op.amex_tran_count, 0)) amex_tran_count,
                           SUM(COALESCE(op.amex_cp_tran_count, 0)) amex_cp_tran_count,
                           SUM(COALESCE(op.amex_cnp_tran_count, 0)) amex_cnp_tran_count,
                           SUM(COALESCE(op.discover_tran_count, 0)) discover_tran_count,
                           SUM(COALESCE(op.discover_cp_tran_count, 0)) discover_cp_tran_count,
                           SUM(COALESCE(op.discover_cnp_tran_count, 0)) discover_cnp_tran_count,
                           SUM(COALESCE(op.beacon_count, 0)) beacon_count,
                           SUM(COALESCE(op.apple_pay_count, 0)) apple_pay_count,
                           SUM(COALESCE(op.wristband_count, 0)) wristband_count,
                           SUM(COALESCE(op.paypal_count, 0)) paypal_count,
                           SUM(COALESCE(op.loadedvalue_count, 0)) loadedvalue_count,
                           SUM(COALESCE(op.directpayment_count, 0)) directpayment_count
                           " . (count($table_cols_name) > 0 ? ", " . implode(", ", $table_cols_name) : "") . ", 
                           'vu.id' AS groupColumn, vu.id AS groupColumnValue
                           
                           FROM orders o
	                    JOIN vendors v ON v.id = o.vendor_id AND v.venue_id = {$id}
	                    JOIN venues vu ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active=1) OR vu.total_by_active_layout = 0)
	                    join users u on u.id = o.user_id AND u.type in (\"" . implode('", "', $sql_user_type) . "\")
	                LEFT JOIN OrderCustomFees ocf ON o.id = ocf.orderId
                    LEFT JOIN sales_by_payment_type op on o.id = op.order_id
                    LEFT JOIN tax_order_temp tot ON o.id = tot.order_id" . "WHERE o.order_status IN ('COMPLETED', 'PARTIAL_REFUND')
            AND o.refund_status NOT IN ('REFUNDED')
            AND o.{$order_date_field} >= '{$start_date}'
            AND o.{$order_date_field} < '{$end_date}'
            GROUP BY vu.id ");

        $resultCustomTenders = DB::select("SELECT
                               ct.id,
                               ct.name,
                               'vu.id' AS groupColumn,
                               vu.id AS groupColumnValue,
                               SUM(oct.cashAmount) AS cashAmount,
                               SUM(oct.customTenderAmount) AS customTenderAmount
                             FROM orders o
	                    JOIN vendors v ON v.id = o.vendor_id AND v.venue_id = {$id}
	                    JOIN venues vu ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active=1) OR vu.total_by_active_layout = 0)
	                    join users u on u.id = o.user_id AND u.type in (\"" . implode('", "', $sql_user_type) . "\")
                    LEFT JOIN sales_by_payment_type op on o.id = op.order_id
                    LEFT JOIN tax_order_temp tot ON o.id = tot.order_id
                               JOIN OrderCustomTenders oct ON (oct.orderId = o.id)
                               JOIN CustomTender ct ON (ct.id = oct.customTenderId)
                               WHERE o.order_status IN ('COMPLETED', 'PARTIAL_REFUND')
            AND o.refund_status NOT IN ('REFUNDED')
            AND o.{$order_date_field} >= '{$start_date}'
            AND o.{$order_date_field} < '{$end_date}' 
            
                               GROUP BY vu.id, ct.id
                             ORDER BY ct.name ASC");

        $resultsIndexed = [];


        foreach ($result as &$r)
        {
            $r['customTenders']                     = [];
            $resultsIndexed[$r['groupColumnValue']] = $r;
        }

        foreach ($resultCustomTenders as &$ct)
        {
            $ct     = get_object_vars($ct);
            $ct['cashAmount']                                                  = (float)$ct['cashAmount'] ;
            $resultsIndexed[$ct['groupColumnValue']]->customTenders[$ct['id']] = $ct;
        }

        return array_values($resultsIndexed);
    }


    private function getCpTenderBreakdownReportVenueByUserId($id = NULL, $start_date, $end_date, $extra_user)
    {
        if ($id === '')
        {
            return [];
        }

//        $id               = $this->db_inventory->escape($id);
//        $start_date       = $this->db_inventory->escape($start_date);
//        $end_date         = $this->db_inventory->escape($end_date);
        $order_date_field = $this->order_date_field;

        $table_cols = $this->multiple_tax_helper_venue($id, $start_date, $end_date);
        $table_cols_name = array_keys($table_cols);

        DB::select('DROP TEMPORARY TABLE IF EXISTS payment_log_by_order');
        DB::select("CREATE TEMPORARY TABLE IF NOT EXISTS payment_log_by_order 
                          (
                            PRIMARY KEY payment_log_by_order_PK (order_id)
                          ) 
                          AS
                          (
                            SELECT
                              o.id         AS order_id,
                              pl.card_type AS card_type,
                              pl.transaction_id,
                              pl.approved
                          
                            FROM orders o
                                   JOIN vendors v ON v.id = o.vendor_id AND v.venue_id = {$id}
                                   JOIN venues vu ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active=1) OR vu.total_by_active_layout = 0)
                                   JOIN payment_log pl  ON o.id = pl.order_id AND pl.transaction_type = 1
                              LEFT JOIN payment_log pl2 ON pl.order_id = pl2.order_id AND pl2.transaction_type = 1 AND (pl.approved < pl2.approved OR (pl.approved = pl2.approved AND pl.transaction_id < pl2.transaction_id))
                            
                            WHERE 
                                  o.{$order_date_field} >= '{$start_date}'
                              AND o.{$order_date_field} < '{$end_date}'
                              AND pl2.id IS NULL
                            
                            GROUP BY o.id, card_type, pl.transaction_id, pl.approved
                          )");

        DB::select('DROP TEMPORARY TABLE IF EXISTS tmp_orders_refunds');
        DB::select("CREATE TEMPORARY TABLE IF NOT EXISTS tmp_orders_refunds 
                        (
  	                      PRIMARY KEY tmp_orders_refunds_PK (order_id, payment_index)
                        ) 
                        AS 
                        (
                          SELECT
                           o.id              AS order_id,
                           op.payment_index  AS payment_index,
                           SUM(rop.amount)   AS refunds_amount,
                           SUM(rop.subtotal) AS refunds_subtotal,
                           SUM(rop.tip)      AS refunds_tip,
                           SUM(rop.fee)      AS refunds_fee,
                           SUM(rop.tax)      AS refunds_tax
                          FROM order_payments op
                                  JOIN orders o ON op.order_id = o.id
                             LEFT JOIN refund_order_payments rop ON rop.order_id = o.id AND rop.refund_status = 'COMPLETED' AND rop.payment_index = op.payment_index
                                  JOIN vendors v                  ON v.id = o.vendor_id AND v.venue_id = {$id}
                                  JOIN venues vu                  ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active = 1) OR vu.total_by_active_layout = 0)
                             LEFT JOIN payment_log_by_order pl    ON o.id = pl.order_id AND op.payment_type IN (1,6,7,8,13,14)
        			   
        			      WHERE 
                                o.{$order_date_field} >= '{$start_date}'
                            AND o.{$order_date_field} < '{$end_date}'
                            AND o.order_status IN ('COMPLETED', 'PARTIAL_REFUND') 
                            AND o.refund_status NOT IN ('REFUNDED')
                            AND op.payment_status IN ('COMPLETED', 'PARTIAL_REFUND')
                        
 					      GROUP BY o.id , op.payment_index
                        )");

        DB::select('DROP TEMPORARY TABLE IF EXISTS sales_by_payment_type');
        DB::select("CREATE TEMPORARY TABLE IF NOT EXISTS sales_by_payment_type
                (
                  PRIMARY KEY sales_by_payment_type_PK (order_id)
                )
                AS
                (
                  SELECT
                    o.id AS order_id,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_subtotal,0), 0)) AS total_cash_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS total_cash_sales_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_cash_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_cash_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_cash_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, 1, 0)) AS cash_tran_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (1,6,7,8,13,14), COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_subtotal,0), 0)) AS total_credit_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (1,6,7,8,13,14), COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS total_credit_sales_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (1,6,7,8,13,14), COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_credit_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (1,6,7,8,13,14), COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_credit_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (1,6,7,8,13,14), COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_credit_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (3,11,16), COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS total_givex_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS total_loadedvalue_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (11,16), COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_stored_value_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (11,16), COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_stored_value_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (11,16), COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_stored_value_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_directpayment_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_directpayment_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_loadedvalue_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_loadedvalue_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 5, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS total_house_sales_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 5, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS total_house_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 5, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_house_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 5, COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_house_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 5, COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_house_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 9, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS total_submit_with_no_payment_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 1 AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS cp_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 1 AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0), 0)) AS cnp_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 1 AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0), 0)) AS cnp_amount_minus_refund,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 6, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS beacon_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 6, 1, 0)) AS beacon_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 7, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS apple_pay_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 7, 1, 0)) AS apple_pay_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 8, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS wristband_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 8, 1, 0)) AS wristband_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 13, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS paypal_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 13, 1, 0)) AS paypal_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS directpayment_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, 1, 0)) AS directpayment_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS loadedvalue_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, 1, 0)) AS loadedvalue_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 9, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS no_payment_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 9, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS no_payment_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 1 AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS cp_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 1 AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS cnp_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 6, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS beacon_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 7, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS apple_pay_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 8, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS wristband_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 13, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS paypal_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0))  AS directpayment_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax)- COALESCE(tor.refunds_amount,0), 0))  AS loadedvalue_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 8, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_wristband_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 13, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_paypal_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_directpayment_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_loadedvalue_tips,
                
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express'), COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS total_amex_sales_with_fee_tip,
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express') AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS amex_cp_amount_with_fee_tip,
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express') AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS amex_cnp_amount_with_fee_tip,
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express'), 1, 0)) AS amex_tran_count,
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express') AND op.is_card_present = 1, 1, 0)) AS amex_cp_tran_count,
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express') AND COALESCE(op.is_card_present,0) <> 1, 1, 0)) AS amex_cnp_tran_count,
                    SUM(IF(pl.card_type = 'Discover', COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS total_discover_sales_with_fee_tip,
                    SUM(IF(pl.card_type = 'Discover' AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS discover_cp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Discover' AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS discover_cnp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Discover', 1, 0)) AS discover_tran_count,
                    SUM(IF(pl.card_type = 'Discover' AND op.is_card_present = 1, 1, 0)) AS discover_cp_tran_count,
                    SUM(IF(pl.card_type = 'Discover' AND COALESCE(op.is_card_present,0) <> 1, 1, 0)) AS discover_cnp_tran_count,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master'), COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS total_mastercard_sales_with_fee_tip,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master') AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS mastercard_cp_amount_with_fee_tip,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master') AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS mastercard_cnp_amount_with_fee_tip,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master'), 1, 0)) AS mastercard_tran_count,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master') AND op.is_card_present = 1, 1, 0)) AS mastercard_cp_tran_count,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master') AND COALESCE(op.is_card_present,0) <> 1, 1, 0)) AS mastercard_cnp_tran_count,
                    SUM(IF(pl.card_type = 'Visa', COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS total_visa_sales_with_fee_tip,
                    SUM(IF(pl.card_type = 'Visa' AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS visa_cp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Visa' AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS visa_cnp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Visa', 1, 0)) AS visa_tran_count,
                    SUM(IF(pl.card_type = 'Visa' AND op.is_card_present = 1, 1, 0)) AS visa_cp_tran_count,
                    SUM(IF(pl.card_type = 'Visa' AND COALESCE(op.is_card_present,0) <> 1, 1, 0)) AS visa_cnp_tran_count,
                    SUM(IF(pl.card_type = 'Other', 1, 0)) AS other_tran_count,
                    SUM(IF(pl.card_type = 'Other' AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS other_cp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Other' AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS other_cnp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Other', COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS total_other_sales_with_fee_tip
                
                  FROM order_payments op
                         JOIN orders o                ON op.order_id = o.id
                    LEFT JOIN tmp_orders_refunds tor  ON (tor.order_id = o.id AND tor.payment_index = op.payment_index)
                         JOIN vendors v                  ON v.id = o.vendor_id AND v.venue_id = {$id}
                         JOIN venues vu                  ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active = 1) OR vu.total_by_active_layout = 0)
                    LEFT JOIN payment_log_by_order pl    ON o.id = pl.order_id AND op.payment_type IN (1,6,7,8,13,14)
        			   
        		  WHERE 
                        o.{$order_date_field} >= '{$start_date}'
                    AND o.{$order_date_field} < '{$end_date}'
                    AND o.order_status IN ('COMPLETED', 'PARTIAL_REFUND') 
                    AND o.refund_status NOT IN ('REFUNDED')
                    AND op.payment_status IN ('COMPLETED', 'PARTIAL_REFUND')
                        
 				  GROUP BY o.id 
 				)  
               ");

        $sql_user_type[] = 'EMPLOYEE';

        if ($extra_user[0] === TRUE)
        {
            $sql_user_type[] = 'GUEST';
        }

        if ($extra_user[1] === TRUE)
        {
            $sql_user_type[] = 'USER';
        }

        $result = DB::select("SELECT u.name name, u.id u_id,  
        SUM(COALESCE(o.tip, 0)) tip,
                           SUM(COALESCE(o.fee, 0)) fee,
                           ROUND(SUM(COALESCE(ocf.`customFeeValue`, 0)), 2) AS custom_fees,
                           SUM(COALESCE(o.tax, 0)) tax,
                           SUM(COALESCE(op.total_credit_tips, 0)) total_credit_tips,
                           SUM(COALESCE(op.total_credit_fees, 0)) total_credit_fees,
                           SUM(COALESCE(op.total_credit_taxes, 0)) total_credit_taxes,
                           SUM(COALESCE(op.total_cash_fees, 0)) total_cash_fees,
                           SUM(COALESCE(op.total_cash_taxes, 0)) total_cash_taxes,
                           SUM(COALESCE(op.total_wristband_tips, 0)) total_wristband_tips,
                           SUM(COALESCE(op.total_paypal_tips, 0)) total_paypal_tips,
                           SUM(COALESCE(op.total_directpayment_tips, 0)) total_directpayment_tips,
                           SUM(COALESCE(op.total_loadedvalue_tips, 0)) total_loadedvalue_tips,
                           SUM(COALESCE(op.cp_amount, 0)) cp_amount,
                           SUM(COALESCE(op.cnp_amount, 0)) cnp_amount,
                           SUM(COALESCE(op.beacon_amount, 0)) beacon_amount,
                           SUM(COALESCE(op.apple_pay_amount, 0)) apple_pay_amount,
                           SUM(COALESCE(op.wristband_amount, 0)) wristband_amount,
                           SUM(COALESCE(op.paypal_amount, 0)) paypal_amount,
                           SUM(COALESCE(op.directpayment_amount, 0)) directpayment_amount,
                           SUM(COALESCE(op.loadedvalue_amount, 0)) loadedvalue_amount,
                           SUM(COALESCE(op.no_payment_amount, 0)) no_payment_amount,
                           SUM(COALESCE(op.no_payment_with_fee_tip, 0)) no_payment_with_fee_tip,
                           SUM(COALESCE(op.cp_amount_with_fee_tip, 0)) cp_amount_with_fee_tip,
                           SUM(COALESCE(op.cnp_amount_with_fee_tip, 0)) cnp_amount_with_fee_tip,
                           SUM(COALESCE(op.beacon_amount_with_fee_tip, 0)) beacon_amount_with_fee_tip,
                           SUM(COALESCE(op.apple_pay_amount_with_fee_tip, 0)) apple_pay_amount_with_fee_tip,
                           SUM(COALESCE(op.wristband_amount_with_fee_tip, 0)) wristband_amount_with_fee_tip,
                           SUM(COALESCE(op.paypal_amount_with_fee_tip, 0)) paypal_amount_with_fee_tip,
                           SUM(COALESCE(op.directpayment_amount_with_fee_tip, 0)) directpayment_amount_with_fee_tip,
                           SUM(COALESCE(op.loadedvalue_amount_with_fee_tip, 0)) loadedvalue_amount_with_fee_tip,
                           SUM(COALESCE(op.total_givex_sales, 0)) givex_amount,
                           SUM(COALESCE(op.total_stored_value_tips, 0)) total_stored_value_tips,
                           SUM(COALESCE(op.total_stored_value_fees, 0)) total_stored_value_fees,
                           SUM(COALESCE(op.total_stored_value_taxes, 0)) total_stored_value_taxes,
                           SUM(COALESCE(op.total_credit_sales, 0)) credit_total,
                           SUM(COALESCE(op.total_credit_sales_with_fee_tip, 0)) credit_total_with_fee_tip,
                           SUM(COALESCE(op.total_cash_sales, 0)) cash_total,
                           SUM(COALESCE(op.total_cash_tips, 0)) total_cash_tips,
                           SUM(COALESCE(op.total_cash_sales_with_fee_tip, 0)) cash_total_with_fee_tip,
                           SUM(COALESCE(o.original_subtotal - o.bill_amount, 0)) discount,
                           SUM(COALESCE(op.total_house_sales_with_fee_tip, 0)) AS house_total_with_fee_tip,
                           SUM(COALESCE(op.total_house_sales, 0)) AS house_account_amount,
                           SUM(COALESCE(op.total_house_tips, 0)) AS total_house_tips,
                           SUM(COALESCE(op.total_house_fees, 0)) AS total_house_fees,
                           SUM(COALESCE(op.total_house_taxes, 0)) AS total_house_taxes,
                           SUM(COALESCE(op.total_submit_with_no_payment_sales, 0)) AS submit_with_no_payment_amount,
                           SUM(COALESCE(o.original_subtotal, 0)) amount,
                           SUM(COALESCE(op.total_amex_sales_with_fee_tip, 0)) total_amex_sales_with_fee_tip,
                           SUM(COALESCE(op.amex_cp_amount_with_fee_tip, 0)) amex_cp_amount_with_fee_tip,
                           SUM(COALESCE(op.amex_cnp_amount_with_fee_tip, 0)) amex_cnp_amount_with_fee_tip,
                           SUM(COALESCE(op.total_discover_sales_with_fee_tip, 0)) total_discover_sales_with_fee_tip,
                           SUM(COALESCE(op.discover_cp_amount_with_fee_tip, 0)) discover_cp_amount_with_fee_tip,
                           SUM(COALESCE(op.discover_cnp_amount_with_fee_tip, 0)) discover_cnp_amount_with_fee_tip,
                           SUM(COALESCE(op.total_mastercard_sales_with_fee_tip, 0)) total_mastercard_sales_with_fee_tip,
                           SUM(COALESCE(op.mastercard_cp_amount_with_fee_tip, 0)) mastercard_cp_amount_with_fee_tip,
                           SUM(COALESCE(op.mastercard_cnp_amount_with_fee_tip, 0)) mastercard_cnp_amount_with_fee_tip,
                           SUM(COALESCE(op.total_visa_sales_with_fee_tip, 0)) total_visa_sales_with_fee_tip,
                           SUM(COALESCE(op.visa_cp_amount_with_fee_tip, 0)) visa_cp_amount_with_fee_tip,
                           SUM(COALESCE(op.visa_cnp_amount_with_fee_tip, 0)) visa_cnp_amount_with_fee_tip,
                           SUM(COALESCE(op.visa_tran_count, 0)) visa_tran_count,
                           SUM(COALESCE(op.visa_cp_tran_count, 0)) visa_cp_tran_count,
                           SUM(COALESCE(op.visa_cnp_tran_count, 0)) visa_cnp_tran_count,
                           SUM(COALESCE(op.cash_tran_count, 0)) cash_tran_count,
                           SUM(COALESCE(op.mastercard_tran_count, 0)) mastercard_tran_count,
                           SUM(COALESCE(op.mastercard_cp_tran_count, 0)) mastercard_cp_tran_count,
                           SUM(COALESCE(op.mastercard_cnp_tran_count, 0)) mastercard_cnp_tran_count,
                           SUM(COALESCE(op.amex_tran_count, 0)) amex_tran_count,
                           SUM(COALESCE(op.amex_cp_tran_count, 0)) amex_cp_tran_count,
                           SUM(COALESCE(op.amex_cnp_tran_count, 0)) amex_cnp_tran_count,
                           SUM(COALESCE(op.discover_tran_count, 0)) discover_tran_count,
                           SUM(COALESCE(op.discover_cp_tran_count, 0)) discover_cp_tran_count,
                           SUM(COALESCE(op.discover_cnp_tran_count, 0)) discover_cnp_tran_count,
                           SUM(COALESCE(op.beacon_count, 0)) beacon_count,
                           SUM(COALESCE(op.apple_pay_count, 0)) apple_pay_count,
                           SUM(COALESCE(op.wristband_count, 0)) wristband_count,
                           SUM(COALESCE(op.paypal_count, 0)) paypal_count,
                           SUM(COALESCE(op.loadedvalue_count, 0)) loadedvalue_count,
                           SUM(COALESCE(op.directpayment_count, 0)) directpayment_count
                           " . (count($table_cols_name) > 0 ? ", " . implode(", ", $table_cols_name) : "") . ", 
                           'u.id' AS groupColumn, u.id AS groupColumnValue 
        FROM  orders o
	                    JOIN vendors v ON v.id = o.vendor_id AND v.venue_id = {$id}
	                    JOIN venues vu ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active=1) OR vu.total_by_active_layout = 0)
	                    join users u on u.id = o.user_id AND u.type in (\"" . implode('", "', $sql_user_type) . "\")
	                LEFT JOIN OrderCustomFees ocf ON o.id = ocf.orderId
                    LEFT JOIN sales_by_payment_type op on o.id = op.order_id
                    LEFT JOIN tax_order_temp tot ON o.id = tot.order_id WHERE o.order_status IN ('COMPLETED', 'PARTIAL_REFUND')
            AND o.refund_status NOT IN ('REFUNDED')
            AND o.{$order_date_field} >= '{$start_date}'
            AND o.{$order_date_field} < '{$end_date}'
            GROUP BY u.id ");

        $resultCustomTenders = DB::select("SELECT
                               ct.id,
                               ct.name,
                               'u.id' AS groupColumn,
                               u.id AS groupColumnValue,
                               SUM(oct.cashAmount) AS cashAmount,
                               SUM(oct.customTenderAmount) AS customTenderAmount
                             FROM  orders o
	                    JOIN vendors v ON v.id = o.vendor_id AND v.venue_id = {$id}
	                    JOIN venues vu ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active=1) OR vu.total_by_active_layout = 0)
	                    join users u on u.id = o.user_id AND u.type in (\"" . implode('", "', $sql_user_type) . "\")
                    LEFT JOIN sales_by_payment_type op on o.id = op.order_id
                    LEFT JOIN tax_order_temp tot ON o.id = tot.order_id
                               JOIN OrderCustomTenders oct ON (oct.orderId = o.id)
                               JOIN CustomTender ct ON (ct.id = oct.customTenderId)
                               
                               WHERE o.order_status IN ('COMPLETED', 'PARTIAL_REFUND')
            AND o.refund_status NOT IN ('REFUNDED')
            AND o.{$order_date_field} >= '{$start_date}'
            AND o.{$order_date_field} < '{$end_date}' 
            
                             GROUP BY u.id, ct.id
                             ORDER BY ct.name ASC");


        $resultsIndexed = [];


        foreach ($result as &$r)
        {
            $r->customTenders                    = [];
            $resultsIndexed[$r->groupColumnValue] = $r;
        }

        foreach ($resultCustomTenders as &$ct)
        {
            $ct     = get_object_vars($ct);
            $ct['cashAmount']                                                  = (float)$ct['cashAmount'] ;
            $resultsIndexed[$ct['groupColumnValue']]->customTenders[$ct['id']] = $ct;
        }

        //dd($resultsIndexed);

        return array_values($resultsIndexed);
    }


    private function getCpTenderBreakdownReportVenueByAccountsByVendor($id = NULL, $start_date, $end_date, $extra_user)
    {
        if ($id === '')
        {
            return [];
        }

//        $id               = $this->db_inventory->escape($id);
//        $start_date       = $this->db_inventory->escape($start_date);
//        $end_date         = $this->db_inventory->escape($end_date);
        $order_date_field = $this->order_date_field;

        $table_cols = $this->multiple_tax_helper_venue($id, $start_date, $end_date);
        $table_cols_name = array_keys($table_cols);

        DB::select('DROP TEMPORARY TABLE IF EXISTS payment_log_by_order');
        DB::select("CREATE TEMPORARY TABLE IF NOT EXISTS payment_log_by_order 
                          (
                            PRIMARY KEY payment_log_by_order_PK (order_id)
                          ) 
                          AS
                          (
                            SELECT
                              o.id         AS order_id,
                              pl.card_type AS card_type,
                              pl.transaction_id,
                              pl.approved
                          
                            FROM orders o
                                   JOIN vendors v ON v.id = o.vendor_id AND v.venue_id = {$id}
                                   JOIN venues vu ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active=1) OR vu.total_by_active_layout = 0)
                                   JOIN payment_log pl  ON o.id = pl.order_id AND pl.transaction_type = 1
                              LEFT JOIN payment_log pl2 ON pl.order_id = pl2.order_id AND pl2.transaction_type = 1 AND (pl.approved < pl2.approved OR (pl.approved = pl2.approved AND pl.transaction_id < pl2.transaction_id))
                            
                            WHERE 
                                  o.{$order_date_field} >= '{$start_date}'
                              AND o.{$order_date_field} < '{$end_date}'
                              AND pl2.id IS NULL
                            
                            GROUP BY o.id, card_type, pl.transaction_id, pl.approved
                          )");

        DB::select('DROP TEMPORARY TABLE IF EXISTS tmp_orders_refunds');
        DB::select("CREATE TEMPORARY TABLE IF NOT EXISTS tmp_orders_refunds 
                        (
  	                      PRIMARY KEY tmp_orders_refunds_PK (order_id, payment_index)
                        ) 
                        AS 
                        (
                          SELECT
                           o.id              AS order_id,
                           op.payment_index  AS payment_index,
                           SUM(rop.amount)   AS refunds_amount,
                           SUM(rop.subtotal) AS refunds_subtotal,
                           SUM(rop.tip)      AS refunds_tip,
                           SUM(rop.fee)      AS refunds_fee,
                           SUM(rop.tax)      AS refunds_tax
                          FROM order_payments op
                                  JOIN orders o ON op.order_id = o.id
                             LEFT JOIN refund_order_payments rop ON rop.order_id = o.id AND rop.refund_status = 'COMPLETED' AND rop.payment_index = op.payment_index
                                  JOIN vendors v                  ON v.id = o.vendor_id AND v.venue_id = {$id}
                                  JOIN venues vu                  ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active = 1) OR vu.total_by_active_layout = 0)
                             LEFT JOIN payment_log_by_order pl    ON o.id = pl.order_id AND op.payment_type IN (1,6,7,8,13,14)
        			   
        			      WHERE 
                                o.{$order_date_field} >= '{$start_date}'
                            AND o.{$order_date_field} < '{$end_date}'
                            AND o.order_status IN ('COMPLETED', 'PARTIAL_REFUND') 
                            AND o.refund_status NOT IN ('REFUNDED')
                            AND op.payment_status IN ('COMPLETED', 'PARTIAL_REFUND')
                        
 					      GROUP BY o.id , op.payment_index
                        )");

        DB::select('DROP TEMPORARY TABLE IF EXISTS sales_by_payment_type');
        DB::select("CREATE TEMPORARY TABLE IF NOT EXISTS sales_by_payment_type
                (
                  PRIMARY KEY sales_by_payment_type_PK (order_id)
                )
                AS
                (
                  SELECT
                    o.id AS order_id,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_subtotal,0), 0)) AS total_cash_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS total_cash_sales_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_cash_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_cash_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_cash_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 2, 1, 0)) AS cash_tran_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (1,6,7,8,13,14), COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_subtotal,0), 0)) AS total_credit_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (1,6,7,8,13,14), COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS total_credit_sales_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (1,6,7,8,13,14), COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_credit_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (1,6,7,8,13,14), COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_credit_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (1,6,7,8,13,14), COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_credit_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (3,11,16), COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS total_givex_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS total_loadedvalue_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (11,16), COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_stored_value_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (11,16), COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_stored_value_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) IN (11,16), COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_stored_value_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_directpayment_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_directpayment_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_loadedvalue_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_loadedvalue_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 5, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS total_house_sales_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 5, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS total_house_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 5, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_house_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 5, COALESCE(op.fee, o.fee) - COALESCE(tor.refunds_fee, 0), 0)) AS total_house_fees,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 5, COALESCE(op.tax, o.tax) - COALESCE(tor.refunds_tax, 0), 0)) AS total_house_taxes,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 9, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS total_submit_with_no_payment_sales,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 1 AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS cp_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 1 AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0), 0)) AS cnp_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 1 AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0), 0)) AS cnp_amount_minus_refund,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 6, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS beacon_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 6, 1, 0)) AS beacon_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 7, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS apple_pay_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 7, 1, 0)) AS apple_pay_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 8, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS wristband_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 8, 1, 0)) AS wristband_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 13, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS paypal_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 13, 1, 0)) AS paypal_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS directpayment_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, 1, 0)) AS directpayment_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS loadedvalue_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, 1, 0)) AS loadedvalue_count,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 9, COALESCE(op.amount, o.bill_amount) - COALESCE(op.tip,0) - COALESCE(op.fee,0) - COALESCE(op.tax,0) - COALESCE(tor.refunds_amount,0) + COALESCE(tor.refunds_tip,0) + COALESCE(tor.refunds_fee,0) + COALESCE(tor.refunds_tax,0), 0)) AS no_payment_amount,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 9, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS no_payment_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 1 AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS cp_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 1 AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS cnp_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 6, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS beacon_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 7, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS apple_pay_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 8, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS wristband_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 13, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0)) AS paypal_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax) - COALESCE(tor.refunds_amount,0), 0))  AS directpayment_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.amount, o.bill_amount + o.fee + o.tip + o.tax)- COALESCE(tor.refunds_amount,0), 0))  AS loadedvalue_amount_with_fee_tip,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 8, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_wristband_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 13, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_paypal_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 14, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_directpayment_tips,
                    SUM(IF(COALESCE(op.orig_payment_type, op.payment_type, o.payment_type) = 15, COALESCE(op.tip, o.tip) - COALESCE(tor.refunds_tip, 0), 0)) AS total_loadedvalue_tips,
                
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express'), COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS total_amex_sales_with_fee_tip,
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express') AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS amex_cp_amount_with_fee_tip,
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express') AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS amex_cnp_amount_with_fee_tip,
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express'), 1, 0)) AS amex_tran_count,
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express') AND op.is_card_present = 1, 1, 0)) AS amex_cp_tran_count,
                    SUM(IF(pl.card_type IN ('American Express', 'AMEX', 'American_express') AND COALESCE(op.is_card_present,0) <> 1, 1, 0)) AS amex_cnp_tran_count,
                    SUM(IF(pl.card_type = 'Discover', COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS total_discover_sales_with_fee_tip,
                    SUM(IF(pl.card_type = 'Discover' AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS discover_cp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Discover' AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS discover_cnp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Discover', 1, 0)) AS discover_tran_count,
                    SUM(IF(pl.card_type = 'Discover' AND op.is_card_present = 1, 1, 0)) AS discover_cp_tran_count,
                    SUM(IF(pl.card_type = 'Discover' AND COALESCE(op.is_card_present,0) <> 1, 1, 0)) AS discover_cnp_tran_count,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master'), COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS total_mastercard_sales_with_fee_tip,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master') AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS mastercard_cp_amount_with_fee_tip,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master') AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS mastercard_cnp_amount_with_fee_tip,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master'), 1, 0)) AS mastercard_tran_count,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master') AND op.is_card_present = 1, 1, 0)) AS mastercard_cp_tran_count,
                    SUM(IF(pl.card_type IN ('MasterCard', 'Master') AND COALESCE(op.is_card_present,0) <> 1, 1, 0)) AS mastercard_cnp_tran_count,
                    SUM(IF(pl.card_type = 'Visa', COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS total_visa_sales_with_fee_tip,
                    SUM(IF(pl.card_type = 'Visa' AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS visa_cp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Visa' AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS visa_cnp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Visa', 1, 0)) AS visa_tran_count,
                    SUM(IF(pl.card_type = 'Visa' AND op.is_card_present = 1, 1, 0)) AS visa_cp_tran_count,
                    SUM(IF(pl.card_type = 'Visa' AND COALESCE(op.is_card_present,0) <> 1, 1, 0)) AS visa_cnp_tran_count,
                    SUM(IF(pl.card_type = 'Other', 1, 0)) AS other_tran_count,
                    SUM(IF(pl.card_type = 'Other' AND op.is_card_present = 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS other_cp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Other' AND COALESCE(op.is_card_present,0) <> 1, COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS other_cnp_amount_with_fee_tip,
                    SUM(IF(pl.card_type = 'Other', COALESCE(op.amount, o.bill_amount) - COALESCE(tor.refunds_amount,0), 0)) AS total_other_sales_with_fee_tip
                
                  FROM order_payments op
                         JOIN orders o                ON op.order_id = o.id
                    LEFT JOIN tmp_orders_refunds tor  ON (tor.order_id = o.id AND tor.payment_index = op.payment_index)
                         JOIN vendors v                  ON v.id = o.vendor_id AND v.venue_id = {$id}
                         JOIN venues vu                  ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active = 1) OR vu.total_by_active_layout = 0)
                    LEFT JOIN payment_log_by_order pl    ON o.id = pl.order_id AND op.payment_type IN (1,6,7,8,13,14)
        			   
        		  WHERE 
                        o.{$order_date_field} >= '{$start_date}'
                    AND o.{$order_date_field} < '{$end_date}'
                    AND o.order_status IN ('COMPLETED', 'PARTIAL_REFUND') 
                    AND o.refund_status NOT IN ('REFUNDED')
                    AND op.payment_status IN ('COMPLETED', 'PARTIAL_REFUND')
                        
 				  GROUP BY o.id 
 				)  
               ");

        $sql_user_type[] = 'EMPLOYEE';

        if ($extra_user[0] === TRUE)
        {
            $sql_user_type[] = 'GUEST';
        }

        if ($extra_user[1] === TRUE)
        {
            $sql_user_type[] = 'USER';
        }

        $result = DB::select("SELECT v.name vendor_name, v.id v_id, u.name name, u.id u_id,  SUM(COALESCE(o.tip, 0)) tip,
                           SUM(COALESCE(o.fee, 0)) fee,
                           ROUND(SUM(COALESCE(ocf.`customFeeValue`, 0)), 2) AS custom_fees,
                           SUM(COALESCE(o.tax, 0)) tax,
                           SUM(COALESCE(op.total_credit_tips, 0)) total_credit_tips,
                           SUM(COALESCE(op.total_credit_fees, 0)) total_credit_fees,
                           SUM(COALESCE(op.total_credit_taxes, 0)) total_credit_taxes,
                           SUM(COALESCE(op.total_cash_fees, 0)) total_cash_fees,
                           SUM(COALESCE(op.total_cash_taxes, 0)) total_cash_taxes,
                           SUM(COALESCE(op.total_wristband_tips, 0)) total_wristband_tips,
                           SUM(COALESCE(op.total_paypal_tips, 0)) total_paypal_tips,
                           SUM(COALESCE(op.total_directpayment_tips, 0)) total_directpayment_tips,
                           SUM(COALESCE(op.total_loadedvalue_tips, 0)) total_loadedvalue_tips,
                           SUM(COALESCE(op.cp_amount, 0)) cp_amount,
                           SUM(COALESCE(op.cnp_amount, 0)) cnp_amount,
                           SUM(COALESCE(op.beacon_amount, 0)) beacon_amount,
                           SUM(COALESCE(op.apple_pay_amount, 0)) apple_pay_amount,
                           SUM(COALESCE(op.wristband_amount, 0)) wristband_amount,
                           SUM(COALESCE(op.paypal_amount, 0)) paypal_amount,
                           SUM(COALESCE(op.directpayment_amount, 0)) directpayment_amount,
                           SUM(COALESCE(op.loadedvalue_amount, 0)) loadedvalue_amount,
                           SUM(COALESCE(op.no_payment_amount, 0)) no_payment_amount,
                           SUM(COALESCE(op.no_payment_with_fee_tip, 0)) no_payment_with_fee_tip,
                           SUM(COALESCE(op.cp_amount_with_fee_tip, 0)) cp_amount_with_fee_tip,
                           SUM(COALESCE(op.cnp_amount_with_fee_tip, 0)) cnp_amount_with_fee_tip,
                           SUM(COALESCE(op.beacon_amount_with_fee_tip, 0)) beacon_amount_with_fee_tip,
                           SUM(COALESCE(op.apple_pay_amount_with_fee_tip, 0)) apple_pay_amount_with_fee_tip,
                           SUM(COALESCE(op.wristband_amount_with_fee_tip, 0)) wristband_amount_with_fee_tip,
                           SUM(COALESCE(op.paypal_amount_with_fee_tip, 0)) paypal_amount_with_fee_tip,
                           SUM(COALESCE(op.directpayment_amount_with_fee_tip, 0)) directpayment_amount_with_fee_tip,
                           SUM(COALESCE(op.loadedvalue_amount_with_fee_tip, 0)) loadedvalue_amount_with_fee_tip,
                           SUM(COALESCE(op.total_givex_sales, 0)) givex_amount,
                           SUM(COALESCE(op.total_stored_value_tips, 0)) total_stored_value_tips,
                           SUM(COALESCE(op.total_stored_value_fees, 0)) total_stored_value_fees,
                           SUM(COALESCE(op.total_stored_value_taxes, 0)) total_stored_value_taxes,
                           SUM(COALESCE(op.total_credit_sales, 0)) credit_total,
                           SUM(COALESCE(op.total_credit_sales_with_fee_tip, 0)) credit_total_with_fee_tip,
                           SUM(COALESCE(op.total_cash_sales, 0)) cash_total,
                           SUM(COALESCE(op.total_cash_tips, 0)) total_cash_tips,
                           SUM(COALESCE(op.total_cash_sales_with_fee_tip, 0)) cash_total_with_fee_tip,
                           SUM(COALESCE(o.original_subtotal - o.bill_amount, 0)) discount,
                           SUM(COALESCE(op.total_house_sales_with_fee_tip, 0)) AS house_total_with_fee_tip,
                           SUM(COALESCE(op.total_house_sales, 0)) AS house_account_amount,
                           SUM(COALESCE(op.total_house_tips, 0)) AS total_house_tips,
                           SUM(COALESCE(op.total_house_fees, 0)) AS total_house_fees,
                           SUM(COALESCE(op.total_house_taxes, 0)) AS total_house_taxes,
                           SUM(COALESCE(op.total_submit_with_no_payment_sales, 0)) AS submit_with_no_payment_amount,
                           SUM(COALESCE(o.original_subtotal, 0)) amount,
                           SUM(COALESCE(op.total_amex_sales_with_fee_tip, 0)) total_amex_sales_with_fee_tip,
                           SUM(COALESCE(op.amex_cp_amount_with_fee_tip, 0)) amex_cp_amount_with_fee_tip,
                           SUM(COALESCE(op.amex_cnp_amount_with_fee_tip, 0)) amex_cnp_amount_with_fee_tip,
                           SUM(COALESCE(op.total_discover_sales_with_fee_tip, 0)) total_discover_sales_with_fee_tip,
                           SUM(COALESCE(op.discover_cp_amount_with_fee_tip, 0)) discover_cp_amount_with_fee_tip,
                           SUM(COALESCE(op.discover_cnp_amount_with_fee_tip, 0)) discover_cnp_amount_with_fee_tip,
                           SUM(COALESCE(op.total_mastercard_sales_with_fee_tip, 0)) total_mastercard_sales_with_fee_tip,
                           SUM(COALESCE(op.mastercard_cp_amount_with_fee_tip, 0)) mastercard_cp_amount_with_fee_tip,
                           SUM(COALESCE(op.mastercard_cnp_amount_with_fee_tip, 0)) mastercard_cnp_amount_with_fee_tip,
                           SUM(COALESCE(op.total_visa_sales_with_fee_tip, 0)) total_visa_sales_with_fee_tip,
                           SUM(COALESCE(op.visa_cp_amount_with_fee_tip, 0)) visa_cp_amount_with_fee_tip,
                           SUM(COALESCE(op.visa_cnp_amount_with_fee_tip, 0)) visa_cnp_amount_with_fee_tip,
                           SUM(COALESCE(op.visa_tran_count, 0)) visa_tran_count,
                           SUM(COALESCE(op.visa_cp_tran_count, 0)) visa_cp_tran_count,
                           SUM(COALESCE(op.visa_cnp_tran_count, 0)) visa_cnp_tran_count,
                           SUM(COALESCE(op.cash_tran_count, 0)) cash_tran_count,
                           SUM(COALESCE(op.mastercard_tran_count, 0)) mastercard_tran_count,
                           SUM(COALESCE(op.mastercard_cp_tran_count, 0)) mastercard_cp_tran_count,
                           SUM(COALESCE(op.mastercard_cnp_tran_count, 0)) mastercard_cnp_tran_count,
                           SUM(COALESCE(op.amex_tran_count, 0)) amex_tran_count,
                           SUM(COALESCE(op.amex_cp_tran_count, 0)) amex_cp_tran_count,
                           SUM(COALESCE(op.amex_cnp_tran_count, 0)) amex_cnp_tran_count,
                           SUM(COALESCE(op.discover_tran_count, 0)) discover_tran_count,
                           SUM(COALESCE(op.discover_cp_tran_count, 0)) discover_cp_tran_count,
                           SUM(COALESCE(op.discover_cnp_tran_count, 0)) discover_cnp_tran_count,
                           SUM(COALESCE(op.beacon_count, 0)) beacon_count,
                           SUM(COALESCE(op.apple_pay_count, 0)) apple_pay_count,
                           SUM(COALESCE(op.wristband_count, 0)) wristband_count,
                           SUM(COALESCE(op.paypal_count, 0)) paypal_count,
                           SUM(COALESCE(op.loadedvalue_count, 0)) loadedvalue_count,
                           SUM(COALESCE(op.directpayment_count, 0)) directpayment_count
                           " . (count($table_cols_name) > 0 ? ", " . implode(", ", $table_cols_name) : "") . ",
                           'v.id, u.id' AS groupColumn, CONCAT(v.id, '-', u.id) AS groupColumnValue
                           
                           FROM  orders o
	                    JOIN vendors v ON v.id = o.vendor_id AND v.venue_id = {$id}
	                    JOIN venues vu ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active=1) OR vu.total_by_active_layout = 0)
	                    join users u on u.id = o.user_id AND u.type in (\"" . implode('", "', $sql_user_type) . "\")
	                LEFT JOIN OrderCustomFees ocf ON o.id = ocf.orderId
                    LEFT JOIN sales_by_payment_type op on o.id = op.order_id
                    LEFT JOIN tax_order_temp tot ON o.id = tot.order_id
	                    WHERE o.order_status IN ('COMPLETED', 'PARTIAL_REFUND')
            AND o.refund_status NOT IN ('REFUNDED')
            AND o.{$order_date_field} >= '{$start_date}'
            AND o.{$order_date_field} < '{$end_date}' 
            GROUP BY v.id, u.id");

        $resultCustomTenders = DB::select("SELECT
                               ct.id,
                               ct.name,
                               'v.id, u.id' AS groupColumn,
                               CONCAT(v.id, '-', u.id) AS groupColumnValue,
                               SUM(oct.cashAmount) AS cashAmount,
                               SUM(oct.customTenderAmount) AS customTenderAmount
                             FROM orders o
	                    JOIN vendors v ON v.id = o.vendor_id AND v.venue_id = {$id}
	                    JOIN venues vu ON v.venue_id = vu.id AND ((vu.total_by_active_layout = 1 AND v.is_active=1) OR vu.total_by_active_layout = 0)
	                    join users u on u.id = o.user_id AND u.type in (\"" . implode('", "', $sql_user_type) . "\")
                    LEFT JOIN sales_by_payment_type op on o.id = op.order_id
                    LEFT JOIN tax_order_temp tot ON o.id = tot.order_id
                               JOIN OrderCustomTenders oct ON (oct.orderId = o.id)
                               JOIN CustomTender ct ON (ct.id = oct.customTenderId)
                           WHERE o.order_status IN ('COMPLETED', 'PARTIAL_REFUND')
            AND o.refund_status NOT IN ('REFUNDED')
            AND o.{$order_date_field} >= '{$start_date}'
            AND o.{$order_date_field} < '{$end_date}' 
                               GROUP BY v.id, u.id, ct.id
                             ORDER BY ct.name ASC");;

        $resultsIndexed = [];


        foreach ($result as &$r)
        {
            $r->customTenders                    = [];
            $resultsIndexed[$r->groupColumnValue] = $r;
        }

        foreach ($resultCustomTenders as &$ct)
        {
            $ct     = get_object_vars($ct);
            $ct['cashAmount']                                                  = (float)$ct['cashAmount'] ;
            $resultsIndexed[$ct['groupColumnValue']]->customTenders[$ct['id']] = $ct;
        }

        //dd($resultsIndexed);

        return array_values($resultsIndexed);
    }

    /**
     * This function returns custom refund amount
     *
     * @param $venueId defines the venue id
     * @param $startDate defines the start date
     * @param $endDate defines the end date
     *
     * @return mixed custom refund amount
     */
    private function getCustomRefundAmount($venueId, $startDate, $endDate)
    {
        $output = DB::select
        (
            "SELECT 
                        SUM(o.original_subtotal) as original_subtotal
                        
                    FROM orders o 
                      JOIN vendors v ON o.vendor_id = v.id AND v.venue_id = :venueId
                      JOIN venues vu ON v.venue_id = vu.id AND customCreditEnabled = 1
                    
                    WHERE
                          o.datetime >= :startDate  
                      AND o.datetime < :endDate
                      AND o.order_status IN ('REFUNDED')
                      AND o.refund_status IN ('REFUNDED')
                    
                    GROUP BY o.id and o.vendor_id  
                   ",
            [
                'venueId' => $venueId,
                'startDate' => $startDate,
                'endDate' => $endDate,
            ]
        );

        $output     = get_object_vars($output[0]);


        return empty($output)? 0 : $output['original_subtotal'];
    }


    /**
     * Set event selected
     *
     * @param $eventId
     */
    private function setEventSelected(int $eventId, int $venueId, int $eventTimeEndShift)
    {
        $currentEvent = $this->getEventDateByVenue($venueId, $eventId, self::VIEW_DATE_FORMAT, $eventTimeEndShift);

        if (isset($currentEvent))
        {
            $currentEvent['datetime']             = $this->convertTimezone($currentEvent['datetime']            , $this->getMysqlTimezone(), $this->getVenueTimezone($venueId));
            $currentEvent['datetime_end']         = $this->convertTimezone($currentEvent['datetime_end']        , $this->getMysqlTimezone(), $this->getVenueTimezone($venueId));
            $currentEvent['calendar_event_start'] = $this->convertTimezone($currentEvent['calendar_event_start'], $this->getMysqlTimezone(), $this->getVenueTimezone($venueId));
            $currentEvent['calendar_event_end']   = $this->convertTimezone($currentEvent['calendar_event_end']  , $this->getMysqlTimezone(), $this->getVenueTimezone($venueId));

            return $currentEvent;
        }

        return FALSE;
    }



    private function getEventDateByVenue(int $venueId, int $eventID, string $dateFormat, int $eventTimeEndShift)
    {
        $event  =   DB::select
                    (
                        "SELECT 
                                  se.id, 
                                  se.name, 
                                  COALESCE(ir.date_opened, se.datetime) as datetime, 
                                  COALESCE(ir.date_closed, se.datetime_end, NOW()) as datetime_end, 
                                  se.datetime as calendar_event_start, 
                                  se.datetime_end as calendar_event_end, 
                                  se.id as event_id, 
                                  se.name as event_name, 
                                  se.datetime as date
                                
                                FROM (`Event` se)
                                  LEFT JOIN `event_data` ed         ON `se`.`id` = `ed`.`event_id` AND se.venue_id = :venueID1
                                  LEFT JOIN `inventory_reports` ir  ON `ir`.`id` = `se`.`inventory_report_id`
                                  
                                WHERE 
                                      `se`.`venue_id` =  :venueID2
                                  AND `se`.`status`   =  1
                                  AND `se`.`id`       =  :eventID",
                        [
                            'venueID1'  => $venueId,
                            'venueID2'  => $venueId,
                            'eventID'   => $eventID,
                        ]
                    );

        $event     = get_object_vars($event[0]);

        if ($event['datetime'] === '0000-00-00 00:00:00')
        {
            $event = [];
        }
        else
        {
            if (is_null($event['datetime_end']))
            {
                $datetime       =   new DateTime($event['datetime']);
                $dateInterval   =   new DateInterval('PT' + $eventTimeEndShift + 'H');
                $datetime->add($dateInterval);
                $event['datetime_end'] = $datetime->format($dateFormat);
            }
            else
            {
                $event['datetime_end'] = date($dateFormat, strtotime($event['datetime_end']));
            }

            $event['datetime'] = date($dateFormat, strtotime($event['datetime']));
        }

        return $event;
    }


    private function getEventsDateListByVenue(int $venueId, string $dateFormat, array $eventData)
    {
        // Event search date range.
        $eventSearchStartDate   =   (!empty($eventData['header_start_date_sql'])
                                        ? $eventData['header_start_date_sql']
                                        : false);
        $eventSearchEndDate     =   (!empty($eventData['header_end_date_sql'])
                                        ? $eventData['header_end_date_sql']
                                        : false);

        // Select dropdown default date range
        $eventSelectFirstStart  =   (!empty($eventData['event_select_first_start'])
                                        ? $eventData['event_select_first_start']
                                        : false);
        $eventSelectFirstEnd    =   (!empty($eventData['event_select_first_end'])
                                        ? $eventData['event_select_first_end']
                                        : false);

        // If the date range is the same as the default date range then display events in the last 30 days
        if ($eventSearchStartDate === $eventSelectFirstStart && $eventSearchEndDate === $eventSelectFirstEnd)
        {
            $eventSearchStartDate   = FALSE;
            $eventSearchEndDate     = FALSE;
        }

        $events = $this->getEventsByVenue($venueId, $eventSearchStartDate, $eventSearchEndDate);

        //dd($events);

        $output = [];
        foreach ($events as $event)
        {
            if ($event->datetime == '0000-00-00 00:00:00')
            {
                continue;
            }

            if (is_null($event->datetime_end))
            {
                $datetime = new DateTime($event->datetime);
                $datetime->modify('+' . $eventData['event_time_end_shift'] . ' hours');
                $event->datetime_end = $datetime->format($dateFormat);
            }
            else
            {
                $event->datetime_end = date($dateFormat, strtotime($event->datetime_end));
            }

            $event->datetime = date($dateFormat, strtotime($event->datetime));

            $output[] = $event;
        }

        //dd($output);
        return $output;
    }


    private function getEventsByVenue(int $venueId, string $eventSearchStartDate, string $eventSearchEndDate):array
    {
        $startDate  = (!empty($eventSearchStartDate)    ? $eventSearchStartDate : 'NOW() - INTERVAL 30 DAY');
        $endDate    = (!empty($eventSearchEndDate)      ? $eventSearchEndDate   : 'NOW()');

        $results    = DB::select
        (
            "SELECT 
                      se.id, 
                      se.seatgeek_id, 
                      se.name, 
                      se.lock_devices_datetime, 
                      se.latitude, 
                      se.longitude, 
                      se.price, 
                      se.short_description, 
                      se.long_description, 
                      se.modified, 
                      se.layout_id, 
                      se.is_active, 
                      se.venue_id, 
                      se.available_items, 
                      se.status, 
                      se.venue_start, 
                      se.venue_end, 
                      se.vendor_start, 
                      se.vendor_end, 
                      se.alcohol_start, 
                      se.alcohol_end, 
                      se.training_start, 
                      se.training_end, 
                      se.confirm_finalize, 
                      se.inventory_report_id, 
                      COALESCE(ir.date_opened, se.datetime) as datetime, 
                      COALESCE(ir.date_closed, se.datetime_end, NOW()) as datetime_end, 
                      se.datetime as calendar_event_start, 
                      se.datetime_end as calendar_event_end, 
                      se.id as event_id, 
                      se.name as event_name, 
                      se.datetime as date, 
                      COALESCE(ed.event_id, 0) as ed_count, 
                      se.confirm_finalize
                      
                    FROM (`Event` se)
                      LEFT JOIN `event_data` ed         ON `se`.`id` = `ed`.`event_id` AND se.venue_id = :venueID1
                      LEFT JOIN `inventory_reports` ir  ON `ir`.`id` = `se`.`inventory_report_id`
                    
                    WHERE 
                        `se`.`venue_id` =   :venueID2
                    AND `se`.`status`   =   1
                    AND `se`.`datetime` >=  :startDate
                    AND `se`.`datetime` <   :endDate
                    
                    GROUP BY `se`.`id`
                    ORDER BY `datetime` desc",
            [
                'venueID1'  => $venueId,
                'venueID2'  => $venueId,
                'startDate' => $startDate,
                'endDate'   => $endDate,
            ]
        );

        return $results;
    }


    private function getVenuesIdsByCorporate(int $corporateId)
    {
        $venues = DB::select
        (
            "SELECT venue_id
                    FROM corporate_venues
                    WHERE corporate_id = :corporateId    
                    ",
            [
                'corporateId' => $corporateId
            ]
        );
        $venuesIds = [];
        foreach ($venues as $v)
        {
            $venuesIds[] = $v['venue_id'];
        }

        return $venuesIds;
    }

    private function getUsedParkingPassCodes(array $parkingPassData)
    {
        /*'venue_id', 'vendor_id', 'account_id', 'used_from_date', 'used_to_date' */
        if(!is_array($parkingPassData))
        {
            $parkingPassData = array();
        }

        $parkingPassData['used'] = TRUE;

        // Quick fix for corp accounts that don't have a venue_id set. Make sure venue_id gets set, else passes from all venues will be returned.
        $parkingPassData['venue_id'] = !empty($parkingPassData['venue_id']) ? $parkingPassData['venue_id'] : -1;

        $whereVenueIDString = "";
        if (!empty($parkingPassData['venue_id']))
        {
            if (!is_array($parkingPassData['venue_id']))
            {
                $whereVenueIDString = "AND p.venueId = " . $parkingPassData['venue_id'];
            }
            else
            {
                $whereVenueIDString = "AND p.venueId IN (" . implode(", ", $parkingPassData['venue_id']) . ")";
            }
        }

        $output = DB::select("SELECT 
                p.*, 
                ppr.accountType, 
                ppr.accountId, 
                ppr.employeeId, 
                vg.id AS vendor_group
                
            FROM ParkingPassCode p  
                   JOIN ParkingPassResponse ppr           ON  ppr.parkingPassCodeId = p.parkingPassCodeId  AND ppr.status='USED'
              LEFT JOIN vendors_serving_vendor_group vsvg ON  vsvg.vendor_id = ppr.accountId AND ppr.accountType = 'VENDOR'
              LEFT JOIN vendor_group vg                   ON  vsvg.vendor_group_id = vg.id
              
            WHERE 
                vg.status     = 1 
            AND vg.is_active  = 1 
            {$whereVenueIDString}                    
        ");
//dd($output);
        return $output;

    }


    /**
     * @param array $requestData
     * @return array
     */
    private function getReportSettings(array $requestData):array
    {
        $output = [
            'venueId'               => $requestData['venue_id'],
            'order_date_field'      => self::DEVICEORDERTIME_FIELD,
            'email'                 => $requestData['email'],
            'report'                => $requestData['report'],
            'event_selected'        => $requestData['event_date'],
            'event_time_end_shift'  => $requestData['event_time_end_shift'],
            'connect_entity_id'     => $requestData['connect_entity_id'],
            'connect_entity_type'   => $requestData['connect_entity_type'],
            'start_date'            => $requestData['start_date'],
            'end_date'              => $requestData['end_date'],
            'grouping'              => (isset($requestData['grouping']) && is_string($requestData['grouping']) ? $requestData['grouping'] : 'product'),
            'sorting'               => (isset($requestData['sorting'])  && is_string($requestData['sorting'])  ? $requestData['sorting']  : 'value'),
            'filter'                => "venue",
            'filter_id'             => $requestData['venue_id'],
//            'filter_venue_id'       => $requestData['filter_venue_id'],
            'weekdays'              => $requestData['weekdays'],
            'reportType'            => (isset($requestData['reportType']) && is_string($requestData['reportType']) && in_array($requestData['reportType'], ['excel','csv','pdf'])
                                        ? $requestData['reportType']
                                        : 'excel'),
//            'limit_offset'          => $requestData['limitOffset'],
//            'include_comps'         => $requestData['include_comps'],
//            'hide_payment_info'     => $requestData['hide_payment_info'],
            'type_guest'            => $requestData['type_guest'],
            'type_user'             => $requestData['type_user'],
//            'filter2'               => $requestData['filter2'],
//            'filter_id2'            => $requestData['filter_id2'],
//            'filter3'               => $requestData['filter3'],
//            'filter_id3'            => $requestData['filter_id3'],
//            'category_1_ids'        => $requestData['category_1_ids'],
//            'category_2_ids'        => $requestData['category_2_ids'],
//            'category_3_ids'        => $requestData['category_3_ids'],
//            'hide_no_cost_items'    => $requestData['hide_no_cost_items'],
//            'roll_up_modifiers'     => $requestData['roll_up_modifiers'],
//            'html_preview'          => $requestData['html_preview'],
//            'html_pdf_fix'          => $requestData['html_pdf_fix'],
//            'reportName'            => $requestData['report_name'],
//            'vendors'               => explode(',', $requestData['vendors']),
//            'vendor_groups'         => explode(',', $requestData['vendor_groups']),
        ];

        return $output;
    }


    /**
     * @param array $reportSettings
     * @return array
     */
    private function getReportData(array $reportSettings):array
    {
        // Mimicking Constructor

        $venueInfo              =   $this->getVenueInfo($reportSettings['connect_entity_type'], $reportSettings['connect_entity_id']);

        if (isset($venueInfo['report_based_on_datetime']) && $venueInfo['report_based_on_datetime'] != '')
        {
            $this->set_order_date_field($venueInfo['report_based_on_datetime']);
        }
        elseif ($reportSettings['connect_entity_type'] === 'corporate' && (isset($reportSettings['connect_entity_id']) && $reportSettings['connect_entity_id'] != '') )
        {
            $this->set_order_date_field(1);
        }

        $this->order_date_field = $reportSettings['order_date_field'] = (!is_null($this->order_date_field) ? $this->order_date_field : $reportSettings['order_date_field']);


        $activeLayoutInfo       =   $this->getActiveLayoutInfo($reportSettings['connect_entity_type'], $reportSettings['connect_entity_id']);

        //dd($activeLayoutInfo);

        $timezone               =   isset($venueInfo['timezone'])        ? $venueInfo['timezone']         : 'America/Los_Angeles';
        $dayStartTime           =   isset($venueInfo['day_start_time'])  ? $venueInfo['day_start_time']   : '00:00:00';
        $dayEndTime             =   isset($venueInfo['day_end_time'])    ? $venueInfo['day_end_time']     : '00:00:00';

        $adjusted_dates         =   $this->setAdjustedDateParameters
                                    (
                                        $reportSettings['start_date'],
                                        $reportSettings['end_date'],
                                        $timezone,
                                        $dayStartTime,
                                        $dayEndTime
                                    );

        $headerStartDate        =   date(self::VIEW_DATE_FORMAT  , strtotime($adjusted_dates['start_date']));
        $headerStartDateSql     =   date(self::DATE_FORMAT       , strtotime($adjusted_dates['start_date']));
        $headerEndDate          =   date(self::VIEW_DATE_FORMAT  , strtotime($adjusted_dates['end_date']));
        $headerEndDateSql       =   date(self::DATE_FORMAT       , strtotime($adjusted_dates['end_date']));

        $startDate              =   $this->convertToUTC($adjusted_dates['start_date'], $timezone);
        $endDate                =   $this->convertToUTC($adjusted_dates['end_date'], $timezone);
        $currentVenueDateTime   =   $this->convertFromUTC(date(self::DATE_FORMAT), $timezone, 'm-d-Y g:i:s A');

        $eventID                =   $reportSettings['event_selected'];
        $adjustedDates2         =   $this->setAdjustedDateParameters
                                    (
                                        false,
                                        false,
                                        $timezone,
                                        $dayStartTime,
                                        $dayEndTime
                                    );
        $eventSelectFirstStart  =   $adjustedDates2['start_date'];
        $eventSelectFirstEnd    =   $adjustedDates2['end_date'];
        $eventsList             =   $this->getEventsDateListByVenue
                                    (
                                        $reportSettings['venueId'],
                                        self::VIEW_DATE_FORMAT,
                                        [
                                            'header_start_date_sql'     => $headerStartDateSql,
                                            'header_end_date_sql'       => $headerEndDateSql,
                                            'event_select_first_start'  => $eventSelectFirstStart,
                                            'event_select_first_end'    => $eventSelectFirstEnd,
                                            'event_time_end_shift'      => $reportSettings['event_time_end_shift'],
                                        ]
                                    );

        //dd($eventsList);

        if($eventID && $eventID !== 0)
        {
            $eventSelected = $this->setEventSelected($eventID, $reportSettings['venueId'], $reportSettings['event_time_end_shift']);

            if (isset($eventSelected) && !$this->existEventInEventsList($eventsList, $eventSelected))
            {
                $eventsList[] = $eventSelected;
            }
        }

        //dd($eventsList);

        // Mimicking report run

        $daysBetween            =   floor((strtotime($endDate) - strtotime($startDate)) / (24 * 60 * 60));

        if (isset($eventSelected) && $eventSelected !== 0)
        {
            $startDate = $this->convertToUTC($eventSelected['datetime'], $timezone);
            $endDate   = $this->convertToUTC($eventSelected['datetime_end'], $timezone);
        }

        $report_name        = 'Tender Type';
        $report_title       = 'Tender Type Report';
        $report_summary     = 'Tender Type Report for a date range';
        $report_value_field = 'product_cost';
        $prepend_unit       = '$';
        $empty_report_text  = 'No vendor found.';

        $outputHtmlBodyToFile   =   false;
        $outputHtmlFileHandle   =   null;
        $outputHtmlFileName     =   null;
        $outputCssFileHandle    =   null;
        $outputCssFileName      =   null;

        if( $reportSettings['reportType'] == 'pdf'
            &&
            (
                    in_array($reportSettings['report'], ['salesPerHour'])
                ||  $reportSettings['html_pdf_fix']
            )
        )
        {
            $uniqueId               =   uniqid();
            $outputHtmlFileName     =   self::API_IMAGE_UPLOAD_PATH.'temphtmltopdf'.$uniqueId.'.html';
            $outputCssFileName      =   self::API_IMAGE_UPLOAD_PATH.'temphtmltopdf'.$uniqueId.'.css';
            $outputHtmlFileHandle   =   fopen($outputHtmlFileName, "w");
            $outputCssFileHandle    =   fopen($outputCssFileName, "w");
            $outputHtmlBodyToFile   =   ($outputHtmlFileHandle && $outputCssFileHandle);
        }

        // Mimicking second switch
        $extra_user = [FALSE, FALSE];
        if ($reportSettings['type_guest'] == '1')
        {
            $extra_user[0] = TRUE;
        }

        if ($reportSettings['type_user'] == '1')
        {
            $extra_user[1] = TRUE;
        }

        $parking_pass_requirements             = [];
        switch ($reportSettings['connect_entity_type'])
        {
            case 'venue':
                $parking_pass_requirements['venue_id'] = $reportSettings['connect_entity_id'];
                break;
            case 'corporate':
                $venueIds = $this->getVenuesIdsByCorporate($reportSettings['connect_entity_id']);
                $parking_pass_requirements['venue_id'] = !empty($venueIds) ? $venueIds : -1;
                break;
        }

        if (isset($reportSettings['start_date']))
        {
            $parking_pass_requirements['used_from_date'] = $reportSettings['start_date'];
        }

        if (isset($reportSettings['end_date']))
        {
            $parking_pass_requirements['used_to_date'] = $reportSettings['end_date'];
        }


        $all_parking_pass_used              = $this->getUsedParkingPassCodes($parking_pass_requirements);
        //dd($all_parking_pass_used);
        $parking_pass_codes_all             = [];
        $parking_pass_codes_by_vendor       = [];
        $parking_pass_codes_by_account      = [];
        $parking_pass_codes_by_venue        = [];
        $parking_pass_codes_by_vendor_group = [];


        foreach ($all_parking_pass_used as $parking_pass)
        {

            if (!key_exists($parking_pass['parkingPassCodeId'], $parking_pass_codes_all)) {
                $parking_pass_codes_all[$parking_pass['parkingPassCodeId']] = 1;
            }
            $parking_pass_codes_by_vendor[$parking_pass['accountId']][$parking_pass['parkingPassCodeId']] = 1;

            if (!key_exists($parking_pass['accountId'], $parking_pass_codes_by_vendor)) {
                $parking_pass_codes_by_vendor[$parking_pass['accountId']] = [];
            }
            $parking_pass_codes_by_vendor[$parking_pass['accountId']][$parking_pass['parkingPassCodeId']] = 1;

            if (!key_exists($parking_pass['employeeId'], $parking_pass_codes_by_account)) {
                $parking_pass_codes_by_account[$parking_pass['employeeId']] = [];
            }
            $parking_pass_codes_by_account[$parking_pass['employeeId']][$parking_pass['parkingPassCodeId']] = 1;

            if (!key_exists($parking_pass['venueId'], $parking_pass_codes_by_venue)) {
                $parking_pass_codes_by_venue[$parking_pass['venueId']] = [];
            }
            $parking_pass_codes_by_venue[$parking_pass['venueId']][$parking_pass['parkingPassCodeId']] = 1;

            if (!key_exists($parking_pass['vendor_group'], $parking_pass_codes_by_vendor_group)) {
                $parking_pass_codes_by_vendor_group[$parking_pass['vendor_group']] = 0;
            }
            $parking_pass_codes_by_vendor_group[$parking_pass['vendor_group']]++;
        }

        foreach ($parking_pass_codes_by_vendor as $k => &$val)
        {
            $parking_pass_codes_by_vendor[$k] = count($val);
        }

        foreach ($parking_pass_codes_by_account as $k => &$val)
        {
            $parking_pass_codes_by_account[$k] = count($val);
        }

        foreach ($parking_pass_codes_by_venue as $k => &$val)
        {
            $parking_pass_codes_by_venue[$k] = count($val);
        }

        $parkingPassCodes               =   count($parking_pass_codes_all);
        $parkingPassCodesByVendor       =   $parking_pass_codes_by_vendor;
        $parkingPassCodesByAccount      =   $parking_pass_codes_by_account;
        $parkingPassCodesByVenue        =   $parking_pass_codes_by_venue;
        $parkingPassCodesByVendorGroup  =   $parking_pass_codes_by_vendor_group;



        $output = [
            'venueInfo'             =>  $venueInfo,
            'activeLayoutInfo'      =>  $activeLayoutInfo,
            'header_start_date'     =>  $headerStartDate,
            'header_start_date_sql' =>  $headerStartDateSql,
            'header_end_date'       =>  $headerEndDate,
            'header_end_date_sql'   =>  $headerEndDateSql,
            'startDate'             =>  $startDate,
            'endDate'               =>  $endDate,
            'currentVenueDateTime'  =>  $currentVenueDateTime,
            'report'                =>  [
                'meta'      =>  [
                    'name'              => $report_name,
                    'title'             => $report_title,
                    'summary'           => $report_summary,
                    'valueField'        => $report_value_field,
                    'prependUnit'       => $prepend_unit,
                    'emptyReportText'   => $empty_report_text,
                ],
                'data'      =>  [
                    'timeZone'              =>   $timezone,
                    'weekdays'              =>   ($reportSettings['reportType'] == 'excel' && $reportSettings['weekdays'] == '1' && $daysBetween <= 15
                                                    ? TRUE
                                                    : FALSE),
                    'extraUser'             => $extra_user,
                    'daysBetween'           => $daysBetween,
                    'outputHtmlFileName'    => $outputHtmlFileName,
                    'outputCssFileName'     => $outputCssFileName,
                    'outputHtmlFileHandle'  => $outputHtmlFileHandle,
                    'outputCssFileHandle'   => $outputCssFileHandle,
                    'outputHtmlBodyToFile'  => $outputHtmlBodyToFile,
                ],
                'parking'   =>  [
                    'parking_pass_codes'                =>   $parkingPassCodes,
                    'parking_pass_codes_by_vendor'      =>   $parkingPassCodesByVendor,
                    'parking_pass_codes_by_account'     =>   $parkingPassCodesByAccount,
                    'parking_pass_codes_by_venue'       =>   $parkingPassCodesByVenue,
                    'parking_pass_codes_by_vendor_group'=>   $parkingPassCodesByVendorGroup,
                ],
                'reportSettings' => $reportSettings,
            ],
        ];

        //dd($output);

        return $output;
    }


    /**
     * determine if an event is into Events list
     * @param $events
     * @param $eventSelected
     *
     * @return bool
     */
    private function existEventInEventsList($events, $eventSelected)
    {
        $existEvent = false;

        foreach ($events as $event)
        {
            $event     = get_object_vars($event);
            if ($event['id'] == $eventSelected['id'])
            {
                $existEvent = true;
                break;
            }
        }

        return $existEvent;
    }


    /**
     * @param $requestData
     * @return array
     */
    private function prepareReportVariables($requestData):array
    {
        $reportSettings =   $this->getReportSettings($requestData);
        $reportData     =   $this->getReportData($reportSettings);

        return $reportData;
    }


    /**
     * @param string $connectEntityType
     * @param int $connectEntityID
     * @return array
     */
    private function getVenueInfo(string $connectEntityType, int $connectEntityID):array
    {
        switch($connectEntityType)
        {
            case 'venue':
                return $this->getVenueInfoByVenueID($connectEntityID);
                break;
            case 'corporate':
                return $this->getVenueInfoByCorporateID($connectEntityID);
                break;
        }
    }


    /**
     * @param string $connectEntityType
     * @param int $connectEntityID
     * @return array
     */
    private function getActiveLayoutInfo(string $connectEntityType, int $connectEntityID):array
    {
        switch($connectEntityType)
        {
            case 'venue':
                return DB::select
                (
                    "SELECT 
                              `id`, 
                              `name`, 
                              `status`, 
                              `is_active`
                            FROM (`layouts`)
                            WHERE `venue_id` =  :venueId
                            AND `status` =  1
                            AND `is_active` =  1
                            ORDER BY `name`",
                    [
                        'venueId' => $connectEntityID,
                    ]
                );
                break;

            case 'corporate':
                return ['id' => null, 'name' => null, 'status' => 1, 'is_active' => 1];
                break;


        }
    }


    /**
     * @param int $venueID
     * @return array
     */
    private function getVenueInfoByVenueID(int $venueID):array
    {
        $results = DB::select
        (
            "SELECT 
                      v.id, 
                      v.shiftplanner_url, 
                      v.latitude, 
                      v.longitude, 
                      v.region_code, 
                      v.micros_venue_id, 
                      v.image_url, 
                      v.campus_icon, 
                      v.micros_gate_url, 
                      v.micros_employee_number, 
                      v.micros_idref_order_type, 
                      v.micros_idref_seat, 
                      v.micros_idref_cash, 
                      v.micros_idref_card, 
                      v.micros_idref_givex, 
                      v.micros_idref_notes, 
                      v.micros_last_heartbeat, 
                      v.micros_heartbeat_interval, 
                      v.micros_notifications_timeout, 
                      v.micros_admin_email, 
                      v.is_open, 
                      v.training_mode, 
                      v.alcohol_enabled, 
                      v.welcome_text, 
                      v.tweet, 
                      v.twitter_rss, 
                      v.ticket_url, 
                      v.event, 
                      v.links, 
                      v.fee_text, 
                      v.receipt_text_locale, 
                      v.seat_geek, 
                      v.info, 
                      v.drink_limit, 
                      v.tip_status, 
                      v.timezone, 
                      v.seat_required, 
                      v.type, 
                      v.layout_type, 
                      v.info_only, 
                      v.name, 
                      v.promo_enabled, 
                      v.alcohol_cutoff_time, 
                      v.alcohol_cutoff_min, 
                      v.disable_receipt, 
                      v.use_spreedly, 
                      v.spreedly_gateway_token, 
                      v.default_vendor_id, 
                      v.total_by_active_layout, 
                      v.givex_user, 
                      v.givex_pass, 
                      v.givex_url1, 
                      v.givex_port1, 
                      v.givex_url2, 
                      v.givex_port2, 
                      v.givex_use2, 
                      v.available_items, 
                      v.control_php_mode, 
                      v.day_start_time, 
                      v.day_end_time, 
                      
                      vt.sp_username, 
                      vt.sp_password, 
                      vt.sp_api_key, 
                      
                      v.tw_orders_enabled, 
                      v.tw_orders_tag, 
                      v.allow_sold_out_sales, 
                      v.is_drink_limit_per_event, 
                      v.refund_add_inv_back, 
                      v.wristband_pin_required, 
                      v.report_based_on_datetime, 
                      v.complete_non_credit_order, 
                      v.support_email, 
                      
                      a.email as admin_email, 
                      
                      v.micros_fee_idref, 
                      v.populate_check_id, 
                      v.sending_partial_tabs, 
                      v.track_inventory, 
                      v.remove_extra_character_pp, 
                      v.item_tax_required, 
                      v.storedValuedQrCodesTrimmingMode, 
                      v.skidataExchangeRate, 
                      v.skidataSpendPointsRate, 
                      v.currency_id, 
                      v.tip_tender, 
                      v.cost_after_receiving, 
                      v.use_sandbox_gateway, 
                      v.skidata_encryption, 
                      v.cc_blacklisting_enabled, 
                      v.suites_enabled, 
                      v.hawking_enabled, 
                      v.passwordMinimumLength, 
                      v.passwordExpirationDays, 
                      v.passwordBlockedLast, 
                      v.passwordIdleTimeMinutes, 
                      v.passwordDisableAfterDays, 
                      v.passwordFailedAttemptsCount, 
                      v.report_refund_on_datetime, 
                      
                      vt.sp_username, 
                      vt.sp_password, 
                      vt.sp_api_key, 
                      
                      v.tw_orders_enabled, 
                      v.tw_orders_tag, 
                      v.allow_sold_out_sales, 
                      v.is_drink_limit_per_event, 
                      v.refund_add_inv_back, 
                      v.wristband_pin_required, 
                      v.report_based_on_datetime, 
                      v.ledgerReporting, 
                      v.complete_non_credit_order, 
                      v.support_email, 
                      
                      a.email as admin_email, 
                      
                      v.micros_fee_idref, 
                      v.populate_check_id, 
                      v.sending_partial_tabs, 
                      v.track_inventory, 
                      v.remove_extra_character_pp, 
                      v.item_tax_required, 
                      v.storedValuedQrCodesTrimmingMode, 
                      v.skidataExchangeRate, 
                      v.skidataSpendPointsRate, 
                      v.currency_id, 
                      v.tip_tender, v.cost_after_receiving, v.use_sandbox_gateway, v.skidata_encryption, v.cc_blacklisting_enabled, v.beaconPaymentAtCounterEnabled, v.shouldPreselectDefaultTip, v.emailOptInEnabled, v.emailOptInDisplayText, v.showChaseBanners, v.shouldAmexCardReduceFee, v.shouldUseRichMerchandiseView, v.shouldUseGradients, v.applePayMerchantIdentifier, v.applePayDisplayedEndMerchantName, v.applePayDisplayedYourAppName, auv.api_user_id as api_user_id, au.shouldAmexCardReduceFee as auShouldAmexCardReduceFee, v.kioskTheme, v.roundingType, v.roundingPrecision, v.taxPerItem, v.tendersEnabled, v.showLiveNationPromo, v.autoCalculateCogs, v.perpetualInventory, v.checkSyncingEnabled, v.allowPartialTipRefund, v.restaurantsEnabled, v.cashRoomByVendor, v.itemTabSortPriority, v.enableAdCampaign, v.adCampaignUrl, i.id AS adCampaignImageId, i.image AS adCampaignImageUrl, v.useSso, 
                      v.reconcileByDenomination, 
                      v.defaultCashRoomDrops
                      
                FROM (`venues` v)
                LEFT JOIN `venue_timeclock` vt ON `v`.`id`=`vt`.`venue_id`
                LEFT JOIN `admin` a ON `a`.`id` = `v`.`admin_id`
                LEFT JOIN `api_user_venues` auv ON `auv`.`venue_id` = `v`.`id`
                LEFT JOIN `api_user` au ON `au`.`id` = `auv`.`api_user_id`
                LEFT JOIN `images` i ON `v`.`id` = `i`.`type_id` AND i.type = 'venue_ad_campaign'
                WHERE `v`.`id` =  :venueID
                LIMIT 1",
            [
                'venueID' => $venueID
            ]
        );

        return $results;
    }


    /**
     * @param int $corporateID
     * @return array
     */
    private function getVenueInfoByCorporateID(int $corporateID):array
    {


        return [];
    }
}
