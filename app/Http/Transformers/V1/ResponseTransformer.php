<?php
namespace App\Http\Transformers\V1;

use Validator;
use League\Fractal\TransformerAbstract;
use Carbon\Carbon;

class ResponseTransformer extends TransformerAbstract
{
    public function validateResponse($responseOutput)
    {
        return Validator::make
        (
            $responseOutput,
            [
                'status'            => 'required|boolean',
                'message'           => 'required|string',
                'httpCode'          => 'required|integer',
                'data'              => 'required|array',
                'data.redirectUrl'  => 'required|string',
                'errors'            => 'array',
            ],
            []
        );
    }


    /**
     * Verify the accuracy of the generic output format.
     *
     * @param array $responseOutput
     *
     * @author <chukky@appetizeapp.com> Chukky Nze
     * @date 07/10/2017 6:33 PM
     *
     * @return array
     */
    public function transform(array $responseOutput)
    {
        $validation = $this->validateResponse($responseOutput);

        if ($validation->passes())
        {
            return [
                "status"    =>  (bool)$responseOutput['status'],
                "message"   =>  (string)$responseOutput['message'],
                "httpCode"  =>  (int)$responseOutput['httpCode'],
                "data"      =>  $responseOutput['data'],
                "errors"    =>  $responseOutput['errors'],

                "timestamp" =>  Carbon::now('UTC'),
            ];
        }
        else
        {
            return [
                "status"   => false,
                "message"   => "Improper response format.",
                "httpCode"  => 500,
                "data"      => [],
                "errors"    => $validation->errors()->getMessages(),

                "timestamp" => Carbon::now('UTC'),
            ];
        }
    }
}