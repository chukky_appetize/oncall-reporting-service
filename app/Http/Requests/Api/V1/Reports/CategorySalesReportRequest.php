<?php
namespace App\Http\Requests\Api\V1\Reports;

use App\Exceptions\Requests\Api\V1\Reports\FailedAuthorizationException;
use App\Exceptions\Requests\Api\V1\Reports\FailedValidation;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class CategorySalesReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize():bool
    {
        return TRUE;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules():array
    {
        return [
            'venue_id'              =>  'required|numeric',
            'email'                 =>  'required|email',
            'report'                =>  'required|string|in:categorySales',
            'start_date'            =>  'required|date_format:"Y-m-d H:i:s"',
            'end_date'              =>  'required|date_format:"Y-m-d H:i:s"|after:start_date',
            'event_date'            =>  'required|numeric',
            'connect_entity_id'     =>  'required|numeric',
            'connect_entity_type'   =>  'required|in:venue,corporate',
            'event_time_end_shift'  =>  'required|numeric', // The number of hours which should be added in the case of lack a events end-date
            'grouping'              =>  '',
            'sorting'               =>  '',
            'filter'                =>  '',
            'filter_id'             =>  '',
            'filter_venue_id'       =>  '',
            'weekdays'              =>  'required|boolean',
            'reportType'            =>  'required|in:excel,csv,pdf,odf',
            'limitOffset'           =>  '',
            'include_comps'         =>  'required|boolean',
            'hide_payment_info'     =>  '',
            'type_guest'            =>  '',
            'type_user'             =>  '',
            'filter2'               =>  '',
            'filter_id2'            =>  '',
            'filter3'               =>  '',
            'filter_id3'            =>  '',
            'category_1_ids'        =>  'required|numeric',
            'category_2_ids'        =>  'required|numeric',
            'category_3_ids'        =>  'required|numeric',
            'hide_no_cost_items'    =>  '',
            'roll_up_modifiers'     =>  '',
            'html_preview'          =>  '',
            'html_pdf_fix'          =>  '',
            'report_name'           =>  '',
            'vendors'               =>  'required|numeric',
            'vendor_groups'         =>  'required|numeric',
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages():array
    {
        return [];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @author Chukky Nze <chukkynze@gmail.com>
     *
     * @param Validator $validator
     *
     * @return FailedValidation
     * @throws FailedValidation
     */
    protected function failedValidation(Validator $validator):FailedValidation
    {
        throw new FailedValidation("Request failed validation.", $validator, '/');
    }

    /**
     * Handle a failed authorization attempt.
     *
     * @return FailedAuthorizationException
     * @throws FailedAuthorizationException
     */
    protected function failedAuthorization():FailedAuthorizationException
    {
        throw new FailedAuthorizationException('This action is unauthorized.');
    }
}
