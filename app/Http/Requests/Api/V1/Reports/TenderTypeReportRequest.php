<?php
namespace App\Http\Requests\Api\V1\Reports;

use App\Exceptions\Requests\Api\V1\Reports\FailedAuthorizationException;
use App\Exceptions\Requests\Api\V1\Reports\FailedValidation;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class TenderTypeReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize():bool
    {
        return TRUE;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules():array
    {
        return [
            'venue_id'              =>  'required|numeric',
            'email'                 =>  'required|email',
            'report'                =>  'required|string|in:tenderType',
            'start_date'            =>  'required|date_format:"Y-m-d H:i:s"',
            'end_date'              =>  'required|date_format:"Y-m-d H:i:s"|after:start_date',
            'event_date'            =>  'required|numeric',
            'connect_entity_id'     =>  'required|numeric',
            'connect_entity_type'   =>  'required|in:venue,corporate',
            'event_time_end_shift'  =>  'required|numeric', // The number of hours which should be added in the case of lack a events end-date
            'grouping'              =>  '',
            'sorting'               =>  '',
            'filter'                =>  '',
            'filter_id'             =>  '',
            'filter_venue_id'       =>  '',
            'weekdays'              =>  'required|boolean',
            'reportType'            =>  'required|in:excel,csv,pdf,odf',
            'limitOffset'           =>  '',
            'include_comps'         =>  '',
            'hide_payment_info'     =>  '',
            'type_guest'            =>  'required|numeric',
            'type_user'             =>  'required|numeric',
            'filter2'               =>  '',
            'filter_id2'            =>  '',
            'filter3'               =>  '',
            'filter_id3'            =>  '',
            'category_1_ids'        =>  '',
            'category_2_ids'        =>  '',
            'category_3_ids'        =>  '',
            'hide_no_cost_items'    =>  '',
            'roll_up_modifiers'     =>  '',
            'html_preview'          =>  '',
            'html_pdf_fix'          =>  '',
            'report_name'           =>  '',
            'vendors'               =>  '',
            'vendor_groups'         =>  '',
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages():array
    {
        return [];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @author Chukky Nze <chukkynze@gmail.com>
     *
     * @param Validator $validator
     *
     * @return FailedValidation
     * @throws FailedValidation
     */
    protected function failedValidation(Validator $validator):FailedValidation
    {
        throw new FailedValidation("Request failed validation.", $validator, '/');
    }

    /**
     * Handle a failed authorization attempt.
     *
     * @return FailedAuthorizationException
     * @throws FailedAuthorizationException
     */
    protected function failedAuthorization():FailedAuthorizationException
    {
        throw new FailedAuthorizationException('This action is unauthorized.');
    }
}
