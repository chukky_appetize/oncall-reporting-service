<?php
namespace App\Exceptions\Requests\Api\V1;

use App\Http\Transformers\V1\ResponseTransformer;
use App\Traits\Utilities\TransformsResponses;
use Illuminate\Http\Response;
use Illuminate\Contracts\Validation\Validator;
use Throwable;

class FormRequestValidationException extends FormRequestException
{
    use TransformsResponses;

    public $validator;
    public $redirectUrl;

    public function __construct($message = "", Validator $validator, string $redirectUrl, $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->validator    =   $validator;
        $this->redirectUrl  =   $redirectUrl;
    }

    public function getValidator():Validator
    {
        return $this->validator;
    }

    public function getRedirectUrl()
    {
        return $this->redirectUrl;
    }

    /**
     * Report the exception.
     *
     * @return void
     */
    public function report()
    {
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function render()
    {
        $this->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);

        $data = [
            'status'    =>  FALSE,
            'message'   =>  $this->getMessage(),
            'data'      =>  [
                'redirectUrl' => $this->getRedirectUrl()
            ],
            'errors'    =>  $this->getValidator()->errors()->getMessages(),
        ];

        $data = array_merge($data, ['httpCode' => $this->getStatusCode()]);
        $cleanedResponse = fractal([$data], new ResponseTransformer())->toArray();

        return response()->json($cleanedResponse['data'][0], $this->getStatusCode(), []);
    }
}