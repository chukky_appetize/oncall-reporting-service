<?php
namespace App\Exceptions\Requests\Api\V1;

use Illuminate\Contracts\Validation\Validator;
use App\Traits\Utilities\TransformsResponses;
use App\Http\Transformers\V1\ResponseTransformer;
use Illuminate\Http\Response;
use Auth;
use Exception;
use Throwable;

class FormRequestException extends Exception
{
    use TransformsResponses;

    public $redirectUrl;
    public $message;

    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->message      = $message;
        $this->redirectUrl  = (Auth::check() ? "/v1/dashboard/overview" : "/secure/logout" );

    }

    public function getRedirectUrl()
    {
        return $this->redirectUrl;
    }

    /**
     * Report the exception.
     *
     * @return void
     */
    public function report()
    {
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function render()
    {
        $this->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);

        $data = [
            'status'    =>  FALSE,
            'message'   =>  $this->getMessage(),
            'data'      =>  [
                'redirectUrl' => $this->getRedirectUrl()
            ],
            'errors'    =>  [$this->message],
        ];

        $data = array_merge($data, ['httpCode' => $this->getStatusCode()]);
        $cleanedResponse = fractal([$data], new ResponseTransformer())->toArray();

        return response()->json($cleanedResponse['data'][0], $this->getStatusCode(), []);
    }
}