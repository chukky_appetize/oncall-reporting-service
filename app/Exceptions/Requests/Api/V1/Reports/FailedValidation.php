<?php
namespace App\Exceptions\Requests\Api\V1\Reports;

use App\Exceptions\Requests\Api\V1\FormRequestValidationException;

class FailedValidation extends FormRequestValidationException {}