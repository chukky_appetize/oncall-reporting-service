<?php
namespace App\Exceptions\Requests\Api\V1\Reports;

use App\Exceptions\Requests\Api\V1\FormRequestException;

class FailedAuthorizationException extends FormRequestException {}