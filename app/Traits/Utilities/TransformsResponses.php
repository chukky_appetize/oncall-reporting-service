<?php
namespace App\Traits\Utilities;

use App\Http\Transformers\V1\ResponseTransformer;
use Illuminate\Http\Response as IlluminateResponse;

trait TransformsResponses
{
    //protected $statusCode = IlluminateResponse::HTTP_OK;

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    public function respond(array $data, array $additionalHeaders = [])
    {
        $data = array_merge($data, ['httpCode' => $this->getStatusCode()]);
        $cleanedResponse = fractal([$data], new ResponseTransformer())->toArray();

        // Regular good ol fashioned response
        return response()->json($cleanedResponse['data'][0], $this->getStatusCode(), $additionalHeaders);
    }
}
