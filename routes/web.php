<?php
use Carbon\Carbon;

Route::get('/', function ()
{
    $shownVars = DB::select("SHOW VARIABLES");

    $vars = [];
    foreach ($shownVars as $name => $shownVar)
    {
        $vars[$shownVar->Variable_name] = $shownVar->Value;
    }


    //dd($vars);
    $output = [
        'app' => 'Appetize Web Services - Reporting Service',
        'status' => true,
        'dependencies' => [
            'git' => [
                'currentBranch' => substr(explode("/", file('/var/www/.git/HEAD')[0], 3)[2], 0, -1),
            ],
            'db' => [
                'sql_mode' => $vars['sql_mode'],
                'innodb_version' => $vars['innodb_version'],
                'hostname' => $vars['hostname'],
            ],
        ],
        "timestamp" => Carbon::now('UTC'),
    ];

    return response()->json($output);
});
