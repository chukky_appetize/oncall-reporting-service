<?php

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::prefix('v1')->middleware([])->namespace('Api\V1')->group(function ()
{
    Route::prefix('reports')->namespace('Reports')->group(function ()
    {
        Route::get('/tender-type', 'TenderTypeReportController@index');
        Route::get('/category-sales', 'CategorySalesReportController@index'); // categorySales
    });
});
