FROM php:7.2.5-fpm-alpine3.7

LABEL maintainer="Chukky Nze <chukky@appetizeapp.com>"

ENV TIME_ZONE UTC

RUN apk update && apk upgrade
RUN apk add --update --no-cache apk-tools
RUN apk add --update --no-cache nginx
RUN apk add --update --no-cache curl
RUN apk add --update --no-cache supervisor
RUN apk add --update --no-cache zip
#RUN apk add mbstring
#RUN apk add --update --no-cache mcrypt
#RUN apk add --update --no-cache xml
RUN apk add --update --no-cache mysql
#RUN apk add --update --no-cache json
#RUN apk add --update --no-cache phar
#RUN apk add --update --no-cache dom
#RUN apk add --update --no-cache xmlwriter
#RUN apk add --update --no-cache tokenizer
#RUN apk add --update --no-cache session
#RUN apk add --update --no-cache pdo

COPY nginx.conf /etc/nginx/nginx.conf

COPY supervisord.conf /etc/supervisor/supervisord.conf
#http://supervisord.org/configuration.html default route start supervisord.conf

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
	php composer-setup.php && \
	php -r "unlink('composer-setup.php');" && \
	mv composer.phar /usr/local/bin/composer

WORKDIR /var/www/html
COPY . /var/www/html

RUN composer install
RUN composer update

ENTRYPOINT ["supervisord"]
#HEALTHCHECK --interval=1m CMD curl -f http://localhost:8888/ || exit 1
