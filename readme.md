## ON CAll Reporting Service

Use this to generate TT && CS reports based on venue (corporate not integrated yet - feel free to do so)

## Get Started
- Clone the repo
- Change ur .env vars to point to prod with ur creds
- use the VPN


My Workflow

- Check if anything is hanging around unnecessarily in docker:
`docker volume ls && docker image ls -a && docker container ls -a`

- Get rid of all your volumes
`docker volume prune -f`

- Delete Containers
`docker container rm reports_web_1 reports_app_1 -f`

- Clear out report service images
`docker image rm reports_web reports_app nginx php  -f`
IF this doesn't work then that means that docker isn't recognizing the image names. Use the first 3 characters of the image id instead. Run this:
`docker image ls -a `

then use the ids to run this:

`docker image rm f75 e69 -f` and `docker image prune -f` after to remove child images

- Verify by running:
`docker volume ls && docker image ls -a && docker container ls -a`
Everything should be empty or just free from any leftover reporting service stuff

-- To bring up the service...
`docker-compose up`

-- Open Postman and import the collection in the repo
Appetize - Reports Service.postman_collection.json

There should be
-- Health Check
-- TT Report
-- Tender Type Report
-- Category Sales Report

The first 2 are the same. The params needed are pretty much exactly the same as in Connect. Most variables appear in connect to set defaults. Here the defaults are in the params so that I can craft a proper request policy. 2 vars to note are:
- connect_entity_id => $this->id
- connect_entity_type => $this->idType


- First I check the HealthCheck. This will show me my branch and the db I'm connecting to which means I have a valid connection:

```
{
    "app": "Appetize Web Services - Reporting Service",
    "status": true,
    "dependencies": {
        "git": {
            "currentBranch": "master"
        },
        "db": {
            "sql_mode": "NO_ENGINE_SUBSTITUTION",
            "innodb_version": "5.7.19-17",
            "hostname": "db-master1.db-rc.iad.aws.qa.appetize-dev.com"
        }
    },
    "timestamp": {
        "date": "2018-06-25 22:22:56.861145",
        "timezone_type": 3,
        "timezone": "UTC"
    }
}
```

-- Then I run the report I need to. Here's an example Category Sales report:
```
GET /api/v1/reports/category-sales?report=categorySales&amp;reportType=excel&amp;start_date=2014-01-28 00:00:00&amp;end_date=2018-06-21 00:00:00&amp;event_date=0&amp;include_comps=0&amp;weekdays=0&amp;vendors=-1&amp;vendor_groups=-1&amp;category_1_ids=0&amp;category_2_ids=0&amp;category_3_ids=0&amp;email=chukky@appetizeapp.com&amp;venue_id=715&amp;connect_entity_id=715&amp;connect_entity_type=venue&amp;event_time_end_shift=6 HTTP/1.1
Host: 0.0.0.0:8080
Cache-Control: no-cache

```

Here's the output:
```
{
    "title": "Get Category Sales Report",
    "status": true,
    "data": {
        "report": {
            "files": {
                "xlsx": "/var/www/storage/715_1529965768.xlsx",
                "ods": "/var/www/storage/715_1529965768.ods",
                "csv": "/var/www/storage/715_1529965768.csv",
                "pdf": "/var/www/storage/715_1529965768.pdf"
            },
            "time": {
                "requestStartTime": 1529965754,
                "requestEndTime": 1529965769,
                "dataRunEndTime": 1529965768,
                "generateReportStartTime": 1529965768,
                "timeToRunReportQueries": "14seconds",
                "timeToGenerateReportFiles": "1seconds",
                "timeForRequest": "15seconds"
            },
            "parameters": {
                "venue_id": "715",
                "email": "chukky@appetizeapp.com",
                "report": "categorySales",
                "start_date": "2014-01-28 00:00:00",
                "end_date": "2018-06-21 00:00:00",
                "event_date": "0",
                "connect_entity_id": "715",
                "connect_entity_type": "venue",
                "event_time_end_shift": "6",
                "weekdays": "0",
                "reportType": "excel",
                "include_comps": "0",
                "category_1_ids": "0",
                "category_2_ids": "0",
                "category_3_ids": "0",
                "vendors": "-1",
                "vendor_groups": "-1"
            }
        }
    },
    "message": "Category Sales generated successfully.",
    "timestamp": {
        "date": "2018-06-25 22:29:29.729001",
        "timezone_type": 3,
        "timezone": "UTC"
    }
}
```


Here's an example TT report:
```
GET /api/v1/reports/tender-type?venue_id=741&amp;email=chukky@appetizeapp.com&amp;report=tenderType&amp;start_date=2018-05-01 00:00:00&amp;end_date=2018-05-03 23:59:59&amp;event_date=0&amp;type_guest=1&amp;type_user=1&amp;connect_entity_id=741&amp;connect_entity_type=venue&amp;weekdays=0&amp;event_time_end_shift=6&amp;reportType=excel HTTP/1.1
Host: 0.0.0.0:8080
Cache-Control: no-cache

```

Here's the output:
```
{
    "title": "Get Tender Type Report",
    "status": true,
    "data": {
        "report": {
            "files": {
                "xlsx": "/var/www/storage/741_1529966454.xlsx",
                "csv": "/var/www/storage/741_1529966454.csv"
            },
            "time": {
                "requestStartTime": 1529966132,
                "requestEndTime": 1529966454,
                "dataRunEndTime": 1529966454,
                "generateReportStartTime": 1529966454,
                "timeToRunReportQueries": "322seconds",
                "timeToGenerateReportFiles": "0seconds",
                "timeForRequest": "322seconds"
            },
            "parameters": {
                "venue_id": "741",
                "email": "chukky@appetizeapp.com",
                "report": "tenderType",
                "start_date": "2018-05-01 00:00:00",
                "end_date": "2018-05-03 23:59:59",
                "event_date": "0",
                "connect_entity_id": "741",
                "connect_entity_type": "venue",
                "event_time_end_shift": "6",
                "weekdays": "0",
                "reportType": "excel",
                "type_guest": "1",
                "type_user": "1"
            }
        }
    },
    "message": "Tender Type generated successfully.",
    "timestamp": {
        "date": "2018-06-25 22:40:54.751671",
        "timezone_type": 3,
        "timezone": "UTC"
    }
}
```

## Using with Prod Data
To use with production data the following will connect you to prod. Open a terminal and run the following command to open an ssh tunnel

`ssh -i ~/.ssh/your-key-location -L <Your internal ip [found at System Preferences | Network]>:<a non used port on ur host>:internal-DB-WOPR-698177319.us-east-1.elb.amazonaws.com:3306 <ur bastion username>@bastion-int.prod.appetizeapp.com`

Basically forward a port on ur local host machine to prod via the bastion server. Here's my example:

`ssh -i ~/.ssh/appetize/appetize -L 10.100.18.194:3326:internal-DB-WOPR-698177319.us-east-1.elb.amazonaws.com:3306 cnze@bastion-int.prod.appetizeapp.com`

Run this in a terminal on your host machine and leave the connection open. Then change your connection parameters in .env like so:


DB_CONNECT_HOST=<Your internal ip>
DB_CONNECT_PORT=<ur non used port on ur host>
DB_CONNECT_DATABASE=appetize
DB_CONNECT_USERNAME=zzzzzzzzz
DB_CONNECT_PASSWORD=yyyyyyyyyyy

Here's mine:

# My ssh test
DB_CONNECT_HOST=10.100.18.194
DB_CONNECT_PORT=3326
DB_CONNECT_DATABASE=appetize
DB_CONNECT_USERNAME=xxxxxx
DB_CONNECT_PASSWORD=yyyyyyyyyyy


Run 0.0.0.0:8080?t=1 and you should see the health check page showing u a prod connection. Here's mine:
````
{
    "app": "Appetize Web Services - Reporting Service",
    "status": true,
    "dependencies": {
    "git": {
         "currentBranch": "combining-temp-tables"
    },
    "db": {
        "sql_mode": "NO_ENGINE_SUBSTITUTION",
        "innodb_version": "5.7.19-17",
        "hostname": "db4.db-wopr.iad.aws.prod.appetizeapp.com"
        }
    },
    "timestamp": {
    "date": "2018-06-27 22:43:01.828853",
    "timezone_type": 3,
    "timezone": "UTC"
    }
}
````
